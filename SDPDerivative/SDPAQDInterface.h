#ifndef SDPA_QD_INTERFACE_H
#define SDPA_QD_INTERFACE_H

#ifdef SDPA_QD_SUPPORT
#include "CommonFile/MathBasic.h"
#include "SDPSolution.h"
#include <Eigen/Sparse>
#include <sdpa-qd/sdpa-qd-7.1.2/sdpa_io.h>

PRJ_BEGIN

template <typename T>
struct PSDConeTraits
{
  typedef Eigen::Matrix<T,-1,1> Vec;
  typedef Eigen::Matrix<T,-1,-1> Mat;
  typedef Eigen::SparseMatrix<T,0,sizeType> SMat;
  typedef std::vector<SMat> PSDCone;
};
struct SDPAInterface
{
public:
  typedef PSDConeTraits<qd_real>::Vec Vec;
  typedef PSDConeTraits<qd_real>::Mat Mat;
  typedef PSDConeTraits<qd_real>::SMat SMat;
  typedef PSDConeTraits<qd_real>::PSDCone PSDCone;
  SDPAInterface();
  void reset(sdpa::Parameter::parameterType type=sdpa::Parameter::PARAMETER_DEFAULT);
  void solve(bool output=true,sdpa::Solutions* ptInit=NULL);
  void check();
  template <typename T>
  void setProblem(const typename PSDConeTraits<T>::Vec& b,const std::vector<typename PSDConeTraits<T>::PSDCone>& cones);
  void getSolution(SDPSolution& sol,scalarD eps=0.0f) const;
  template <typename T>
  void fillInputData(const typename PSDConeTraits<T>::PSDCone& cone,int bid);
  template <typename T>
  void countNonZeros(const typename PSDConeTraits<T>::PSDCone& cone,int bid);
  Eigen::Map<const Mat> ZG(int bid) const;
  Eigen::Map<const Mat> FG(int bid) const;
  Vec xG() const;
  template <typename MAT,typename MATIN>
  MAT convert(const MATIN& in) const {
    MAT ret;
    ret.resize(in.rows(),in.cols());
    for(sizeType r=0; r<in.rows(); r++)
      for(sizeType c=0; c<in.cols(); c++)
        ret(r,c)=in(r,c).x[0];
    return ret;
  }
private:
  Mat F0(int bid) const;
  Mat FI(int bid,int mid) const;
  Mat convert(const sdpa::SparseLinearSpace& ms,int bid) const;
  void printMat(const std::string& name,const Mat& m) const;
  sdpa::Parameter _param;
  sdpa::InputData _inputData;
  sdpa::ComputeTime _com;
  sdpa::Solutions _currentPt;
  std::vector<int> _blockStruct;
  std::vector<std::vector<int>> _SDP_index;
};

PRJ_END
#endif

#endif

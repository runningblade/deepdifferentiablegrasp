//fromString
template <typename T>
void convert(const std::string& str,T& val) {
  val=boost::lexical_cast<T>(str);
}
template <typename T,char LABEL>
void convert(const std::string& str,SOSTerm<T,LABEL>& val) {
  val << str;
}
template <typename T,char LABEL>
void convert(const std::string& str,SOSPolynomial<T,LABEL>& val) {
  val << str;
}

//toString
template <typename T>
std::string convert(T val) {
  return boost::lexical_cast<std::string>(val);
}
template <typename T,char LABEL>
std::string convert(const SOSTerm<T,LABEL>& val) {
  return val.operator std::string();
}
template <typename T,char LABEL>
std::string convert(const SOSPolynomial<T,LABEL>& val) {
  return val.operator std::string();
}

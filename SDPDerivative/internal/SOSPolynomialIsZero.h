//meta-template function to check if a polynomial/term is zero
template <typename T>
struct IsZero {
  static bool isZero(const T& val) {
    return val==0;
  }
  static T removeZero(const T& val) {
    return val;
  }
};
template <typename T,char LABEL>
struct IsZero<SOSPolynomial<T,LABEL>> {
  static bool isZero(const SOSPolynomial<T,LABEL>& val) {
    for(sizeType i=0;i<(sizeType)val._terms.size();i++)
      if(!IsZero<T>::isZero(val._terms[i]._coef))
        return false;
    return true;
  }
  static SOSPolynomial<T,LABEL> removeZero(const SOSPolynomial<T,LABEL>& val) {
    SOSPolynomial<T,LABEL> ret;
    for(sizeType i=0;i<(sizeType)val._terms.size();i++) {
      SOSTerm<T,LABEL> t=val._terms[i];
      t._coef=IsZero<T>::removeZero(t._coef);
      if(!IsZero<T>::isZero(t._coef))
        ret+=t;
    }
    return ret;
  }
};

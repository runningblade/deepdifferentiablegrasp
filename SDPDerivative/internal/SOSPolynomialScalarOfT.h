//meta-template function to convert scalar to poly-of-poly
template <typename T>
struct ScalarOfT {
  typedef T Type;
  template <typename T2>
  static T convert(T2 val) {
    return T(val);
  }
};
template <typename T,char LABEL>
struct ScalarOfT<SOSTerm<T,LABEL>> {
  typedef typename ScalarOfT<T>::Type Type;
  template <typename T2>
  static SOSTerm<T,LABEL> convert(T2 val) {
    return SOSTerm<T,LABEL>(ScalarOfT<T>::convert(val));
  }
};
template <typename T,char LABEL>
struct ScalarOfT<SOSPolynomial<T,LABEL>> {
  typedef typename ScalarOfT<T>::Type Type;
  template <typename T2>
  static SOSPolynomial<T,LABEL> convert(T2 val) {
    return SOSPolynomial<T,LABEL>(ScalarOfT<T>::convert(val));
  }
};

#include "SOSPolynomialScalarQ.h"

#ifdef MIXED_ACCURACY
#ifndef QUADMATH_SUPPORT
PRJ_BEGIN
bool readBinaryData(scalarQ& coef,std::istream& is,IOData* dat) {
  is.read((char*)&coef,sizeof(scalarQ));
  return is.good();
}
bool writeBinaryData(scalarQ coef,std::ostream& os,IOData* dat) {
  os.write((char*)&coef,sizeof(scalarQ));
  return os.good();
}
PRJ_END
namespace std
{
#define STDQUAD1(NAME) scalarQ NAME(scalarQ a) {return NAME##q(a);}
#define STDQUAD2(NAME) scalarQ NAME(scalarQ a,scalarQ b) {return NAME##q(a,b);}
STDQUAD1(acos)
STDQUAD1(acosh)
STDQUAD1(asin)
STDQUAD1(asinh)
STDQUAD1(atan)
STDQUAD1(atanh)
STDQUAD2(atan2)
STDQUAD1(cbrt)
STDQUAD1(ceil)
STDQUAD1(cosh)
STDQUAD1(cos)
STDQUAD1(erf)
STDQUAD1(erfc)
STDQUAD1(exp)
STDQUAD1(fabs)
STDQUAD1(floor)
STDQUAD2(fmax)
STDQUAD2(fmin)
STDQUAD2(fmod)
STDQUAD1(isinf)
STDQUAD1(isnan)
STDQUAD1(log)
STDQUAD1(log10)
STDQUAD1(log2)
STDQUAD2(pow)
STDQUAD1(sinh)
STDQUAD1(sin)
STDQUAD1(sqrt)
STDQUAD1(tanh)
STDQUAD1(tan)
#undef STDQUAD1
#undef STDQUAD2
scalarQ abs(scalarQ a)
{
  return fabs(a);
}
bool isfinite(scalarQ a)
{
  return !isinfq(a) && !isnanq(a);
}
scalarQ frexp(scalarQ a,int* exp)
{
  return frexpq(a,exp);
}
scalarQ ldexp(scalarQ a,int exp)
{
  return ldexpq(a,exp);
}
istream& operator>>(istream& input,scalarQ& x)
{
  double tmp;
  input >> tmp;
  x=tmp;
  return input;
}
ostream& operator<<(ostream& output,scalarQ x)
{
  output << (double)x;
  return output;
}
}
#endif
#endif

#include "CommonFile/Config.h"
#include <iostream>

#define MIXED_ACCURACY
#ifndef MIXED_ACCURACY
//we disable this to make it run on longleaf
typedef scalarD scalarQ;
#else
#include <quadmath.h>
typedef __float128 scalarQ;
#ifndef QUADMATH_SUPPORT
PRJ_BEGIN
struct IOData;
bool readBinaryData(scalarQ& coef,std::istream& is,IOData* dat=NULL);
bool writeBinaryData(scalarQ coef,std::ostream& os,IOData* dat=NULL);
PRJ_END
namespace std
{
#define STDQUAD1(NAME) scalarQ NAME(scalarQ a);
#define STDQUAD2(NAME) scalarQ NAME(scalarQ a,scalarQ b);
STDQUAD1(acos)
STDQUAD1(acosh)
STDQUAD1(asin)
STDQUAD1(asinh)
STDQUAD1(atan)
STDQUAD1(atanh)
STDQUAD2(atan2)
STDQUAD1(cbrt)
STDQUAD1(ceil)
STDQUAD1(cosh)
STDQUAD1(cos)
STDQUAD1(erf)
STDQUAD1(erfc)
STDQUAD1(exp)
STDQUAD1(fabs)
STDQUAD1(floor)
STDQUAD2(fmax)
STDQUAD2(fmin)
STDQUAD2(fmod)
STDQUAD1(isinf)
STDQUAD1(isnan)
STDQUAD1(log)
STDQUAD1(log10)
STDQUAD1(log2)
STDQUAD2(pow)
STDQUAD1(sinh)
STDQUAD1(sin)
STDQUAD1(sqrt)
STDQUAD1(tanh)
STDQUAD1(tan)
#undef STDQUAD1
#undef STDQUAD2
scalarQ abs(scalarQ a);
bool isfinite(scalarQ a);
scalarQ frexp(scalarQ a,int* exp);
scalarQ ldexp(scalarQ a,int exp);
istream& operator>>(istream& input,scalarQ& x);
ostream& operator<<(ostream& output,scalarQ x);
}
#endif
#endif

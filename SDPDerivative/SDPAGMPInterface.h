#ifndef SDPA_GMP_INTERFACE_H
#define SDPA_GMP_INTERFACE_H

#ifdef SDPA_GMP_SUPPORT
#include "CommonFile/MathBasic.h"
#include "SDPSolution.h"
#include <Eigen/Sparse>
#include <sdpa-gmp/sdpa-gmp-7.1.3/sdpa_io.h>

PRJ_BEGIN

template <typename T>
struct PSDConeTraits
{
  typedef Eigen::Matrix<T,-1,1> Vec;
  typedef Eigen::Matrix<T,-1,-1> Mat;
  typedef Eigen::SparseMatrix<T,0,sizeType> SMat;
  typedef std::vector<SMat> PSDCone;
};
struct SDPAInterface
{
public:
  typedef PSDConeTraits<mpf_class>::Vec Vec;
  typedef PSDConeTraits<mpf_class>::Mat Mat;
  typedef PSDConeTraits<mpf_class>::SMat SMat;
  typedef PSDConeTraits<mpf_class>::PSDCone PSDCone;
  SDPAInterface();
  void reset(sdpa::Parameter::parameterType type=sdpa::Parameter::PARAMETER_DEFAULT);
  void solve(bool output=true,sdpa::Solutions* ptInit=NULL);
  void check();
  template <typename T>
  void setProblem(const typename PSDConeTraits<T>::Vec& b,const std::vector<typename PSDConeTraits<T>::PSDCone>& cones);
  void getSolution(SDPSolution& sol,scalarD eps=0.0f) const;
  template <typename T>
  void fillInputData(const typename PSDConeTraits<T>::PSDCone& cone,int bid);
  template <typename T>
  void countNonZeros(const typename PSDConeTraits<T>::PSDCone& cone,int bid);
  Eigen::Map<const Mat> ZG(int bid) const;
  Eigen::Map<const Mat> FG(int bid) const;
  Vec xG() const;
private:
  Mat F0(int bid) const;
  Mat FI(int bid,int mid) const;
  Mat convert(const sdpa::SparseLinearSpace& ms,int bid) const;
  void printMat(const std::string& name,const Mat& m) const;
  sdpa::Parameter _param;
  sdpa::InputData _inputData;
  sdpa::ComputeTime _com;
  sdpa::Solutions _currentPt;
  std::vector<int> _blockStruct;
  std::vector<std::vector<int>> _SDP_index;
};

PRJ_END
#endif

#endif

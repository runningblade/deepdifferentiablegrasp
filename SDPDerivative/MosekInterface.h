#ifndef MOSEK_INTERFACE_H
#define MOSEK_INTERFACE_H

#include "SOSPolynomial.h"
#include <CommonFile/solvers/Objective.h>
#include <fusion.h>

PRJ_BEGIN

//Mosek
#define DECL_MOSEK_TYPES    \
typedef mosek::fusion::Expr ExprM;  \
typedef mosek::fusion::Model ModelM;  \
typedef mosek::fusion::Matrix MatrixM;  \
typedef mosek::fusion::Domain DomainM;  \
typedef mosek::fusion::Variable VariableM;  \
typedef mosek::fusion::Constraint ConstraintM;  \
typedef mosek::fusion::Expression ExpressionM;  \
typedef mosek::fusion::ObjectiveSense ObjectiveSenseM;
struct MosekInterface : public Objective<scalarD>
{
  DECL_MOSEK_TYPES
  typedef std::vector<DMat,Eigen::aligned_allocator<DMat>> Dss;
  //solver
  static bool trySolve(ModelM& m,std::string& str);
  static bool trySolve(ModelM& m);
  static void ensureSolve(ModelM& m,std::string& str);
  static void ensureSolve(ModelM& m);
  //to mosek matrix
  static std::shared_ptr<monty::ndarray<double,1>> toMosek(const Vec& v);
  static std::shared_ptr<monty::ndarray<double,1>> toMosek(std::vector<double>& vals);
  static std::shared_ptr<monty::ndarray<double,1>> toMosek(double* dat,sizeType sz);
  template <typename T2>
  static monty::rc_ptr<MatrixM> toMosek(const Eigen::SparseMatrix<T2,0,sizeType>& m)
  {
      std::vector<int> rows,cols;
      std::vector<double> vals;
      for(sizeType k=0; k<m.outerSize(); ++k)
        for(typename Eigen::SparseMatrix<T2,0,sizeType>::InnerIterator it(m,k); it; ++it) {
          rows.push_back(it.row());
          cols.push_back(it.col());
          vals.push_back((typename ScalarOfT<T2>::Type)it.value());
        }
      return toMosek(m.rows(),m.cols(),rows,cols,vals);
  }
  static monty::rc_ptr<MatrixM> toMosek(int nrR,int nrC,std::vector<int>& rows,std::vector<int>& cols,std::vector<double>& vals);
  static monty::rc_ptr<MatrixM> toMosek(int nrR,int nrC,int nrE,int* rows,int* cols,double* vals);
  //to mosek expression
  static monty::rc_ptr<ExpressionM> toMosekE(const Vec& v);
  static monty::rc_ptr<ExpressionM> toMosekE(std::vector<double>& vals);
  static monty::rc_ptr<ExpressionM> toMosekE(double* dat,sizeType sz);
  //to eigen matrix
  static Vec fromMosek(VariableM& v);
  //initial guess
  static void toMoesk(VariableM& v,const Vec& vLevel);
  //index
  static monty::rc_ptr<ExpressionM> index(monty::rc_ptr<VariableM> v,const std::vector<sizeType>& ids);
  //linear constraint
  static void addCI(ModelM& m,const SMat& CI,const Vec& CI0,const Coli& CIType,monty::rc_ptr<VariableM> v);
  //SDP sensitivity analysis
  static void getFZ(std::vector<monty::rc_ptr<ConstraintM>>& cons,Dss& Fss,Dss& Zss);
};

PRJ_END

#endif

#include "internal/SOSPolynomialScalarQ.h"
#include "CommonFile/IOBasic.h"
#include "SparseUtils.h"
#include "SOSProblem.h"
#include <boost/algorithm/string.hpp>

USE_PRJ_NAMESPACE

//countADerivOffset
template <typename T>
sizeType countADerivOffset(const Eigen::SparseMatrix<T,0,sizeType>& val) {
  return 0;
}
sizeType countADerivOffset(const Eigen::SparseMatrix<SOSPolynomial<scalarD,'V'>,0,sizeType>& A) {
  typedef Eigen::SparseMatrix<SOSPolynomial<scalarD,'V'>,0,sizeType> SMat;
  sizeType offset=0;
  for(sizeType k=0; k<A.outerSize(); ++k)
    for(typename SMat::InnerIterator it(A,k); it; ++it)
      offset=std::max<sizeType>(offset,it.value().nrVar());
  return offset;
}
//buildADerivValue
template <typename T>
T buildADerivValue(const T& val,sizeType nrVarXLambda) {
  FUNCTION_NOT_IMPLEMENTED
  return T();
}
SOSPolynomial<scalarD,'V'> buildADerivValue(const SOSPolynomial<scalarD,'V'>& val,sizeType nrVarXLambda) {
  SOSPolynomial<scalarD,'V'> gval;
  std::vector<SOSPolynomial<scalarD,'V'>> g=val.gradient();
  for(sizeType i=0; i<(sizeType)g.size(); i++)
    gval+=g[i]*SOSTerm<scalarD,'V'>(1,nrVarXLambda+i,1);
  return gval;
}
scalarD buildADerivValue(const scalarD& val,sizeType nrVarXLambda) {
  return 0;
}
#ifdef MIXED_ACCURACY
#ifndef QUADMATH_SUPPORT
scalarQ buildADerivValue(const scalarQ& val,sizeType nrVarXLambda) {
  return 0;
}
#endif
#endif
//SOSProblem
template <typename T,char LABELX,char LABELY>
SOSProblem<T,LABELX,LABELY>::SOSProblem() {}
template <typename T,char LABELX,char LABELY>
SOSProblem<T,LABELX,LABELY>::SOSProblem(const PolyXYs& preCondGE,const PolyXYs& preCondEQ,const PolyXY& postCond,sizeType desiredHalfOrder,bool forceQuadratic)
{
  reset(preCondGE,preCondEQ,postCond,desiredHalfOrder,forceQuadratic);
}
template <typename T,char LABELX,char LABELY>
void SOSProblem<T,LABELX,LABELY>::reset(const PolyXYs& preCondGE,const PolyXYs& preCondEQ,PolyXY postCond,sizeType desiredHalfOrder,bool forceQuadratic)
{
  //count how many Xs,Ys,order do we have
  _nrVarX=postCond.template nrVar<LABELX>();
  _nrVarY=postCond.template nrVar<LABELY>();
  _orderX=postCond.template order<LABELX>();
  _orderY=postCond.template order<LABELY>();
  for(sizeType i=0; i<(sizeType)preCondGE.size(); i++) {
    ASSERT_MSGV(preCondGE[i].template nrVar<LABELX>()==0,"nrVarX(preCondGE%ld)>0",i)
    _nrVarY=std::max<sizeType>(_nrVarY,preCondGE[i].template nrVar<LABELY>());
  }
  for(sizeType i=0; i<(sizeType)preCondEQ.size(); i++) {
    ASSERT_MSGV(preCondEQ[i].template nrVar<LABELX>()==0,"nrVarX(preCondEQ%ld)>0",i)
    _nrVarY=std::max<sizeType>(_nrVarY,preCondEQ[i].template nrVar<LABELY>());
  }
  //build monomial/term map
  _termMap.clear();
  sizeType halfOrder0=(_orderY+1)/2;
  _monomialY=buildMonomial(halfOrder0);
  updateTermMap();
  if(desiredHalfOrder>halfOrder0) {
    halfOrder0=desiredHalfOrder;
    _monomialY=buildMonomial(halfOrder0);
    updateTermMap();
  }
  //handle quadratic cones by building Augmented Lagrangian constraints
  ASSERT_MSGV(_orderX==1 || _orderX==2,"We can only handle constraint with orderX==1 or orderX==2, while given orderX=(%ld)!",_orderX)
  if(_orderX==1 && !forceQuadratic)
    _augLagCons.resize(0);
  else buildAugLagCons(postCond);
  //build main PSDCone
  MAT coefMat;
  _nrVarXLambda=_nrVarX;
  _PSDCones.assign(1,sosCoef(postCond));
  //build Lagrangian multiplier cones
  for(sizeType i=0; i<(sizeType)preCondEQ.size(); i++) {
    sizeType order=preCondEQ[i].template order<LABELY>(),halfOrder=(order+1)/2;
    ASSERT_MSGV(halfOrder<halfOrder0,"order(preCondEQ%ld)>=order(postCond)",i)
    PolyXY multiplier=buildLagrangian(_nrVarXLambda,halfOrder0-halfOrder,coefMat);
    _PSDCones[0]-=sosCoef(preCondEQ[i]*multiplier);
  }
  for(sizeType i=0; i<(sizeType)preCondGE.size(); i++) {
    sizeType order=preCondGE[i].template order<LABELY>(),halfOrder=(order+1)/2;
    ASSERT_MSGV(halfOrder<halfOrder0,"order(preCondGE%ld)>=order(postCond)",i)
    PolyXY multiplier=buildLagrangian(_nrVarXLambda,halfOrder0-halfOrder,coefMat);
    _PSDCones[0]-=sosCoef(preCondGE[i]*multiplier);
    _PSDCones.push_back(coefMat);  //we compact the matrix
  }
  buildPSDConeCoefs();
}
template <typename T,char LABELX,char LABELY>
bool SOSProblem<T,LABELX,LABELY>::read(std::istream& is,IOData* dat)
{
  readBinaryData(_nrVarX,is);
  readBinaryData(_nrVarY,is);
  readBinaryData(_orderX,is);
  readBinaryData(_orderY,is);
  readBinaryData(_nrVarXLambda,is);
  //temporary
  readBinaryData(_monomialY,is);
  readBinaryData(_termMap,is);
  PolyXY::read(_PSDCones,is);
  PolyXY::read(_augLagCons,is);
  //unused data
  buildPSDConeCoefs();
  return is.good();
}
template <typename T,char LABELX,char LABELY>
bool SOSProblem<T,LABELX,LABELY>::write(std::ostream& os,IOData* dat) const
{
  writeBinaryData(_nrVarX,os);
  writeBinaryData(_nrVarY,os);
  writeBinaryData(_orderX,os);
  writeBinaryData(_orderY,os);
  writeBinaryData(_nrVarXLambda,os);
  //temporary
  writeBinaryData(_monomialY,os);
  writeBinaryData(_termMap,os);
  PolyXY::write(_PSDCones,os);
  PolyXY::write(_augLagCons,os);
  return os.good();
}
template <typename T,char LABELX,char LABELY>
boost::shared_ptr<SerializableBase> SOSProblem<T,LABELX,LABELY>::copy() const
{
  return boost::shared_ptr<SerializableBase>(new SOSProblem<T,LABELX,LABELY>);
}
template <typename T,char LABELX,char LABELY>
std::string SOSProblem<T,LABELX,LABELY>::type() const
{
  return typeid(SOSProblem<T,LABELX,LABELY>).name();
}
//evaluate using vals
template <typename T,char LABELX,char LABELY>
void SOSProblem<T,LABELX,LABELY>::evaluate(SOSProblemScalar& other,COLDCM vals,COLDCM* valsDeriv) const
{
  other._nrVarX=_nrVarX;
  other._nrVarY=_nrVarY;
  other._orderX=_orderX;
  other._orderY=_orderY;
  other._nrVarXLambda=_nrVarXLambda;
  other._cs=_cs;
  if(!valsDeriv) {
    //coefsTypeII
    evaluateSMat(_A,other._A,vals,NULL);
    evaluateVec(_A0,other._A0,vals);
  } else {
    //coefsTypeIIDeriv
    evaluateSMat(_ADeriv,other._A,vals,valsDeriv);
  }
}
template <typename T,char LABELX,char LABELY>
void SOSProblem<T,LABELX,LABELY>::evaluateSMat(const SMat& curr,typename SOSProblemScalar::SMat& other,COLDCM vals,COLDCM* valsDeriv)
{
  ASSERT(curr.isCompressed())
  other.resize(curr.rows(),curr.cols());
  other.reserve(curr.nonZeros());
  COLD valsCPY=valsDeriv?concat<COLD>(vals,*valsDeriv):vals;
  for(sizeType c=0; c<curr.cols(); c++) {
    other.startVec(c);
    for(typename SMat::InnerIterator it(curr,c); it; ++it)
      other.insertBack(it.row(),c)=ParallelEvaluate<T>::evalSOSProblem(it.value(),valsCPY);
  }
  other.finalize();
}
template <typename T,char LABELX,char LABELY>
void SOSProblem<T,LABELX,LABELY>::evaluateVec(const VECX& curr,typename SOSProblemScalar::VECX& other,COLDCM vals)
{
  other.resize(curr.size());
  for(sizeType i=0; i<curr.size(); i++)
    other[i]=ParallelEvaluate<T>::evalSOSProblem(curr[i],vals);
}
//the first cone is the main condition, and the other cones are lagrangian multiplier cones
template <typename T,char LABELX,char LABELY>
const std::vector<typename SOSProblem<T,LABELX,LABELY>::MAT>& SOSProblem<T,LABELX,LABELY>::PSDCones() const
{
  return _PSDCones;
}
template <typename T,char LABELX,char LABELY>
sizeType SOSProblem<T,LABELX,LABELY>::nrVarXLambda() const
{
  return _nrVarXLambda;
}
template <typename T,char LABELX,char LABELY>
sizeType SOSProblem<T,LABELX,LABELY>::nrVarX() const
{
  return _nrVarX;
}
template <typename T,char LABELX,char LABELY>
sizeType SOSProblem<T,LABELX,LABELY>::orderX() const
{
  return _orderX;
}
template <typename T,char LABELX,char LABELY>
bool SOSProblem<T,LABELX,LABELY>::isLinear() const
{
  return _augLagCons.size()==0;
}
template <typename T,char LABELX,char LABELY>
bool SOSProblem<T,LABELX,LABELY>::isQuadratic() const
{
  return _augLagCons.size()>0;
}
template <typename T,char LABELX,char LABELY>
std::string SOSProblem<T,LABELX,LABELY>::printCones() const
{
  std::string ret;
  for(sizeType i=0; i<(sizeType)_PSDCones.size(); i++) {
    std::string name="postCond Cone";
    if(i>0)
      name=std::string("preCondGE")+std::to_string(i)+" Cone";
    ret+=name+":\n";
    ret+=printCone(_PSDCones[i]);
  }
  return ret;
}
template <typename T,char LABELX,char LABELY>
std::string SOSProblem<T,LABELX,LABELY>::printAugLagCons() const
{
  std::string ret;
  if(isLinear())
    ret+="This is linear cone, no augmented Lagrangian constraints!\n";
  else {
    ret+="This is quadratic cone, augmented Lagrangian constraints:\n";
    for(sizeType i=0; i<(sizeType)_augLagCons.size(); i++)
      ret+="X"+std::to_string(i)+"="+_augLagCons[i].compactString()+"\n";
  }
  return ret;
}
template <typename T,char LABELX,char LABELY>
const typename SOSProblem<T,LABELX,LABELY>::VEC& SOSProblem<T,LABELX,LABELY>::augLagCons() const
{
  return _augLagCons;
}
template <typename T,char LABELX,char LABELY>
const Coli& SOSProblem<T,LABELX,LABELY>::cs() const
{
  return _cs;
}
template <typename T,char LABELX,char LABELY>
const typename SOSProblem<T,LABELX,LABELY>::SMat& SOSProblem<T,LABELX,LABELY>::A() const
{
  return _A;
}
template <typename T,char LABELX,char LABELY>
const typename SOSProblem<T,LABELX,LABELY>::VECX& SOSProblem<T,LABELX,LABELY>::A0() const
{
  return _A0;
}
//writeVTK
template <typename T,char LABELX,char LABELY>
void SOSProblem<T,LABELX,LABELY>::writeQ1AVTK(const std::string& path) const
{
  //blkR
  Coli blkR=Coli::Zero(_PSDCones.size()-1);
  for(sizeType i=1; i<(sizeType)_PSDCones.size(); i++)
    blkR[i-1]=_PSDCones[i].rows()*(_PSDCones[i].rows()+1)/2;
  ASSERT(blkR.sum()==_A.rows())
  //blkC
  Coli blkC=Coli::Zero(_PSDCones.size()-1);
  for(sizeType i=2; i<(sizeType)_PSDCones.size(); i++)
    blkC[i-1]=blkR[i-1];
  blkC[0]=_A.cols()-blkC.sum();
  ASSERT(blkC.sum()==_A.cols())
  //SMat
  writeSMatVTK<T,0,sizeType>(_A,path,&blkR,&blkC);
}
//helper
template <typename T,char LABELX,char LABELY>
void SOSProblem<T,LABELX,LABELY>::buildPSDConeCoefs()
{
  sizeType rowsA=0;
  std::vector<T> A0Vec;
  typename PolyX::STRIPS trips;
  typedef typename ScalarOfT<T>::Type Scalar;
  const Scalar sqrt2=std::sqrt((Scalar)2);
  _cs.setZero((sizeType)_PSDCones.size());
  for(sizeType i=0; i<(sizeType)_PSDCones.size(); i++) {
    const MAT& cone=_PSDCones[i];
    sizeType N=cone.rows();
    _cs[i]=N*(N+1)/2;
    A0Vec.resize(rowsA+_cs[i],ScalarOfT<T>::convert(0));
    //collect trips
    for(sizeType r=0,off=0; r<N; r++)
      for(sizeType c=r; c<N; c++,off++) {
        const PolyX& entry=cone(r,c);
        for(sizeType t=0; t<(sizeType)entry._terms.size(); t++) {
          const TermX& entryT=entry._terms[t];
          if(entryT._id.empty()) {
            A0Vec[rowsA+off]=r==c?entryT._coef:entryT._coef*sqrt2;
          } else {
            ASSERT_MSG(entryT._order[0]==1,"This is not a LMI, so we cannot build PSD coneCoefficient for it!")
            trips.push_back(typename PolyX::STRIP(rowsA+off,entryT._id[0],r==c?entryT._coef:entryT._coef*sqrt2));
          }
        }
      }
    rowsA+=_cs[i];
  }
  //initialize
  _A.resize(rowsA,_nrVarXLambda);
  _A.setFromTriplets(trips.begin(),trips.end());
  _A.makeCompressed();
  _A0=Eigen::Map<VECX>(&A0Vec[0],(sizeType)A0Vec.size());
  //derivative
  _ADeriv.resize(rowsA,_nrVarXLambda);
  sizeType offset=countADerivOffset(_A);
  for(sizeType k=0; k<_A.outerSize(); ++k) {
    _ADeriv.startVec(k);
    for(typename SMat::InnerIterator it(_A,k); it; ++it)
      _ADeriv.insertBack(it.row(),it.col())=buildADerivValue(it.value(),offset);
  }
  _ADeriv.finalize();
  _ADeriv.makeCompressed();
}
template <typename T,char LABELX,char LABELY>
void SOSProblem<T,LABELX,LABELY>::buildAugLagCons(PolyXY& postCond)
{
  PolyXY ret;
  sizeType nrT=(sizeType)postCond._terms.size();
  _augLagCons.resize(nrT);
  for(sizeType i=0; i<nrT; i++) {
    const TermXY& t=postCond._terms[i];
    //replace quadratic term with linear term
    TermXY tLinear;
    tLinear._coef=PolyX(TermX(1,i,1));
    tLinear._id=t._id;
    tLinear._order=t._order;
    ret+=tLinear;
    //add augmented Lagrangian constraint
    _augLagCons[i]=t._coef;
  }
  //replace quadratic polynomial with linear polynomial
  postCond=ret;
  _nrVarX=postCond.template nrVar<LABELX>();
  ASSERT(_nrVarX==(sizeType)_augLagCons.size())
}
template <typename T,char LABELX,char LABELY>
std::string SOSProblem<T,LABELX,LABELY>::printCone(const MAT& cone) const
{
  sizeType len=0;
  std::string ret;
  std::vector<std::vector<std::string>> terms(cone.rows());
  for(sizeType r=0; r<cone.rows(); r++)
    for(sizeType c=0; c<cone.cols(); c++) {
      std::string str=cone(r,c).compactString();
      len=std::max<sizeType>(len,str.length());
      terms[r].push_back(str);
    }
  for(sizeType r=0; r<cone.rows(); r++) {
    for(sizeType c=0; c<cone.cols(); c++) {
      std::string str=terms[r][c];
      if((sizeType)str.length()<len)
        str+=std::string(len-str.length(),' ');
      ASSERT((sizeType)str.length()==len)
      ret+=str+" | ";
    }
    ret+="\n";
  }
  return ret;
}
template <typename T,char LABELX,char LABELY>
typename SOSProblem<T,LABELX,LABELY>::PolyXY SOSProblem<T,LABELX,LABELY>::buildLagrangian(sizeType& nrVar,sizeType order,MAT& coefMat) const
{
  typedef typename ScalarOfT<T>::Type Scalar;
  const Scalar sqrt2=std::sqrt((Scalar)2);
  PolyXY monomial=buildMonomial(order),ret;
  sizeType nrT=(sizeType)monomial._terms.size();
  coefMat.resize(nrT,nrT);
  for(sizeType r=0; r<nrT; r++)
    for(sizeType c=r; c<nrT; c++) {
      TermXY t=monomial._terms[r]*monomial._terms[c];
      t._coef=TermX(ScalarOfT<T>::convert(r==c?1:1/sqrt2),nrVar++,1);  //this makes Lagrangian block in A identity matrices
      coefMat(r,c)=coefMat(c,r)=t._coef;
      ret+=t;
    }
  return ret;
}
template <typename T,char LABELX,char LABELY>
void SOSProblem<T,LABELX,LABELY>::buildMonomial(PolyXY& ret,sizeType remainingOrder,TermXY term,int vid) const
{
  if(vid==_nrVarY)
    ret+=term;
  else {
    for(sizeType order=0; order<=remainingOrder; order++) {
      TermXY tmp=term;
      if(order>0) {
        tmp._id.push_back(vid);
        tmp._order.push_back(order);
      }
      buildMonomial(ret,remainingOrder-order,tmp,vid+1);
    }
  }
}
template <typename T,char LABELX,char LABELY>
typename SOSProblem<T,LABELX,LABELY>::PolyXY SOSProblem<T,LABELX,LABELY>::buildMonomial(sizeType maxOrder) const
{
  PolyXY ret;
  buildMonomial(ret,maxOrder,ScalarOfT<SOSTerm<PolyX,LABELY>>::convert(1),0);
  return ret;
}
template <typename T,char LABELX,char LABELY>
typename SOSProblem<T,LABELX,LABELY>::MAT SOSProblem<T,LABELX,LABELY>::sosCoef(const PolyXY& poly) const
{
  sizeType nrT=(sizeType)_monomialY._terms.size();
  MAT ret=MAT::Constant(nrT,nrT,ScalarOfT<PolyX>::convert(0));
  for(sizeType i=0; i<(sizeType)poly._terms.size(); i++) {
    typename std::map<TermXY,Vec2i>::const_iterator it=_termMap.find(poly._terms[i]);
    ASSERT(it!=_termMap.end())
    Vec2i rc=it->second;
    if(rc[0]==rc[1])
      ret(rc[0],rc[1])+=poly._terms[i]._coef;
    else {
      ret(rc[0],rc[1])+=poly._terms[i]._coef*ScalarOfT<T>::convert(0.5);
      ret(rc[1],rc[0])+=poly._terms[i]._coef*ScalarOfT<T>::convert(0.5);
    }
  }
  return ret;
}
template <typename T,char LABELX,char LABELY>
void SOSProblem<T,LABELX,LABELY>::updateTermMap()
{
  sizeType nrT=(sizeType)_monomialY._terms.size();
  for(sizeType r=0; r<nrT; r++)
    for(sizeType c=0; c<=r; c++) {
      TermXY t=_monomialY._terms[r]*_monomialY._terms[c];
      if(_termMap.find(t)==_termMap.end())
        _termMap[t]=Vec2i(r,c);
    }
}
//force instance
template class SOSProblem<SOSPolynomial<scalarD,'V'>,'x','a'>;
template class SOSProblem<scalarD,'x','a'>;
#ifdef MIXED_ACCURACY
#ifndef QUADMATH_SUPPORT
template class SOSProblem<scalarQ,'x','a'>;
#endif
#endif

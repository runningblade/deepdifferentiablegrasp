#ifndef MPF_KERNEL_H
#define MPF_KERNEL_H

#include "CommonFile/solvers/MatVec.h"

PRJ_BEGIN

//============================================================================
// Vector Interface
template <>
struct Kernel<mpf_class> {
  template<typename T2>
  struct Rebind {
    typedef Kernel<T2> value;
  };
  typedef mpf_class T;
  typedef mpf_class Scalar;
  typedef Eigen::Matrix<mpf_class,-1,1> Vec;
  typedef Eigen::Matrix<mpf_class,-1,-1> Mat;
  static FORCE_INLINE T dot(const Vec& x, const Vec& y,sizeType n=-1) {
    if(n == -1)return x.dot(y);
    else return x.block(0,0,n,1).dot(y.block(0,0,n,1));
  }
  static FORCE_INLINE T norm(const Vec& x,sizeType n=-1) {
    if(n == -1)return x.norm();
    else return x.block(0,0,n,1).norm();
  }
  static FORCE_INLINE T norm1(const Vec& x,sizeType n=-1) {
    if(n == -1)return x.cwiseAbs().sum();
    else return x.block(0,0,n,1).cwiseAbs().sum();
  }
  static FORCE_INLINE void copy(const Vec& x,Vec& y,sizeType n=-1) {
    if(n == -1)y=x;
    else y.block(0,0,n,1)=x.block(0,0,n,1);
  }
  static FORCE_INLINE void copy(const Mat& x,Mat& y) {
    y=x;
  }
  static FORCE_INLINE void ncopy(const Vec& x,Vec& y,sizeType n=-1) {
    if(n == -1)y=-x;
    else y.block(0,0,n,1)=-x.block(0,0,n,1);
  }
  static FORCE_INLINE sizeType indexAbsMax(const Vec& x,sizeType n=-1) {
    if(n == -1)n=x.size();
    sizeType maxInd = 0;
    T maxValue = (T)0.0f;
    for(sizeType i = 0; i < n; ++i) {
      if(abs(x[i]) > maxValue) {
        maxValue = abs(x[i]);
        maxInd = i;
      }
    }
    return maxInd;
  }
  static FORCE_INLINE T absMax(const Vec& x,sizeType n=-1) {
    if(n == -1)return abs(x[indexAbsMax(x)]);
    else return abs(x[indexAbsMax(x.block(0,0,n,1))]);
  }
  static FORCE_INLINE T minValue(const Vec& x) {
    return x.minCoeff();
  }
  static FORCE_INLINE T maxValue(const Vec& x) {
    return x.maxCoeff();
  }
  static FORCE_INLINE void scale(T alpha,Vec& y,sizeType n=-1) {
    if(n == -1)y*=alpha;
    else y.block(0,0,n,1)*=alpha;
  }
  static FORCE_INLINE void addScaled(const T& alpha, const Vec& x, Vec& y,sizeType n=-1) {
    if(n == -1)y+=x*alpha;
    else y.block(0,0,n,1)+=x.block(0,0,n,1)*alpha;
  }
  static FORCE_INLINE void add(const Vec& x,const Vec& y,Vec& result,sizeType n=-1) {
    if(n == -1)result=x+y;
    else result.block(0,0,n,1)=x.block(0,0,n,1)+y.block(0,0,n,1);
  }
  static FORCE_INLINE void sub(const Vec& x,const Vec& y,Vec& result,sizeType n=-1) {
    if(n == -1)result=x-y;
    else result.block(0,0,n,1)=x.block(0,0,n,1)-y.block(0,0,n,1);
  }
  static FORCE_INLINE void cwiseProd(const Vec& x, const Vec& y, Vec& z,sizeType n=-1) {
    if(n == -1)z.array()=x.array()*y.array();
    else z.block(0,0,n,1).array()=
        x.block(0,0,n,1).array()*
        y.block(0,0,n,1).array();
  }
  static FORCE_INLINE void cwisePow(const T& p, Vec& x,sizeType n=-1) {
    FUNCTION_NOT_IMPLEMENTED
  }
  static FORCE_INLINE void zero(Vec& v,sizeType n=-1) {
    set(v,0.0f,n);
  }
  static FORCE_INLINE void set(Vec& v,const T& val,sizeType n=-1) {
    if(n == -1)v.setConstant(val);
    else v.block(0,0,n,1).setConstant(val);
  }
  static FORCE_INLINE void mul(const Mat& A,const Vec& x,Vec& out) {
    out=A*x;
  }
  static FORCE_INLINE void mulT(const Mat& A,const Vec& x,Vec& out) {
    out=A.transpose()*x;
  }
  static FORCE_INLINE sizeType rows(const Mat& A) {
    return A.rows();
  }
  static FORCE_INLINE sizeType cols(const Mat& A) {
    return A.cols();
  }
  static FORCE_INLINE T colNorm(const Mat& A,sizeType i) {
    return A.col(i).norm();
  }
  template <typename A,typename B>
  static FORCE_INLINE void addMulT(A& x,const T& b,const B& c) {
    x+=b*c;
  }
  template <typename A,typename B>
  static FORCE_INLINE void subMulT(A& x,const T& b,const B& c) {
    x-=b*c;
  }
  static FORCE_INLINE T absV(const T& val) {
    return abs(val);
  }
  static FORCE_INLINE T ZeroV() {
    return (T)0.0f;
  }
};

PRJ_END

#endif

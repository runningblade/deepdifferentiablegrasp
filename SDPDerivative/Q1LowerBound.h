#ifndef Q1_LOWER_BOUND_H
#define Q1_LOWER_BOUND_H

#include "SOSProblem.h"
#include "SDPSolution.h"
#include "MosekInterface.h"

PRJ_BEGIN

class Q1LowerBound : public Objective<scalarD>, public SerializableBase
{
public:
  DECL_MOSEK_TYPES
  typedef SOSTerm<scalarD,'V'> TermV;
  typedef SOSPolynomial<scalarD,'V'> PolyV;
  typedef SOSProblem<scalarD,'x','a'> SOSProb;
  typedef SOSProblem<SOSPolynomial<scalarD,'V'>,'x','a'> SOSProbTpl;
  typedef std::vector<Mat6Xd,Eigen::aligned_allocator<Mat6Xd>> Vsss;
  typedef std::vector<DMat,Eigen::aligned_allocator<DMat>> Ess;
  typedef std::vector<Vec,Eigen::aligned_allocator<Vec>> Sss;
  Q1LowerBound();
  Q1LowerBound(const Mat6d& M,scalarD mu,sizeType halfOrder,sizeType nrPt,sizeType nrDir);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  //compute value
  void debugBuildProb();
  Mat6Xd buildVss(const Mat3Xd& pss,const Mat3Xd& nss) const;
  SOSProb buildProb(const Mat6Xd& vss,const Mat6Xd* vssDeriv=NULL) const;
  SOSProb buildProbBF(const Mat6Xd& vss) const;
  void buildCones(const SOSProb& prob,std::vector<SOSProb::PSDCone>& cones) const;
  void writeProbSDPA(const SOSProb& prob,const std::string& path) const;
  scalarD solveProb(const SOSProb& prob,SDPSolution& sol,scalarD eps=0.0f,bool output=true) const;
  scalarD solveProbSDPA(const SOSProb& prob,SDPSolution& sol,scalarD eps=0.0f,bool output=true) const;
  scalarD solveProbMosek(const SOSProb& prob,SDPSolution& sol,scalarD eps=0.0f,bool output=true) const;
  void check(const SOSProb& prob,const SDPSolution& sol) const;
  sizeType nrDir() const;
  sizeType nrPt() const;
  //compute jacobian
  void debugJG(sizeType nrIter=10) const;
  SDPSolution::SMat computeJG(const SOSProb& prob,const SDPSolution& sol) const;
  SDPSolution::DMat computeDGDEps(const Mat6Xd& vss,const Vsss& dVdEps,const SDPSolution& sol) const;
  SDPSolution::Vec computeG(const SOSProb& prob,const SDPSolution& sol) const;
  void writeJGVTK(const std::string& path,const SMat& JG) const;
  Vec computeC() const;
  //compute jacobian eigen
  void debugJGEigen(sizeType nrIter=10) const;
  SDPSolution::SMat computeSJGEigen(const SDPSolution& sol) const;
  SDPSolution::SMat computeVJGEigen(const SDPSolution& sol) const;
  SDPSolution::SMat computeJGEigen(const SOSProb& prob,const SDPSolution& sol) const;
  SDPSolution::DMat computeDGDEpsEigen(const Mat6Xd& vss,const Vsss& dVdEps,const SDPSolution& sol) const;
  //compute jacobian eigen symmetric
  void debugJGSymmetric(sizeType nrIter=10) const;
  SDPSolution::DMat computeJGSymmetric(const SOSProb& prob,const SDPSolution& sol) const;
  SDPSolution::DMat computeDGDEpsSymmetric(const Mat6Xd& vss,const Vsss& dVdEps,const SOSProb& prob,const SDPSolution& sol) const;
  //sensitivity
  SDPSolution::Vec computeDXDEps(const Mat6Xd& vss,const Vsss& dVdEps,const SOSProb& prob,const SDPSolution& sol) const;
  SDPSolution::Vec computeDXDEpsEigen(const Mat6Xd& vss,const Vsss& dVdEps,const SOSProb& prob,const SDPSolution& sol) const;
  SDPSolution::Vec computeDXDEpsSymmetric(const Mat6Xd& vss,const Vsss& dVdEps,const SOSProb& prob,const SDPSolution& sol) const;
  void debugDXDEps(sizeType nrIter=10) const;
private:
  //parameter
  Mat6d _invM;
  scalarD _mu;
  sizeType _halfOrder;
  sizeType _nrPt,_nrDir;
  SOSProbTpl _probTpl;
};

PRJ_END

#endif

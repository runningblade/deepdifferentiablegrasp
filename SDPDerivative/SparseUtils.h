#ifndef SPARSE_UTILS_H
#define SPARSE_UTILS_H

#include <Eigen/Eigen>
#include <Eigen/Sparse>
#include <CommonFile/solvers/Objective.h>
#include <CommonFile/solvers/ParallelVector.h>
#include <boost/static_assert.hpp>

PRJ_BEGIN

//sparse matrix building
//dense
template <typename MAT,typename Derived>
EIGEN_DEVICE_FUNC static void addBlock(MAT& H,sizeType r,sizeType c,const Eigen::MatrixBase<Derived>& coef)
{
  H.block(r,c,coef.rows(),coef.cols())+=coef.template cast<typename MAT::Scalar>();
}
template <typename MAT>
EIGEN_DEVICE_FUNC static void addBlock(MAT& H,sizeType r,sizeType c,typename MAT::Scalar coef)
{
  H(r,c)+=coef;
}
template <typename MAT>
EIGEN_DEVICE_FUNC static void addBlockId(MAT& H,sizeType r,sizeType c,sizeType nr,typename MAT::Scalar coefId)
{
  H.block(r,c,nr,nr).diagonal().array()+=coefId;
}
//sparse
template <typename Derived>
EIGEN_DEVICE_FUNC static void addBlock(ParallelVector<Eigen::Triplet<typename Derived::Scalar,sizeType> >& H,sizeType r,sizeType c,const Eigen::MatrixBase<Derived>& coef)
{
#ifndef __CUDACC__
  sizeType nrR=coef.rows();
  sizeType nrC=coef.cols();
  for(sizeType i=0; i<nrR; i++)
    for(sizeType j=0; j<nrC; j++)
      H.push_back(Eigen::Triplet<typename Derived::Scalar,sizeType>(r+i,c+j,coef(i,j)));
#else
  FUNCTION_NOT_IMPLEMENTED
#endif
}
template <typename T>
EIGEN_DEVICE_FUNC static void addBlock(ParallelVector<Eigen::Triplet<T,sizeType> >& H,sizeType r,sizeType c,T coef)
{
#ifndef __CUDACC__
  H.push_back(Eigen::Triplet<T,sizeType>(r,c,coef));
#else
  FUNCTION_NOT_IMPLEMENTED
#endif
}
template <typename Derived>
EIGEN_DEVICE_FUNC static void addBlockI(ParallelVector<Eigen::Triplet<typename Derived::Scalar,sizeType> >& H,sizeType r,sizeType c,const Eigen::MatrixBase<Derived>& coefI)
{
#ifndef __CUDACC__
  sizeType nr=coefI.size();
  for(sizeType i=0; i<nr; i++)
    H.push_back(Eigen::Triplet<typename Derived::Scalar,sizeType>(r+i,c+i,coefI[i]));
#else
  FUNCTION_NOT_IMPLEMENTED
#endif
}
template <typename T>
EIGEN_DEVICE_FUNC static void addBlockId(ParallelVector<Eigen::Triplet<T,sizeType> >& H,sizeType r,sizeType c,sizeType nr,T coefId)
{
#ifndef __CUDACC__
  for(sizeType i=0; i<nr; i++)
    H.push_back(Eigen::Triplet<T,sizeType>(r+i,c+i,coefId));
#else
  FUNCTION_NOT_IMPLEMENTED
#endif
}
template <typename S,int O,typename I>
EIGEN_DEVICE_FUNC static void addBlock(ParallelVector<Eigen::Triplet<S,sizeType> >& H,sizeType r,sizeType c,const Eigen::SparseMatrix<S,O,I>& coef)
{
#ifndef __CUDACC__
  for(sizeType k=0; k<coef.outerSize(); ++k)
    for(typename Eigen::SparseMatrix<S,O,I>::InnerIterator it(coef,k); it; ++it)
      H.push_back(Eigen::Triplet<S,sizeType>(r+it.row(),c+it.col(),it.value()));
#else
  FUNCTION_NOT_IMPLEMENTED
#endif
}
//sparseVec
template <typename Derived>
EIGEN_DEVICE_FUNC static void addBlock(Eigen::SparseVector<typename Derived::Scalar,0,sizeType>& H,sizeType r,const Eigen::MatrixBase<Derived>& coef)
{
  for(sizeType i=0; i<coef.rows(); i++)
    H.coeffRef(r+i)+=coef[i];
}
//svec indices
EIGEN_DEVICE_FUNC static sizeType IJToTri(sizeType N,sizeType r,sizeType c)
{
  if(r>c)std::swap(r,c);
  return N*(N-1)/2-(N-r)*(N-r-1)/2+c;
}
EIGEN_DEVICE_FUNC static void TriToIJ(sizeType N,sizeType k,sizeType& i,sizeType& j)
{
  i=N-1-sizeType(std::sqrt(-8*k+4*N*(N+1)-7)/2.0-0.5);
  j=k+i-N*(N+1)/2+(N-i)*(N+1-i)/2;
}
EIGEN_DEVICE_FUNC static sizeType NSVecToN(sizeType N)
{
  return std::sqrt(N*2);
}
EIGEN_DEVICE_FUNC static void debugTriToIJ(sizeType N)
{
  sizeType i,j,off=0;
  for(sizeType r=0; r<N; r++)
    for(sizeType c=r; c<N; c++,off++) {
      TriToIJ(N,off,i,j);
      ASSERT(i==r && j==c && IJToTri(N,i,j)==off)
    }
}
//svec
template <typename T>
EIGEN_DEVICE_FUNC static typename Eigen::Matrix<T,-1,1> toSVec(const Eigen::Matrix<T,-1,-1>& m)
{
  typedef Eigen::Matrix<T,-1,1> Vec;
  //typedef Eigen::Matrix<T,-1,-1> DMat;
  const T sqrt2=std::sqrt((T)2);
  Vec ret=Vec::Zero(m.rows()*(m.rows()+1)/2);
  for(sizeType r=0,off=0; r<m.rows(); r++)
    for(sizeType c=r; c<m.cols(); c++,off++)
      ret[off]=r==c?m(r,c):m(r,c)*sqrt2;
  return ret;
}
template <typename T>
EIGEN_DEVICE_FUNC static typename Eigen::Matrix<T,-1,-1> fromSVec(const Eigen::Matrix<T,-1,1>& s)
{
  //typedef Eigen::Matrix<T,-1,1> Vec;
  typedef Eigen::Matrix<T,-1,-1> DMat;
  const T sqrt2=std::sqrt((T)2);
  sizeType rows=NSVecToN(s.size());
  DMat ret=DMat::Zero(rows,rows);
  for(sizeType r=0,off=0; r<ret.rows(); r++)
    for(sizeType c=r; c<ret.cols(); c++,off++)
      if(r==c)
        ret(r,c)=s[off];
      else ret(r,c)=ret(c,r)=s[off]/sqrt2;
  return ret;
}
//symmetric kronecker
template <typename S,typename MATL,typename MATR>
EIGEN_DEVICE_FUNC static typename Eigen::Matrix<S,-1,-1> symmetricKronecker(sizeType N,const MATL& A,const MATR& B)
{
  typename Eigen::Matrix<S,-1,-1> SK;
  sizeType rowsSK=N*(N+1)/2;
  SK.resize(rowsSK,rowsSK);
  const S sqrt2=std::sqrt((S)2);
  const S invSqrt2=1/sqrt2;
  //Hrc=(ArmXmnBcn+BrmXmnAcn)/2
  for(sizeType r=0,offr=0; r<N; r++)
    for(sizeType c=r; c<N; c++,offr++) {
      for(sizeType rx=0,offc=0; rx<N; rx++)
        for(sizeType cx=rx; cx<N; cx++,offc++) {
          S val=(A(r,rx)*B(c,cx)+A(c,cx)*B(r,rx))/2;
          if(rx!=cx) {
            val+=(A(r,cx)*B(c,rx)+A(c,rx)*B(r,cx))/2;
            val*=invSqrt2;
          }
          if(r!=c)
            val*=sqrt2;
          SK(offr,offc)=val;
        }
    }
  return SK;
}
template <typename S,typename MATL>
EIGEN_DEVICE_FUNC static typename Eigen::Matrix<S,-1,-1> symmetricKronecker(const MATL& A)
{
  typedef std::function<S(sizeType,sizeType)> MATR;
  MATR B=[&](sizeType r,sizeType c) {
    return r==c?1:0;
  };
  return symmetricKronecker<S,MATL,MATR>(A.rows(),A,B);
}
template <typename S,int O,typename I,typename VECA,typename VECB>
EIGEN_DEVICE_FUNC static typename Eigen::Matrix<S,-1,-1> symmetricKroneckerSVec(const VECA& A,const VECB& B)
{
  typedef std::function<S(I,I)> MAT;
  const S invSqrt2=1/std::sqrt((S)2);
  sizeType N=std::sqrt(A.size()*2);
  MAT AM=[&](sizeType r,sizeType c) {
    S val=A[IJToTri(N,r,c)];
    return r==c?val:val*invSqrt2;
  };
  MAT BM=[&](sizeType r,sizeType c) {
    S val=B[IJToTri(N,r,c)];
    return r==c?val:val*invSqrt2;
  };
  return symmetricKronecker<S,MAT,MAT>(N,AM,BM);
}
template <typename S,typename VECA>
EIGEN_DEVICE_FUNC static typename Eigen::Matrix<S,-1,-1> symmetricKroneckerSVec(const VECA& A)
{
  typedef std::function<S(sizeType,sizeType)> MAT;
  const S invSqrt2=1/std::sqrt((S)2);
  sizeType N=std::sqrt(A.size()*2);
  MAT AM=[&](sizeType r,sizeType c) {
    S val=A[IJToTri(N,r,c)];
    return r==c?val:val*invSqrt2;
  };
  MAT B=[&](sizeType r,sizeType c) {
    return r==c?1:0;
  };
  return symmetricKronecker<S,MAT,MAT>(N,AM,B);
}
//symmetric kronecker eigen
template <typename T>
EIGEN_DEVICE_FUNC static typename Eigen::Matrix<T,-1,1> symmetricKroneckerEigenvectors(const typename Eigen::Matrix<T,-1,-1>& m,sizeType I)
{
  typedef Eigen::Matrix<T,-1,1> Vec;
  //typedef Eigen::Matrix<T,-1,-1> DMat;
  const T sqrt2=std::sqrt((T)2);
  Vec ret=Vec::Zero(m.rows()*(m.rows()+1)/2);
  for(sizeType r=0,off=0; r<m.rows(); r++)
    for(sizeType c=r; c<m.rows(); c++,off++)
      ret[off]=m(r,I)*m(c,I)*(r==c?1:sqrt2);
  return ret;
}
template <typename T>
EIGEN_DEVICE_FUNC static typename Eigen::Matrix<T,-1,1> symmetricKroneckerEigenvectors(const typename Eigen::Matrix<T,-1,-1>& m,sizeType I,sizeType J)
{
  typedef Eigen::Matrix<T,-1,1> Vec;
  //typedef Eigen::Matrix<T,-1,-1> DMat;
  const T invSqrt2=1/std::sqrt((T)2);
  Vec ret=Vec::Zero(m.rows()*(m.rows()+1)/2);
  for(sizeType r=0,off=0; r<m.rows(); r++)
    for(sizeType c=r; c<m.rows(); c++,off++) {
      ret[off]=m(r,I)*m(c,J)+m(r,J)*m(c,I);
      ret[off]*=(r==c)?invSqrt2:1;
    }
  return ret;
}
template <typename T>
EIGEN_DEVICE_FUNC static typename Eigen::Matrix<T,-1,-1> symmetricKroneckerEigenvectors(const typename Eigen::Matrix<T,-1,-1>& V)
{
  Eigen::Matrix<T,-1,-1> ret;
  sizeType rows=V.rows()*(V.rows()+1)/2;
  ret.resize(rows,rows);
  for(sizeType r=0,off=0; r<V.rows(); r++)
    for(sizeType c=r; c<V.rows(); c++,off++)
      if(r==c)
        ret.col(off)=symmetricKroneckerEigenvectors(V,r);
      else ret.col(off)=symmetricKroneckerEigenvectors(V,r,c);
  return ret;
}
template <typename T>
EIGEN_DEVICE_FUNC static typename Eigen::Matrix<T,-1,1> symmetricKroneckerEigenvalues(const typename Eigen::Matrix<T,-1,1>& V)
{
  Eigen::Matrix<T,-1,1> ret;
  sizeType rows=V.rows()*(V.rows()+1)/2;
  ret.resize(rows);
  for(sizeType r=0,off=0; r<V.rows(); r++)
    for(sizeType c=r; c<V.rows(); c++,off++)
      ret[off]=0.5*(V[r]+V[c]);
  return ret;
}
EIGEN_DEVICE_FUNC static void debugsymmetricKroneckerEigen(sizeType N)
{
  Matd M=Matd::Random(N,N);
  M=(M+M.transpose()).eval();
  Matd MSK=symmetricKronecker<scalarD,Matd>(M);
  //eigen system
  Eigen::SelfAdjointEigenSolver<Matd> sol(M,Eigen::ComputeEigenvectors);
  Matd VSK=symmetricKroneckerEigenvectors<scalarD>(sol.eigenvectors());
  Cold sSK=symmetricKroneckerEigenvalues<scalarD>(sol.eigenvalues());
  INFOV("MSKNorm=%f MSKNormErr=%f",MSK.norm(),(VSK*sSK.asDiagonal()*VSK.transpose()-MSK).norm())
}
//complementary trisection of symmetric kronecker eigen
template <typename T>
EIGEN_DEVICE_FUNC static void symmetricKroneckerEigenvectors
(const typename Eigen::Matrix<T,-1,-1>& V,
 typename Eigen::Matrix<T,-1,-1>& V1,
 typename Eigen::Matrix<T,-1,-1>& V2,
 typename Eigen::Matrix<T,-1,-1>& V3,
 const Colc& typeFZ,sizeType nrV1,sizeType nrV2,sizeType nrV3)
{
  sizeType rows=V.rows()*(V.rows()+1)/2;
  V1.resize(rows,nrV1);
  V2.resize(rows,nrV2);
  V3.resize(rows,nrV3);
  sizeType offV1=0,offV2=0,offV3=0;
  for(sizeType r=0,off=0; r<V.rows(); r++)
    for(sizeType c=r; c<V.rows(); c++,off++)
      if(r==c) {
        if(typeFZ[off]==1)
          V1.col(offV1++)=symmetricKroneckerEigenvectors(V,r);
        else if(typeFZ[off]==2)
          V2.col(offV2++)=symmetricKroneckerEigenvectors(V,r);
        else V3.col(offV3++)=symmetricKroneckerEigenvectors(V,r);
      } else {
        if(typeFZ[off]==1)
          V1.col(offV1++)=symmetricKroneckerEigenvectors(V,r,c);
        else if(typeFZ[off]==2)
          V2.col(offV2++)=symmetricKroneckerEigenvectors(V,r,c);
        else V3.col(offV3++)=symmetricKroneckerEigenvectors(V,r,c);
      }
  ASSERT(V1.cols()==offV1 && V2.cols()==offV2 && V3.cols()==offV3)
}
template <typename T>
EIGEN_DEVICE_FUNC static void symmetricKroneckerEigensystemTrisection
(const typename Eigen::Matrix<T,-1,-1>& V,
 const typename Eigen::Matrix<T,-1,1>& EF,
 const typename Eigen::Matrix<T,-1,1>& EZ,
 typename Eigen::Matrix<T,-1,-1>& V1,
 typename Eigen::Matrix<T,-1,-1>& V2,
 typename Eigen::Matrix<T,-1,-1>& V3,
 typename Eigen::Matrix<T,-1,1>& Z1,
 typename Eigen::Matrix<T,-1,1>& Z2,
 typename Eigen::Matrix<T,-1,1>& F1,
 typename Eigen::Matrix<T,-1,1>& F2,
 T eps,T eps2=1E12)
{
  sizeType nrV1=0,nrV2=0,nrV3=0;
  std::vector<T> Z1V,Z2V,F1V,F2V;
  Colc typeFZ=Colc::Zero(EF.size()*(EF.size()+1)/2);
  typename Eigen::Matrix<T,-1,1> EFSK,EZSK;
  if(eps>0) {
    //determine non-zeros using an epsilon
    EFSK=symmetricKroneckerEigenvalues<T>(EF);
    EZSK=symmetricKroneckerEigenvalues<T>(EZ);
    for(sizeType c=0; c<typeFZ.size(); c++)
      if(EFSK[c]<eps) {
        Z1V.push_back(EZSK[c]);
        typeFZ[c]=1;
        nrV1++;
      } else if(EZSK[c]<eps) {
        F2V.push_back(EFSK[c]);
        typeFZ[c]=3;
        nrV3++;
      } else {
        if(eps2>0 && EZSK[c]>eps2*EFSK[c]) {
          Z1V.push_back(EZSK[c]);
          typeFZ[c]=1;
          nrV1++;
        } else {
          Z2V.push_back(EZSK[c]);
          F1V.push_back(EFSK[c]);
          typeFZ[c]=2;
          nrV2++;
        }
      }
  } else {
    //assuming strict complementarity
    //determine non-zeros by comparing EFSK and EZSK
    std::vector<bool> isEFLarger(EF.size());
    for(sizeType i=0; i<EF.size(); i++)
      isEFLarger[i]=EF[i]>EZ[i];
    for(sizeType r=0,off=0; r<EF.size(); r++)
      for(sizeType c=r; c<EF.size(); c++,off++) {
        T EFSK=0.5*(EF[r]+EF[c]);
        T EZSK=0.5*(EZ[r]+EZ[c]);
        if(!isEFLarger[r] && !isEFLarger[c]) {
          Z1V.push_back(EZSK);
          typeFZ[off]=1;
          nrV1++;
        } else if(isEFLarger[r] && isEFLarger[c]) {
          F2V.push_back(EFSK);
          typeFZ[off]=3;
          nrV3++;
        } else {
          if(eps2>0 && EZSK>eps2*EFSK) {
            Z1V.push_back(EZSK);
            typeFZ[off]=1;
            nrV1++;
          } else {
            Z2V.push_back(EZSK);
            F1V.push_back(EFSK);
            typeFZ[off]=2;
            nrV2++;
          }
        }
      }
  }
  Z1=Eigen::Map<typename Eigen::Matrix<T,-1,1>>(&Z1V[0],(sizeType)Z1V.size());
  Z2=Eigen::Map<typename Eigen::Matrix<T,-1,1>>(&Z2V[0],(sizeType)Z2V.size());
  F1=Eigen::Map<typename Eigen::Matrix<T,-1,1>>(&F1V[0],(sizeType)F1V.size());
  F2=Eigen::Map<typename Eigen::Matrix<T,-1,1>>(&F2V[0],(sizeType)F2V.size());
  symmetricKroneckerEigenvectors<T>(V,V1,V2,V3,typeFZ,nrV1,nrV2,nrV3);
}
//visualize
template <typename S,int O,typename I>
EIGEN_DEVICE_FUNC static void writeSMatVTK(const typename Eigen::SparseMatrix<S,O,I>& coef,const std::string& path,const Coli* blkR,const Coli* blkC)
{
  VTKWriter<scalar> os("SMat",path,true);
  std::vector<Vec3,Eigen::aligned_allocator<Vec3>> vss;
  std::vector<Vec3i,Eigen::aligned_allocator<Vec3i>> tss;
  std::vector<Vec2i,Eigen::aligned_allocator<Vec2i>> lss;
  //element
  for(sizeType k=0; k<coef.outerSize(); ++k)
    for(typename Eigen::SparseMatrix<S,O,I>::InnerIterator it(coef,k); it; ++it) {
      sizeType off=(sizeType)vss.size();
      vss.push_back(Vec3(it.col()  ,coef.rows()-it.row()-1,1));
      vss.push_back(Vec3(it.col()+1,coef.rows()-it.row()-1,1));
      vss.push_back(Vec3(it.col()+1,coef.rows()-it.row()  ,1));
      vss.push_back(Vec3(it.col()  ,coef.rows()-it.row()  ,1));
      tss.push_back(Vec3i(off+0,off+1,off+2));
      tss.push_back(Vec3i(off+0,off+2,off+3));
    }
  //line
  {
    sizeType off=(sizeType)vss.size();
    vss.push_back(Vec3(0          ,0          ,1));
    vss.push_back(Vec3(coef.cols(),0          ,1));
    vss.push_back(Vec3(coef.cols(),coef.rows(),1));
    vss.push_back(Vec3(0          ,coef.rows(),1));
    lss.push_back(Vec2i(off+0,off+1));
    lss.push_back(Vec2i(off+1,off+2));
    lss.push_back(Vec2i(off+2,off+3));
    lss.push_back(Vec2i(off+3,off+0));
  }
  //blk lines
  if(blkR)
    for(sizeType i=0,Y=coef.rows(); i<blkR->size(); Y-=blkR->coeff(i),i++)
      if(Y>0 && Y<coef.rows()) {
        sizeType off=(sizeType)vss.size();
        vss.push_back(Vec3(0          ,Y,1));
        vss.push_back(Vec3(coef.cols(),Y,1));
        lss.push_back(Vec2i(off+0,off+1));
      }
  if(blkC)
    for(sizeType i=0,X=0; i<blkC->size(); X+=blkC->coeff(i),i++)
      if(X>0 && X<coef.cols()) {
        sizeType off=(sizeType)vss.size();
        vss.push_back(Vec3(X,0          ,1));
        vss.push_back(Vec3(X,coef.rows(),1));
        lss.push_back(Vec2i(off+0,off+1));
      }
  //feed
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(tss.begin(),tss.end(),VTKWriter<scalar>::TRIANGLE);
  os.appendCells(lss.begin(),lss.end(),VTKWriter<scalar>::LINE);
}

PRJ_END

#endif

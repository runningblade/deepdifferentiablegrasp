#ifndef SYSV_INTERFACE_H
#define SYSV_INTERFACE_H

#include "SDPSolution.h"

PRJ_BEGIN

template <typename T>
struct SYSVInterface
{
  typedef Eigen::Matrix<T,-1,-1> Mat;
  static void solve(Mat& LHS,Mat& RHS);
};
template <>
struct SYSVInterface<double>
{
  typedef Eigen::Matrix<double,-1,-1> Mat;
  static void solve(Mat& LHS,Mat& RHS);
  static void debug(sizeType N=100);
};
template <>
struct SYSVInterface<float>
{
  typedef Eigen::Matrix<float,-1,-1> Mat;
  static void solve(Mat& LHS,Mat& RHS);
  static void debug(sizeType N=100);
};
#if defined(SDPA_GMP_SUPPORT)||defined(SDPA_DD_SUPPORT)||defined(SDPA_QD_SUPPORT)
template <>
struct SYSVInterface<SDPA_FLOAT>
{
  typedef Eigen::Matrix<SDPA_FLOAT,-1,-1> Mat;
  static void solve(Mat& LHS,Mat& RHS);
  static void debug(sizeType N=100);
};
#endif

PRJ_END

#endif

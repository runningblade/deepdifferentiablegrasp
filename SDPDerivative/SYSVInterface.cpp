#include "SYSVInterface.h"
#include "DebugGradient.h"
#include "lapacke.h"

USE_PRJ_NAMESPACE

template <typename T>
void SYSVInterface<T>::solve(Mat& LHS,Mat& RHS)
{
  FUNCTION_NOT_IMPLEMENTED
}
void SYSVInterface<double>::solve(Mat& LHS,Mat& RHS)
{
  int n=LHS.rows();
  int nrhs=RHS.cols();
  int lda=n;
  int ldb=n;
  int* ipiv=(int*)LAPACKE_malloc(sizeof(double)*n);
  LAPACKE_dsysv(LAPACK_COL_MAJOR,'L',n,nrhs,LHS.data(),lda,ipiv,RHS.data(),ldb);
  LAPACKE_free(ipiv);
}
void SYSVInterface<double>::debug(sizeType N)
{
  Mat LHS=Mat::Random(N,N);
  LHS=(LHS+LHS.transpose()).eval();
  Mat RHS=Mat::Random(N,N/2);
  Mat ref=LHS.fullPivLu().solve(RHS);
  INFOV("Ref: %f, err: %f",ref.norm(),(LHS*ref-RHS).norm())
  solve(LHS,RHS);
  DEFINE_NUMERIC_DELTA_T(double)
  DEBUG_GRADIENT("Sys",ref.norm(),(ref-RHS).norm())
}
void SYSVInterface<float>::solve(Mat& LHS,Mat& RHS)
{
  int n=LHS.rows();
  int nrhs=RHS.cols();
  int lda=n;
  int ldb=n;
  int* ipiv=(int*)LAPACKE_malloc(sizeof(float)*n);
  LAPACKE_ssysv(LAPACK_COL_MAJOR,'L',n,nrhs,LHS.data(),lda,ipiv,RHS.data(),ldb);
  LAPACKE_free(ipiv);
}
void SYSVInterface<float>::debug(sizeType N)
{
  Mat LHS=Mat::Random(N,N);
  LHS=(LHS+LHS.transpose()).eval();
  Mat RHS=Mat::Random(N,N/2);
  Mat ref=LHS.fullPivLu().solve(RHS);
  INFOV("Ref: %f, err: %f",ref.norm(),(LHS*ref-RHS).norm())
  solve(LHS,RHS);
  DEFINE_NUMERIC_DELTA_T(float)
  DEBUG_GRADIENT("Sys",ref.norm(),(ref-RHS).norm())
}
#if defined(SDPA_DD_SUPPORT)||defined(SDPA_QD_SUPPORT)||defined(SDPA_GMP_SUPPORT)
void SYSVInterface<SDPA_FLOAT>::solve(Mat& LHS,Mat& RHS)
{
  RHS=LHS.fullPivLu().solve(RHS);
}
void SYSVInterface<SDPA_FLOAT>::debug(sizeType N)
{
  Mat LHS=Mat::Random(N,N);
  LHS=(LHS+LHS.transpose()).eval();
  Mat RHS=Mat::Random(N,N/2);
  Mat ref=LHS.fullPivLu().solve(RHS);
  INFOV("Ref: %f, err: %f",SDPA2ScalarD(ref.norm()),SDPA2ScalarD((LHS*ref-RHS).norm()))
  solve(LHS,RHS);
  DEFINE_NUMERIC_DELTA_T(float)
  DEBUG_GRADIENT("Sys",SDPA2ScalarD(ref.norm()),SDPA2ScalarD((ref-RHS).norm()))
}
#endif

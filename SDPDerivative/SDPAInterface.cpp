#ifdef SDPA_SUPPORT
#include "internal/SOSPolynomialScalarQ.h"
#include "SDPAInterface.h"
#include "SparseUtils.h"

USE_PRJ_NAMESPACE

using namespace sdpa;

SDPAInterface::SDPAInterface()
{
  reset();
}
void SDPAInterface::reset(sdpa::Parameter::parameterType type)
{
  _type=(SDPA::ParameterType)type;
}
void SDPAInterface::solve(bool output,sdpa::Solutions* ptInit)
{
  ASSERT(_problem)
  _problem->setDisplay(output?stdout:NULL);
  if(ptInit) {
    _problem->currentPt.copyFrom(*ptInit);
    _problem->setInitPoint(true);
  }
  _problem->setNumThreads(OmpSettings::getOmpSettings().nrThreads());
  _problem->initializeSolve();
  _problem->solve();
}
void SDPAInterface::check() const
{
  std::vector<double> ZDotFI(_b.size(),0.0);
  //print y
  Vec xVec=x();
  INFO("Solution Check:")
  printMat("x",xVec);
  //print X
  for(int i=0; i<(int)_PSDCones.size(); i++) {
    Mat f=F(i);
    Mat z=Z(i);
    Mat fRef=F0(i);
    for(int j=0; j<_b.size(); j++) {
      ZDotFI[j]+=(FI(i,j)*z.transpose()).trace();
      fRef+=FI(i,j)*xVec[j];
    }
    printMat("z"+std::to_string(i),z);
    printMat("f"+std::to_string(i),f);
    printMat("fRef"+std::to_string(i),fRef);
    printMat("z"+std::to_string(i)+"*f"+std::to_string(i),z*f);
  }
  for(int j=0; j<_b.size(); j++) {
    INFOV("ZDotFI=%f BI=%f",ZDotFI[j],_b[j])
  }
}
template <typename T>
void SDPAInterface::setProblem(const typename PSDConeTraits<T>::Vec& b,const std::vector<typename PSDConeTraits<T>::PSDCone>& cones)
{
  _problem.reset(new SDPA);
  _problem->setParameterType(_type);
  _problem->inputConstraintNumber(b.size());
  _PSDCones.resize(cones.size());
  _problem->inputBlockNumber((int)_PSDCones.size());
  for(int i=0; i<(int)_PSDCones.size(); i++) {
    _PSDCones[i].resize(cones[i].size());
    for(int j=0; j<(int)cones[i].size(); j++)
      _PSDCones[i][j]=cones[i][j].template cast<double>();
    _problem->inputBlockSize(i+1,_PSDCones[i][0].rows());
    _problem->inputBlockType(i+1,SDPA::SDP);
  }
  _problem->initializeUpperTriangleSpace();
  //bVec
  _b=b.template cast<double>();
  for(int i=0; i<b.size(); i++)
    _problem->inputCVec(i+1,_b[i]);
  //entries
  for(int i=0; i<(int)_PSDCones.size(); i++)
    for(int j=0; j<(int)_PSDCones[i].size(); j++) {
      double sgn=j==0?-1:1;
      const SMat& F=_PSDCones[i][j];
      for(int k=0; k<(int)F.outerSize(); ++k)
        for(typename SMat::InnerIterator it(F,k); it; ++it)
          if(it.col()>=it.row())
            _problem->inputElement(j,i+1,it.row()+1,it.col()+1,sgn*it.value());
    }
  _problem->initializeUpperTriangle();
  _problem->initializeSolve();
}
void SDPAInterface::getSolution(SDPSolution& sol,scalarD eps) const
{
  sol._currentPt.reset(new Solutions);
  sol._currentPt->copyFrom(_problem->currentPt);
  int nrPSDCones=(int)_PSDCones.size(),nrFZ=0;
  for(sizeType i=0; i<nrPSDCones; i++)
    nrFZ+=_PSDCones[i][0].rows()*(_PSDCones[i][0].rows()+1)/2;
  sol._F.resize(nrFZ);
  sol._Z.resize(nrFZ);
  sol._X=x();
  sol._vssFZ1.resize(nrPSDCones);
  sol._vssFZ2.resize(nrPSDCones);
  sol._vssFZ3.resize(nrPSDCones);
  sol._sssF1.resize(nrPSDCones);
  sol._sssF2.resize(nrPSDCones);
  sol._sssZ1.resize(nrPSDCones);
  sol._sssZ2.resize(nrPSDCones);
  for(sizeType i=0,off=0; i<nrPSDCones; i++) {
    Matd FI=F(i);
    Matd ZI=Z(i);
    Vec FISVec=toSVec<scalarD>(FI);
    Vec ZISVec=toSVec<scalarD>(ZI);
    sol._F.segment(off,FISVec.size())=FISVec;
    sol._Z.segment(off,ZISVec.size())=ZISVec;
    //find eigenvalues
    Eigen::SelfAdjointEigenSolver<Matd> eig(FI+ZI);
    Vec EF=(eig.eigenvectors().transpose()*(FI*eig.eigenvectors())).diagonal();
    Vec EZ=(eig.eigenvectors().transpose()*(ZI*eig.eigenvectors())).diagonal();
    symmetricKroneckerEigensystemTrisection<scalarD>
    (eig.eigenvectors(),EF,EZ,
     sol._vssFZ1[i],sol._vssFZ2[i],sol._vssFZ3[i],
     sol._sssZ1[i],sol._sssZ2[i],sol._sssF1[i],sol._sssF2[i],eps);
    off+=FISVec.size();
  }
}
Eigen::Map<const SDPAInterface::Mat> SDPAInterface::Z(int bid) const
{
  int N=_PSDCones[bid][0].rows();
  return Eigen::Map<const SDPAInterface::Mat>(const_cast<SDPA&>(*_problem).getResultYMat(bid+1),N,N);
}
Eigen::Map<const SDPAInterface::Mat> SDPAInterface::F(int bid) const
{
  int N=_PSDCones[bid][0].rows();
  return Eigen::Map<const SDPAInterface::Mat>(const_cast<SDPA&>(*_problem).getResultXMat(bid+1),N,N);
}
Eigen::Map<const SDPAInterface::Vec> SDPAInterface::x() const
{
  return Eigen::Map<const Vec>(const_cast<SDPA&>(*_problem).getResultXVec(),_b.size());
}
//helper
SDPAInterface::Mat SDPAInterface::F0(int bid) const
{
  return _PSDCones[bid][0].toDense();
}
SDPAInterface::Mat SDPAInterface::FI(int bid,int mid) const
{
  return _PSDCones[bid][mid+1].toDense();
}
SDPAInterface::Mat SDPAInterface::convert(const sdpa::SparseMatrix& m) const
{
  Mat ret;
  ret.setZero(m.nRow,m.nCol);
  if(m.type==sdpa::SparseMatrix::DENSE) {
    for(int r=0,i=0; r<m.nRow; r++)
      for(int c=0; c<m.nCol; c++,i++)
        ret(r,c)=m.de_ele[i];
  } else {
    for(int i=0; i<m.NonZeroCount; i++)
      ret(m.row_index[i],m.column_index[i])=m.sp_ele[i];
  }
  return ret;
}
void SDPAInterface::printMat(const std::string& name,const Mat& m) const
{
  INFO(name.c_str())
  for(int r=0; r<m.rows(); r++) {
    for(int c=0; c<m.cols(); c++)
      std::cout << m(r,c) << " ";
    std::cout << std::endl;
  }
}

//instance
PRJ_BEGIN
template void SDPAInterface::setProblem<scalarF>(const typename PSDConeTraits<scalarF>::Vec& b,const std::vector<typename PSDConeTraits<scalarF>::PSDCone>& cones);
template void SDPAInterface::setProblem<scalarD>(const typename PSDConeTraits<scalarD>::Vec& b,const std::vector<typename PSDConeTraits<scalarD>::PSDCone>& cones);
PRJ_END
#endif

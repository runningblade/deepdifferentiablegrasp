#include "internal/SOSPolynomialScalarQ.h"
#include "SOSPolynomial.h"
#include "DebugGradient.h"
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

#define NO_CONSISTENCY_CHECK
#include "internal/SOSPolynomialSolve.h"
#include "internal/SOSPolynomialConvert.h"
#include "internal/SOSPolynomialRobustInversion.h"
#include "internal/SOSPolynomialIsZero.h"
//SOSTerm
template <typename T,char LABEL>
SOSTerm<T,LABEL>::SOSTerm() {}
template <typename T,char LABEL>
SOSTerm<T,LABEL>::SOSTerm(T val):_coef(val) {}
template <typename T,char LABEL>
SOSTerm<T,LABEL>::SOSTerm(T val,sizeType id,sizeType order):_coef(val) {
  if(order>0) {
    _id.assign(1,id);
    _order.assign(1,order);
  }
  consistencyCheck();
}
template <typename T,char LABEL>
SOSTerm<T,LABEL>::SOSTerm(const std::string& str) {
  *this<<str;
}
template <typename T,char LABEL>
bool SOSTerm<T,LABEL>::read(std::istream& is) {
  readBinaryData(_id,is);
  readBinaryData(_order,is);
  readBinaryData(_coef,is);
  return is.good();
}
template <typename T,char LABEL>
bool SOSTerm<T,LABEL>::write(std::ostream& os) const {
  writeBinaryData(_id,os);
  writeBinaryData(_order,os);
  writeBinaryData(_coef,os);
  return os.good();
}
template <typename T,char LABEL>
std::string SOSTerm<T,LABEL>::type() const {
  return typeid(SOSTerm).name();
}
//cmp
template <typename T,char LABEL>
bool SOSTerm<T,LABEL>::operator<(const SOSTerm& other) const {
  sizeType i=0;
  while(i<(sizeType)_id.size()&&i<(sizeType)other._id.size()) {
    if(_id[i]<other._id[i])
      return true;
    else if(_id[i]>other._id[i])
      return false;
    else {
      if(_order[i]<other._order[i])
        return true;
      else if(_order[i]>other._order[i])
        return false;
    }
    i++;
  }
  if(_id.size()<other._id.size())
    return true;
  else if(_id.size()>other._id.size())
    return false;
  else return false;
}
template <typename T,char LABEL>
bool SOSTerm<T,LABEL>::operator>(const SOSTerm& other) const {
  return other<*this;
}
template <typename T,char LABEL>
bool SOSTerm<T,LABEL>::operator==(const SOSTerm& other) const {
  return !(*this<other) && !(other<*this);
}
template <typename T,char LABEL>
bool SOSTerm<T,LABEL>::operator!=(const SOSTerm& other) const {
  return !(*this==other);
}
//op
template <typename T,char LABEL>
sizeType SOSTerm<T,LABEL>::nrVar() const
{
  if(_id.empty())
    return 0;
  else return _id.back()+1;
}
template <typename T,char LABEL>
sizeType SOSTerm<T,LABEL>::order() const
{
  if(_order.empty())
    return 0;
  else {
    sizeType order=0;
    for(sizeType i=0; i<(sizeType)_order.size(); i++)
      order+=_order[i];
    return order;
  }
}
template <typename T,char LABEL>
bool SOSTerm<T,LABEL>::hasId(sizeType id) const
{
  std::vector<sizeType>::const_iterator it=std::lower_bound(_id.begin(),_id.end(),id);
  return it!=_id.end() && *it==id;
}
template <typename T,char LABEL>
sizeType SOSTerm<T,LABEL>::order(sizeType id) const
{
  std::vector<sizeType>::const_iterator it=std::lower_bound(_id.begin(),_id.end(),id);
  sizeType off=it-_id.begin();
  return _order[off];
}
template <typename T,char LABEL>
SOSTerm<T,LABEL> SOSTerm<T,LABEL>::removeId(sizeType id) const
{
  SOSTerm ret=*this;
  std::vector<sizeType>::const_iterator it=std::lower_bound(ret._id.begin(),ret._id.end(),id);
  if(it!=ret._id.end() && *it==id) {
    sizeType off=it-ret._id.begin();
    ret._id.erase(ret._id.begin()+off);
    ret._order.erase(ret._order.begin()+off);
  }
  return ret;
}
template <typename T,char LABEL>
SOSTerm<T,LABEL> SOSTerm<T,LABEL>::operator*(T other) const {
  SOSTerm<T,LABEL> ret=*this;
  ret._coef*=other;
  return ret;
}
template <typename T,char LABEL>
SOSTerm<T,LABEL>& SOSTerm<T,LABEL>::operator*=(T other) {
  return *this=*this*other;
}
template <typename T,char LABEL>
SOSTerm<T,LABEL> SOSTerm<T,LABEL>::operator*(const SOSTerm& other) const {
  SOSTerm<T,LABEL> ret;
  sizeType i=0,j=0;
  while(i<(sizeType)_id.size() && j<(sizeType)other._id.size()) {
    if(_id[i]<other._id[j]) {
      ret.add(_id[i],_order[i]);
      i++;
    } else if(_id[i]>other._id[j]) {
      ret.add(other._id[j],other._order[j]);
      j++;
    } else {
      ret.add(_id[i],_order[i]+other._order[j]);
      i++;
      j++;
    }
  }
  while(i<(sizeType)_id.size()) {
    ret.add(_id[i],_order[i]);
    i++;
  }
  while(j<(sizeType)other._id.size()) {
    ret.add(other._id[j],other._order[j]);
    j++;
  }
  ret._coef=_coef*other._coef;
  ret.consistencyCheck();
  return ret;
}
template <typename T,char LABEL>
SOSTerm<T,LABEL>& SOSTerm<T,LABEL>::operator*=(const SOSTerm& other) {
  return *this=*this*other;
}
template <typename T,char LABEL>
SOSTerm<T,LABEL> SOSTerm<T,LABEL>::operator+(const SOSTerm& other) const {
  ASSERT(*this==other)
  SOSTerm<T,LABEL> ret=*this;
  ret._coef+=other._coef;
  ret.consistencyCheck();
  return ret;
}
template <typename T,char LABEL>
SOSTerm<T,LABEL>& SOSTerm<T,LABEL>::operator+=(const SOSTerm& other) {
  return *this=*this+other;
}
template <typename T,char LABEL>
SOSTerm<T,LABEL> SOSTerm<T,LABEL>::operator-(const SOSTerm& other) const {
  ASSERT(*this==other)
  SOSTerm<T,LABEL> ret=*this;
  ret._coef-=other._coef;
  ret.consistencyCheck();
  return ret;
}
template <typename T,char LABEL>
SOSTerm<T,LABEL>& SOSTerm<T,LABEL>::operator-=(const SOSTerm& other) {
  return *this=*this-other;
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL> SOSTerm<T,LABEL>::integrate(sizeType nrVar) const
{
  sizeType j=0;
  SOSPolynomial<T,LABEL> ret,p;
  for(sizeType i=0; i<nrVar; i++) {
    sizeType order=0;
    if(j<(sizeType)_id.size()&&_id[j]==i)
      order=_order[j++];
    p =SOSTerm(ScalarOfT<T>::convert(1/scalarD(order+1)),i+nrVar,order+1);
    p-=SOSTerm(ScalarOfT<T>::convert(1/scalarD(order+1)),i      ,order+1);
    if(i==0)
      ret=p;
    else ret*=p;
  }
  ASSERT(j==(sizeType)_id.size())
  return ret*SOSTerm(_coef);
}
template <typename T,char LABEL>
void SOSTerm<T,LABEL>::gradient(std::vector<SOSPolynomial<T,LABEL>>& grad) const
{
  for(sizeType i=0; i<(sizeType)_id.size(); i++) {
    SOSTerm<T,LABEL> t=*this;
    if(_id.empty())
      continue;
    else if(_order[i]>1) {
      t._order[i]--;
      t._coef*=ScalarOfT<T>::convert(_order[i]);
    } else {
      t._id.erase(t._id.begin()+i);
      t._order.erase(t._order.begin()+i);
    }
    grad[_id[i]]+=t;
  }
  for(sizeType i=0; i<(sizeType)grad.size(); i++)
    grad[i].consistencyCheck();
}
template <typename T,char LABEL>
T SOSTerm<T,LABEL>::integrate(const COLD& L,const COLD& U) const
{
  T ret=ScalarOfT<T>::convert(1);
  sizeType j=0;
  for(sizeType i=0; i<L.size(); i++) {
    sizeType order=0;
    if(j<(sizeType)_id.size()&&_id[j]==i)
      order=_order[j++];
    ret*=ScalarOfT<T>::convert((std::pow(U[i],order+1)-std::pow(L[i],order+1))/(order+1));
  }
  ASSERT(j==(sizeType)_id.size())
  return _coef*ret;
}
template <typename T,char LABEL>
template <typename HESS>
T SOSTerm<T,LABEL>::evalTpl(const COLD& x,VEC* grad,HESS* hess) const
{
  T ret=ScalarOfT<T>::convert(1),fgradX,fhessX,tmp;
  std::vector<T> xx0(_id.size(),ScalarOfT<T>::convert(1));
  std::vector<T> xx1(_id.size(),ScalarOfT<T>::convert(1));
  std::vector<T> xx2(_id.size(),ScalarOfT<T>::convert(1));
  //value
  for(sizeType i=0; i<(sizeType)_id.size(); i++) {
    xx0[i]=ScalarOfT<T>::convert(std::pow(x[_id[i]],_order[i]));
    ret*=xx0[i];
    if(i>0)
      xx1[i]=xx1[i-1]*xx0[i-1];
  }
  //gradient
  if(grad)
    for(sizeType i=(sizeType)_id.size()-1; i>=0; i--) {
      fgradX=ScalarOfT<T>::convert(_order[i]*std::pow(x[_id[i]],_order[i]-1));
      ParallelEvaluate<T>::gradient(*grad,_id[i],_coef*fgradX*xx1[i]*xx2[i]);
      if(i>0)
        xx2[i-1]=xx2[i]*xx0[i];
    }
  //hessian
  if(hess)
    for(sizeType i=0; i<(sizeType)_id.size(); i++) {
      if(_order[i]>1) {
        fhessX=ScalarOfT<T>::convert(_order[i]*(_order[i]-1)*std::pow(x[_id[i]],_order[i]-2));
        ParallelEvaluate<T>::hessian(*hess,_id[i],_id[i],_coef*fhessX*xx1[i]*xx2[i]);
      }
      fgradX=ScalarOfT<T>::convert(_order[i]*std::pow(x[_id[i]],_order[i]-1));
      tmp=xx1[i]*fgradX;
      for(sizeType j=i+1; j<(sizeType)_id.size(); j++) {
        fgradX=ScalarOfT<T>::convert(_order[j]*std::pow(x[_id[j]],_order[j]-1));
        ParallelEvaluate<T>::hessianSym(*hess,_id[i],_id[j],_coef*tmp*fgradX*xx2[j]);
        tmp*=xx0[j];
      }
    }
  return _coef*ret;
}
template <typename T,char LABEL>
T SOSTerm<T,LABEL>::eval(const COLD& x,VEC* grad,MAT* hess) const
{
  return evalTpl<MAT>(x,grad,hess);
}
template <typename T,char LABEL>
template <typename JAC>
T SOSTerm<T,LABEL>::evalJacTpl(sizeType row,const COLD& x,JAC* jac) const
{
  T ret=ScalarOfT<T>::convert(1),fgradX;
  std::vector<T> xx0(_id.size(),ScalarOfT<T>::convert(1));
  std::vector<T> xx1(_id.size(),ScalarOfT<T>::convert(1));
  std::vector<T> xx2(_id.size(),ScalarOfT<T>::convert(1));
  //value
  for(sizeType i=0; i<(sizeType)_id.size(); i++) {
    xx0[i]=ScalarOfT<T>::convert(std::pow(x[_id[i]],_order[i]));
    ret*=xx0[i];
    if(i>0)
      xx1[i]=xx1[i-1]*xx0[i-1];
  }
  //gradient
  if(jac)
    for(sizeType i=(sizeType)_id.size()-1; i>=0; i--) {
      fgradX=ScalarOfT<T>::convert(_order[i]*std::pow(x[_id[i]],_order[i]-1));
      ParallelEvaluate<T>::hessian(*jac,row,_id[i],_coef*fgradX*xx1[i]*xx2[i]);
      if(i>0)
        xx2[i-1]=xx2[i]*xx0[i];
    }
  return _coef*ret;
}
template <typename T,char LABEL>
T SOSTerm<T,LABEL>::evalJac(sizeType row,const COLD& x,STRIPS* jac) const
{
  return evalJacTpl(row,x,jac);
}
template <typename T,char LABEL>
T SOSTerm<T,LABEL>::evalJac(sizeType row,const COLD& x,MAT* jac) const
{
  return evalJacTpl(row,x,jac);
}
//io
template <typename T,char LABEL>
SOSTerm<T,LABEL>::operator T() const {
  ASSERT(_id.empty() && _order.empty())
  return _coef;
}
template <typename T,char LABEL>
SOSTerm<T,LABEL>::operator std::string() const {
  std::string ret="("+convert(_coef)+")";
  for(sizeType i=0; i<(sizeType)_id.size(); i++)
    ret+="*"+std::string(1,LABEL)+convert(_id[i])+"^"+convert(_order[i]);
  return ret;
}
template <typename T,char LABEL>
void SOSTerm<T,LABEL>::operator<<(std::string str) {
  boost::erase_all(str," ");
  boost::erase_all(str,"\n");
  _id.clear();
  _order.clear();

  //coef part
  sizeType j=str.size();
  while(j>0 && str[j-1]!=')')
    j--;
  if(j>0)
    convert(str.substr(1,j-2),_coef);
  else {
    size_t pos=str.find_first_of('*');
    if(pos==std::string::npos)
      j=str.size();
    else j=(sizeType)pos;
    convert(str.substr(0,j),_coef);
  }
  if(j==(sizeType)str.size())
    return;

  //other part
  std::istringstream iss(str.substr(j));
  char m,c,o;
  sizeType id,order;
  while(!iss.eof()) {
    iss >> m >> c >> id >> o >> order;
    ASSERT(m=='*' && c==LABEL && o=='^')
    add(id,order);
  }
}
//helper
template <typename T,char LABEL>
void SOSTerm<T,LABEL>::add(sizeType id,sizeType order) {
  std::vector<sizeType>::iterator it=std::lower_bound(_id.begin(),_id.end(),id);
  sizeType off=it-_id.begin();
  if(it==_id.end() || *it!=id) {
    _id.insert(off+_id.begin(),id);
    _order.insert(off+_order.begin(),order);
  } else {
    _order[off]+=order;
  }
}
template <typename T,char LABEL>
void SOSTerm<T,LABEL>::consistencyCheck() const {
#ifndef NO_CONSISTENCY_CHECK
  for(sizeType i=0; i<(sizeType)_id.size()-1; i++) {
    ASSERT(_id[i]<_id[i+1])
  }
  for(sizeType i=0; i<(sizeType)_id.size(); i++) {
    ASSERT(_order[i]>0)
  }
#endif
}

//SOSPolynomial
template <typename T,char LABEL>
SOSPolynomial<T,LABEL>::SOSPolynomial() {}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL>::SOSPolynomial(T other):_terms(1,SOSTerm<T,LABEL>(other)) {}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL>::SOSPolynomial(const SOSTerm<T,LABEL>& other):_terms(1,other) {}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL>::SOSPolynomial(const std::string& str) {
  *this<<str;
}
template <typename T,char LABEL>
bool SOSPolynomial<T,LABEL>::read(std::istream& is,IOData* dat) {
  readBinaryData(_terms,is);
  return is.good();
}
template <typename T,char LABEL>
bool SOSPolynomial<T,LABEL>::write(std::ostream& os,IOData* dat) const {
  writeBinaryData(_terms,os);
  return os.good();
}
template <typename T,char LABEL>
boost::shared_ptr<SerializableBase> SOSPolynomial<T,LABEL>::copy() const
{
  return boost::shared_ptr<SerializableBase>(new SOSPolynomial<T,LABEL>);
}
template <typename T,char LABEL>
std::string SOSPolynomial<T,LABEL>::type() const {
  return typeid(SOSPolynomial).name();
}
//cmp
template <typename T,char LABEL>
bool SOSPolynomial<T,LABEL>::operator==(const SOSPolynomial& other) const
{
  return false;
}
//op
template <typename T,char LABEL>
sizeType SOSPolynomial<T,LABEL>::nrVar() const
{
  return nrVar<LABEL>();
}
template <typename T,char LABEL>
sizeType SOSPolynomial<T,LABEL>::order() const
{
  return order<LABEL>();
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL> SOSPolynomial<T,LABEL>::operator*(const T& other) const {
  SOSPolynomial ret=*this;
  return ret*=other;
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL>& SOSPolynomial<T,LABEL>::operator*=(const T& other) {
  for(sizeType i=0; i<(sizeType)_terms.size(); i++)
    _terms[i]._coef*=other;
  return *this;
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL> SOSPolynomial<T,LABEL>::operator*(const SOSTerm<T,LABEL>& other) const {
  SOSPolynomial ret=*this;
  return ret*=other;
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL>& SOSPolynomial<T,LABEL>::operator*=(const SOSTerm<T,LABEL>& other) {
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<(sizeType)_terms.size(); i++)
    _terms[i]*=other;
  std::sort(_terms.begin(),_terms.end());
  consistencyCheck();
  return *this;
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL> SOSPolynomial<T,LABEL>::operator*(const SOSPolynomial& other) const {
  SOSPolynomial ret=*this;
  return ret*=other;
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL>& SOSPolynomial<T,LABEL>::operator*=(const SOSPolynomial& other) {
  std::vector<SOSPolynomial> otherByTerms(other._terms.size());
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<(sizeType)other._terms.size(); i++)
    otherByTerms[i]=*this*other._terms[i];
  sum(otherByTerms);
  return *this;
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL> SOSPolynomial<T,LABEL>::operator+(const SOSTerm<T,LABEL>& other) const {
  SOSPolynomial ret=*this;
  return ret+=other;
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL>& SOSPolynomial<T,LABEL>::operator+=(const SOSTerm<T,LABEL>& other) {
  typename std::vector<SOSTerm<T,LABEL>>::iterator it=std::lower_bound(_terms.begin(),_terms.end(),other);
  if(it==_terms.end() || *it!=other)
    _terms.insert(it,other);
  else it->_coef+=other._coef;
  return *this;
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL> SOSPolynomial<T,LABEL>::operator+(const SOSPolynomial& other) const {
  sizeType i=0,j=0;
  SOSPolynomial ret;
  while(i<(sizeType)_terms.size() && j<(sizeType)other._terms.size()) {
    if(_terms[i]<other._terms[j]) {
      ret.add(_terms[i]);
      i++;
    } else if(_terms[i]>other._terms[j]) {
      ret.add(other._terms[j]);
      j++;
    } else {
      ret.add(_terms[i]+other._terms[j]);
      i++;
      j++;
    }
  }
  while(i<(sizeType)_terms.size()) {
    ret.add(_terms[i]);
    i++;
  }
  while(j<(sizeType)other._terms.size()) {
    ret.add(other._terms[j]);
    j++;
  }
  ret.consistencyCheck();
  return ret;
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL>& SOSPolynomial<T,LABEL>::operator+=(const SOSPolynomial& other) {
  return *this=*this+other;
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL> SOSPolynomial<T,LABEL>::operator-(const SOSTerm<T,LABEL>& other) const {
  SOSPolynomial ret=*this;
  return ret-=other;
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL>& SOSPolynomial<T,LABEL>::operator-=(const SOSTerm<T,LABEL>& other) {
  SOSTerm<T,LABEL> otherNeg=other;
  otherNeg._coef*=ScalarOfT<T>::convert(-1);
  return operator+=(otherNeg);
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL> SOSPolynomial<T,LABEL>::operator-(const SOSPolynomial& other) const {
  SOSPolynomial ret=*this;
  return ret-=other;
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL>& SOSPolynomial<T,LABEL>::operator-=(const SOSPolynomial& other) {
  return operator+=(other*ScalarOfT<T>::convert(-1));
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL> SOSPolynomial<T,LABEL>::integrate() const
{
  if(_terms.empty())
    return SOSPolynomial();
  SOSPolynomial ret=_terms[0].integrate(nrVar());
  for(sizeType i=1; i<(sizeType)_terms.size(); i++)
    ret+=_terms[i].integrate(nrVar());
  return ret;
}
template <typename T,char LABEL>
std::vector<SOSPolynomial<T,LABEL>> SOSPolynomial<T,LABEL>::gradient() const
{
  std::vector<SOSPolynomial<T,LABEL>> ret(nrVar());
  for(sizeType i=0; i<(sizeType)_terms.size(); i++)
    _terms[i].gradient(ret);
  return ret;
}
template <typename T,char LABEL>
std::vector<std::vector<SOSPolynomial<T,LABEL>>> SOSPolynomial<T,LABEL>::hessian() const
{
  std::vector<std::vector<SOSPolynomial>> ret;
  std::vector<SOSPolynomial<T,LABEL>> g=gradient();
  for(sizeType i=0; i<(sizeType)g.size(); i++)
    ret.push_back(g[i].gradient());
  return ret;
}
template <typename T,char LABEL>
void SOSPolynomial<T,LABEL>::sum(const std::vector<SOSPolynomial>& polys)
{
  //merge
  _terms.clear();
  for(sizeType i=0; i<(sizeType)polys.size(); i++)
    _terms.insert(_terms.end(),polys[i]._terms.begin(),polys[i]._terms.end());
  if(_terms.empty())
    return;
  //sort
  std::sort(_terms.begin(),_terms.end());
  //compact
  sizeType j=0;
  for(sizeType i=1; i<(sizeType)_terms.size(); i++)
    if(_terms[i]==_terms[j])
      _terms[j]._coef+=_terms[i]._coef;
    else _terms[++j]=_terms[i];
  _terms.resize(j+1);
  consistencyCheck();
}
template <typename T,char LABEL>
typename SOSPolynomial<T,LABEL>::VEC SOSPolynomial<T,LABEL>::gradientCoef() const
{
  VEC gc=VEC::Constant(nrVar(),ScalarOfT<T>::convert(0));
  std::vector<SOSPolynomial<T,LABEL>> g=gradient();
  for(sizeType i=0; i<nrVar(); i++)
    gc[i]=g.at(i).operator T();
  return gc;
}
template <typename T,char LABEL>
typename SOSPolynomial<T,LABEL>::MAT SOSPolynomial<T,LABEL>::hessianCoef() const
{
  MAT hc=MAT::Constant(nrVar(),nrVar(),ScalarOfT<T>::convert(0));
  std::vector<std::vector<SOSPolynomial>> h=hessian();
  for(sizeType i=0; i<nrVar(); i++)
    for(sizeType j=0; j<(sizeType)h.at(i).size(); j++)
      hc(i,j)=h.at(i).at(j).operator T();
  return hc;
}
template <typename T,char LABEL>
typename SOSPolynomial<T,LABEL>::MAT SOSPolynomial<T,LABEL>::JTJCoef() const
{
  sizeType nrT=(sizeType)_terms.size();
  MAT JTJ=MAT::Constant(nrT,nrT,ScalarOfT<T>::convert(0));
  OMP_PARALLEL_FOR_
  for(sizeType r=0; r<nrT; r++)
    for(sizeType c=0; c<nrT; c++)
      JTJ(r,c)=_terms[r]._coef*_terms[c]._coef;
  return JTJ;
}
template <typename T,char LABEL>
T SOSPolynomial<T,LABEL>::integrate(const COLD& L,const COLD& U) const
{
  if(_terms.empty())
    return T();
  std::vector<T> termsI(_terms.size()),tmp;
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<(sizeType)_terms.size(); i++)
    termsI[i]=_terms[i].integrate(L,U);
  while(termsI.size()>1) {
    tmp.resize((termsI.size()+1)/2);
    OMP_PARALLEL_FOR_
    for(sizeType k=0; k<(sizeType)tmp.size(); k++)
      if(k*2==(sizeType)termsI.size()-1)
        tmp[k]=termsI[k*2];
      else tmp[k]=termsI[k*2]+termsI[k*2+1];
    termsI.swap(tmp);
  }
  return termsI[0];
}
template <typename T,char LABEL>
T SOSPolynomial<T,LABEL>::eval(const COLD& x,VEC* grad,MAT* hess) const
{
  return ParallelEvaluate<T>::eval(*this,x,grad,hess);
}
template <typename T,char LABEL>
T SOSPolynomial<T,LABEL>::evalTrips(const COLD& x,VEC* grad,STRIPS* hess) const
{
  return ParallelEvaluate<T>::eval(*this,x,grad,hess);
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL> SOSPolynomial<T,LABEL>::rename(const std::vector<sizeType>& ids) const
{
  //collect ids
  std::map<sizeType,sizeType> idMap;
  for(sizeType i=0; i<(sizeType)ids.size(); i++)
    idMap[i]=ids[i];
  //remap
  SOSPolynomial ret=*this;
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<(sizeType)ret._terms.size(); i++) {
    const SOSTerm<T,LABEL>& t0=_terms[i];
    SOSTerm<T,LABEL>& t=ret._terms[i];
    t._id.clear();
    t._order.clear();
    for(sizeType d=0; d<(sizeType)t0._id.size(); d++)
      t.add(idMap.find(t0._id[d])->second,t0._order[d]);
  }
  std::sort(ret._terms.begin(),ret._terms.end());
  ret.consistencyCheck();
  return ret;
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL> SOSPolynomial<T,LABEL>::compact(std::vector<sizeType>& ids) const
{
  //collect ids
  std::map<sizeType,sizeType> idMap;
  for(sizeType i=0; i<(sizeType)_terms.size(); i++) {
    const SOSTerm<T,LABEL>& t0=_terms[i];
    for(sizeType j=0; j<(sizeType)t0._id.size(); j++)
      idMap[t0._id[j]]=-1;
  }
  //find maxId
  ids.clear();
  sizeType off=0;
  for(std::map<sizeType,sizeType>::iterator
      beg=idMap.begin(),end=idMap.end(); beg!=end; beg++) {
    ids.push_back(beg->first);
    beg->second=off++;
  }
  //remap
  SOSPolynomial ret=*this;
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<(sizeType)ret._terms.size(); i++) {
    const SOSTerm<T,LABEL>& t0=_terms[i];
    SOSTerm<T,LABEL>& t=ret._terms[i];
    t._id.clear();
    t._order.clear();
    for(sizeType d=0; d<(sizeType)t0._id.size(); d++)
      t.add(idMap.find(t0._id[d])->second,t0._order[d]);
  }
  ret.consistencyCheck();
  return ret;
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL> SOSPolynomial<T,LABEL>::setAllCoef(T coef) const
{
  SOSPolynomial ret=*this;
  for(sizeType i=0; i<(sizeType)ret._terms.size(); i++)
    ret._terms[i]._coef=coef;
  return ret;
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL> SOSPolynomial<T,LABEL>::removeZero() const
{
  return IsZero<SOSPolynomial<T,LABEL>>::removeZero(*this);
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL> SOSPolynomial<T,LABEL>::removeVariableId(sizeType id) const
{
  SOSPolynomial ret=*this;
  for(sizeType i=0; i<(sizeType)ret._terms.size(); i++) {
    SOSTerm<T,LABEL>& t=ret._terms[i];
    for(sizeType j=0; j<(sizeType)t._id.size(); j++) {
      ASSERT(t._id[j]!=id)
      if(t._id[j]>id)
        t._id[j]--;
    }
  }
  return ret;
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL> SOSPolynomial<T,LABEL>::linearConstraint(sizeType id,const SOSPolynomial& cons) const
{
  SOSPolynomial ret;
  for(sizeType i=0; i<(sizeType)_terms.size(); i++) {
    SOSTerm<T,LABEL> t=_terms[i];
    if(t.hasId(id)) {
      sizeType order=t.order(id);
      SOSTerm<T,LABEL> t2=t.removeId(id);
      SOSPolynomial p=cons;
      for(sizeType i=1; i<order; i++)
        p*=cons;
      ret+=p*t2;
    } else {
      ret+=t;
    }
  }
  return ret;
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL> SOSPolynomial<T,LABEL>::linearTransform(const std::map<sizeType,SOSPolynomial>& cons) const
{
  SOSPolynomial ret;
  for(sizeType i=0; i<(sizeType)_terms.size(); i++) {
    SOSTerm<T,LABEL> t=_terms[i];
    SOSPolynomial sRet=SOSTerm<T,LABEL>(t._coef);
    for(sizeType d=0; d<(sizeType)t._id.size(); d++)
      for(sizeType o=0; o<(sizeType)t._order[d]; o++)
        sRet*=cons.find(t._id[d])->second;
    ret+=sRet;
  }
  return ret;
}
template <typename T,char LABEL>
typename SOSPolynomial<T,LABEL>::COLD SOSPolynomial<T,LABEL>::solve(const std::vector<SOSPolynomial>& LHS,const std::vector<SOSPolynomial>& RHS)
{
  sizeType nrEQ=0;
  typename Objective<typename ScalarOfT<T>::Type>::STrips Lss,Rss;
  for(sizeType i=0; i<(sizeType)LHS.size(); i++)
    Solve<SOSPolynomial>::solve(nrEQ,Lss,Rss,LHS[i],RHS[i]);

  typename Objective<typename ScalarOfT<T>::Type>::SMat LHSM,RHSM;
  LHSM.resize(nrEQ,nrEQ);
  LHSM.setFromTriplets(Lss.begin(),Lss.end());
  RHSM.resize(nrEQ,1);
  RHSM.setFromTriplets(Rss.begin(),Rss.end());

  COLD RHSD=RHSM.toDense();
  MATD LHSD=LHSM.toDense();
  //std::cout << LHSD << std::endl << RHSD << std::endl;
  //analyze nullspace rows
  sizeType nrRow=0;
  for(sizeType i=0; i<LHSD.rows(); i++)
    if(LHSD.row(i).cwiseAbs().maxCoeff()!=0) {
      LHSD.row(nrRow)=LHSD.row(i);
      RHSD[nrRow++]=RHSD[i];
    } else {
      ASSERT(RHSD[i]==0)
    }
  LHSD=LHSD.block(0,0,nrRow,nrRow).eval();
  RHSD=RHSD.segment(0,nrRow).eval();
  COLD ret=RobustInversion<T,LABEL>::eval(LHSD)*RHSD;
  //std::cout << (LHSD*ret-RHSD) << std::endl;
  return ret;
}
//io
template <typename T,char LABEL>
bool SOSPolynomial<T,LABEL>::read(std::vector<VEC>& v,std::istream& is)
{
  sizeType sz;
  readBinaryData(sz,is);
  v.resize(sz);
  for(sizeType i=0; i<sz; i++)
    read(v[i],is);
  return is.good();
}
template <typename T,char LABEL>
bool SOSPolynomial<T,LABEL>::read(std::vector<MAT>& m,std::istream& is)
{
  sizeType sz;
  readBinaryData(sz,is);
  m.resize(sz);
  for(sizeType i=0; i<sz; i++)
    read(m[i],is);
  return is.good();
}
template <typename T,char LABEL>
bool SOSPolynomial<T,LABEL>::write(const std::vector<VEC>& v,std::ostream& os)
{
  sizeType sz=v.size();
  writeBinaryData(sz,os);
  for(sizeType i=0; i<sz; i++)
    write(v[i],os);
  return os.good();
}
template <typename T,char LABEL>
bool SOSPolynomial<T,LABEL>::write(const std::vector<MAT>& m,std::ostream& os)
{
  sizeType sz=m.size();
  writeBinaryData(sz,os);
  for(sizeType i=0; i<sz; i++)
    write(m[i],os);
  return os.good();
}
template <typename T,char LABEL>
bool SOSPolynomial<T,LABEL>::read(VEC& v,std::istream& is)
{
  MAT m;
  bool ret=read(m,is);
  v=m;
  return ret;
}
template <typename T,char LABEL>
bool SOSPolynomial<T,LABEL>::read(MAT& m,std::istream& is)
{
  sizeType rr,cc;
  readBinaryData(rr,is);
  readBinaryData(cc,is);
  m.resize(rr,cc);
  for(sizeType r=0; r<m.rows(); r++)
    for(sizeType c=0; c<m.cols(); c++)
      readBinaryData(m(r,c),is);
  return is.good();
}
template <typename T,char LABEL>
bool SOSPolynomial<T,LABEL>::write(const VEC& v,std::ostream& os)
{
  return write((MAT)v,os);
}
template <typename T,char LABEL>
bool SOSPolynomial<T,LABEL>::write(const MAT& m,std::ostream& os)
{
  writeBinaryData((sizeType)m.rows(),os);
  writeBinaryData((sizeType)m.cols(),os);
  for(sizeType r=0; r<m.rows(); r++)
    for(sizeType c=0; c<m.cols(); c++)
      writeBinaryData(m(r,c),os);
  return os.good();
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL>::operator T() const {
  if(_terms.empty())
    return ScalarOfT<T>::convert(0);
  else {
    ASSERT(_terms.size()==1 && _terms[0]._id.empty())
    return _terms[0]._coef;
  }
}
template <typename T,char LABEL>
SOSPolynomial<T,LABEL>::operator std::string() const {
  if(_terms.empty())
    return std::string();
  std::string ret=convert(_terms[0]);
  for(sizeType i=1; i<(sizeType)_terms.size(); i++)
    ret+="+\n"+convert(_terms[i]);
  return ret+"\n";
}
template <typename T,char LABEL>
std::string SOSPolynomial<T,LABEL>::compactString() const {
  SOSPolynomial ret=removeZero();
  std::string str(ret);
  boost::erase_all(str," ");
  boost::erase_all(str,"\n");
  return str;
}
template <typename T,char LABEL>
void SOSPolynomial<T,LABEL>::operator<<(std::string str) {
  _terms.clear();
  boost::erase_all(str," ");
  boost::erase_all(str,"\n");
  int depth=0;
  std::vector<sizeType> cut;
  cut.push_back(-1);
  for(sizeType i=0; i<(sizeType)str.size(); i++) {
    if(str[i]=='(')
      depth++;
    else if(str[i]==')')
      depth--;
    else if(str[i]=='+'&&depth==0)
      cut.push_back(i);
  }
  cut.push_back(str.size());
  for(sizeType i=1; i<(sizeType)cut.size(); i++) {
    sizeType p0=cut[i-1]+1,p1=cut[i];
    _terms.push_back(SOSTerm<T,LABEL>(str.substr(p0,p1-p0)));
  }
  std::sort(_terms.begin(),_terms.end());
}
//debug
template <typename T,char LABEL>
void SOSPolynomial<T,LABEL>::debugIntegrate() const {
  COLD L=COLD::Random(nrVar()),U=COLD::Random(nrVar()),LU=concat(L,U);
  T polyIEval=integrate().eval(LU),polyIEvalRef=polyIEval-integrate(L,U);
  std::cout << "Integrate: " << convert(polyIEval) << " err: " << convert(polyIEvalRef) << std::endl;
}
template <typename T,char LABEL>
void SOSPolynomial<T,LABEL>::debugGradient() const {
  COLD x=COLD::Random(nrVar());
  VEC g=VEC::Constant(nrVar(),ScalarOfT<T>::convert(0));
  eval(x,&g);
  std::vector<SOSPolynomial> grad=gradient();
  for(sizeType i=0; i<(sizeType)grad.size(); i++) {
    T gradV=grad[i].eval(x),gradVRef=gradV-g[i];
    std::cout << "GradientA: " << convert(gradV) << " err: " << convert(gradVRef) << std::endl;
  }
}
template <typename T,char LABEL>
void SOSPolynomial<T,LABEL>::debugEval() const {
  DEFINE_NUMERIC_DELTA
  COLD x=COLD::Random(nrVar());
  COLD dx=COLD::Random(nrVar());
  VEC g=VEC::Constant(nrVar(),ScalarOfT<T>::convert(0)),g2=g;
  MAT h=MAT::Constant(nrVar(),nrVar(),ScalarOfT<T>::convert(0));
  T f=eval(x,&g,&h);
  T f2=eval(x+dx*DELTA,&g2,NULL);
  {
    T gdxV=ScalarOfT<T>::convert(0),gdxVRef=ScalarOfT<T>::convert(0);
    for(sizeType i=0; i<g.size(); i++) {
      gdxV+=g[i]*ScalarOfT<T>::convert(dx[i]);
      gdxVRef+=g[i]*ScalarOfT<T>::convert(dx[i]);
    }
    gdxVRef-=(f2-f)*ScalarOfT<T>::convert(1/DELTA);
    std::cout << "Gradient: " << convert(gdxV) << " err: " << convert(gdxVRef) << std::endl;
  }
  {
    VEC hdxV=VEC::Constant(nrVar(),ScalarOfT<T>::convert(0));
    VEC hdxVRef=VEC::Constant(nrVar(),ScalarOfT<T>::convert(0));
    for(sizeType i=0; i<g.size(); i++)
      for(sizeType j=0; j<g.size(); j++) {
        hdxV[i]+=h(i,j)*ScalarOfT<T>::convert(dx[j]);
        hdxVRef[i]+=h(i,j)*ScalarOfT<T>::convert(dx[j]);
      }
    hdxVRef-=(g2-g)*ScalarOfT<T>::convert(1/DELTA);
    T hdxVNorm=ScalarOfT<T>::convert(0),hdxVRefNorm=ScalarOfT<T>::convert(0);
    for(sizeType i=0; i<hdxV.size(); i++) {
      hdxVNorm+=hdxV[i]*hdxV[i];
      hdxVRefNorm+=hdxVRef[i]*hdxVRef[i];
    }
    std::cout << "Hessian: " << convert(hdxVNorm) << " err: " << convert(hdxVRefNorm) << std::endl;
  }
}
//helper
template <typename T,char LABEL>
void SOSPolynomial<T,LABEL>::add(const SOSTerm<T,LABEL>& other) {
  typename std::vector<SOSTerm<T,LABEL>>::iterator it=std::lower_bound(_terms.begin(),_terms.end(),other);
  if(it==_terms.end()||*it!=other)
    _terms.insert(it,other);
  else *it+=other;
}
template <typename T,char LABEL>
void SOSPolynomial<T,LABEL>::consistencyCheck() const {
#ifndef NO_CONSISTENCY_CHECK
  for(sizeType i=0; i<(sizeType)_terms.size()-1; i++) {
    ASSERT(_terms[i]<_terms[i+1])
  }
  for(sizeType i=0; i<(sizeType)_terms.size(); i++)
    _terms[i].consistencyCheck();
#endif
}

//force instance
DECL_POLYTYPES(scalarD,D)
DECL_POLYTYPES(scalarQ,Q)
#define INSTANCE_POLY(TT,POSTFIX)                   \
template class SOSTerm<TT,'V'>;                     \
template class SOSPolynomial<TT,'V'>;               \
template class SOSTerm<PolyV##POSTFIX,'x'>;         \
template class SOSPolynomial<PolyV##POSTFIX,'x'>;   \
template class SOSTerm<PolyVX##POSTFIX,'a'>;        \
template class SOSPolynomial<PolyVX##POSTFIX,'a'>;  \
template class SOSTerm<TT,'x'>;                     \
template class SOSPolynomial<TT,'x'>;               \
template class SOSTerm<PolyX##POSTFIX,'a'>;         \
template class SOSPolynomial<PolyX##POSTFIX,'a'>;
INSTANCE_POLY(scalarD,D)
#ifdef MIXED_ACCURACY
#ifndef QUADMATH_SUPPORT
INSTANCE_POLY(scalarQ,Q)
#endif
#endif

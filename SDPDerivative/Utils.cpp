#include "Utils.h"
#include <CommonFile/IOBasic.h>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/version.hpp>
#include <cctype>

PRJ_BEGIN

size_t Hash::operator()(const Vec2i& key) const
{
  boost::hash<sizeType> h;
  return h(key[0])+h(key[1]);
}
size_t Hash::operator()(const Vec3i& key) const
{
  boost::hash<sizeType> h;
  return h(key[0])+h(key[1])+h(key[2]);
}
size_t Hash::operator()(const Vec4i& key) const
{
  boost::hash<sizeType> h;
  return h(key[0])+h(key[1])+h(key[2])+h(key[3]);
}
size_t Hash::operator()(const Coli& key) const
{
  boost::hash<sizeType> h;
  size_t ret=0;
  for(sizeType i=0; i<key.size(); i++)
    ret+=h(key[i]);
  return ret;
}
//string
bool beginsWith(const std::string& name,const std::string& regex)
{
  return name.size() >= regex.size() && name.substr(0,regex.size()) == regex;
}
bool endsWith(const std::string& name,const std::string& regex)
{
  return name.size() >= regex.size() && name.substr(name.size()-regex.size(),regex.size()) == regex;
}
//filesystem
bool notDigit(char c)
{
  return !std::isdigit(c);
}
bool lessDirByNumber(boost::filesystem::path A,boost::filesystem::path B)
{
  std::string strA=A.string();
  std::string::iterator itA=std::remove_if(strA.begin(),strA.end(),notDigit);
  strA.erase(itA,strA.end());

  std::string strB=B.string();
  std::string::iterator itB=std::remove_if(strB.begin(),strB.end(),notDigit);
  strB.erase(itB,strB.end());

  return boost::lexical_cast<sizeType>(strA) < boost::lexical_cast<sizeType>(strB);
}
bool exists(const boost::filesystem::path& path)
{
  return boost::filesystem::exists(path);
}
void remove(const boost::filesystem::path& path)
{
  if(boost::filesystem::exists(path))
    try {
      boost::filesystem::remove(path);
    } catch(...) {}
}
void removeDir(const boost::filesystem::path& path)
{
  if(boost::filesystem::exists(path))
    try {
      boost::filesystem::remove_all(path);
    } catch(...) {}
}
void create(const boost::filesystem::path& path)
{
  if(!boost::filesystem::exists(path))
    try {
      boost::filesystem::create_directory(path);
    } catch(...) {}
}
void recreate(const boost::filesystem::path& path)
{
  removeDir(path);
  try {
    boost::filesystem::create_directory(path);
  } catch(...) {}
}
std::vector<boost::filesystem::path> files(const boost::filesystem::path& path)
{
  std::vector<boost::filesystem::path> ret;
  for(boost::filesystem::directory_iterator beg(path),end; beg!=end; beg++)
    if(boost::filesystem::is_regular_file(*beg))
      ret.push_back(*beg);
  return ret;
}
std::vector<boost::filesystem::path> directories(const boost::filesystem::path& path)
{
  std::vector<boost::filesystem::path> ret;
  for(boost::filesystem::directory_iterator beg(path),end; beg!=end; beg++)
    if(boost::filesystem::is_directory(*beg))
      ret.push_back(*beg);
  return ret;
}
void sortFilesByNumber(std::vector<boost::filesystem::path>& files)
{
  sort(files.begin(),files.end(),lessDirByNumber);
}
bool isDir(const boost::filesystem::path& path)
{
  return boost::filesystem::is_directory(path);
}
size_t fileSize(const boost::filesystem::path& path)
{
  return (size_t)boost::filesystem::file_size(path);
}
std::vector<std::string> toParams(sizeType argc,char** argv)
{
  std::vector<std::string> params;
  for(sizeType i=0; i<argc; i++)
    params.push_back(argv[i]);
  return params;
}
std::string parseProps(sizeType argc,char** argv,boost::property_tree::ptree& pt)
{
  return parseProps(toParams(argc,argv),pt);
}
std::string parseProps(const std::vector<std::string>& params,boost::property_tree::ptree& pt)
{
  std::string addParam;
  for(sizeType i=0; i<(sizeType)params.size(); i++) {
    const std::string& str=params[i];
    size_t pos=str.find("=");
    if(pos != std::string::npos) {
      pt.put<std::string>(str.substr(0,pos),str.substr(pos+1));
      addParam+="_"+str;
    }
  }
  return addParam+"_";
}
//property tree
void readPtreeBinary(boost::property_tree::ptree& pt,std::istream& is)
{
  std::string str;
  readBinaryData(str,is);
  std::istringstream iss(str);
  boost::property_tree::read_xml(iss,pt);
}
void writePtreeBinary(const boost::property_tree::ptree& pt,std::ostream& os)
{
  std::ostringstream oss;
  boost::property_tree::write_xml(oss,pt);
  writeBinaryData(oss.str(),os);
}
void readPtreeAscii(boost::property_tree::ptree& pt,const boost::filesystem::path& path)
{
  boost::property_tree::read_xml(path.string(),pt);
}
void writePtreeAscii(const boost::property_tree::ptree& pt,const boost::filesystem::path& path,const std::string& suffix)
{
  boost::filesystem::ofstream os(path);
  writePtreeAscii(pt,os,suffix);
}
void writePtreeAscii(const boost::property_tree::ptree& pt,std::ostream& os,const std::string& suffix)
{
#if BOOST_VERSION >= 105600
  boost::property_tree::xml_writer_settings<std::string> settings('\t',1);
#else
  boost::property_tree::xml_writer_settings<boost::property_tree::ptree::key_type::value_type> settings('\t',1);
#endif
  //suffix
  if(suffix == "xml")
    boost::property_tree::write_xml(os,pt,settings);
  else if(suffix == "ini")
    boost::property_tree::write_ini(os,pt);
  else {
    ASSERT_MSGV(false,"Unsupported suffix for writing ptree %s!",suffix.c_str())
  }
}

PRJ_END

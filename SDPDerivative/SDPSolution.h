#ifndef SDP_SOLUTION_H
#define SDP_SOLUTION_H

namespace sdpa
{
class Solutions;
}
#include "SOSProblem.h"
//SDPA
#ifdef SDPA_SUPPORT
#define SDPA_FLOAT double
PRJ_BEGIN
FORCE_INLINE static scalarD SDPA2ScalarD(SDPA_FLOAT in) {
  return in;
}
template <typename T,int R,int C>
FORCE_INLINE static Eigen::Matrix<T,R,C> SDPA2ScalarD(const Eigen::Matrix<SDPA_FLOAT,R,C>& in) {
  return in.template cast<T>();
}
template <typename T>
FORCE_INLINE static Eigen::SparseMatrix<T,0,sizeType> SDPA2ScalarD(const Eigen::SparseMatrix<SDPA_FLOAT,0,sizeType>& in) {
  return in.template cast<T>();
}
template <typename T,int R,int C>
FORCE_INLINE static typename Eigen::Matrix<SDPA_FLOAT,R,C> scalarD2SDPA(const Eigen::Matrix<T,R,C>& in) {
  return in.template cast<SDPA_FLOAT>();
}
template <typename T>
FORCE_INLINE static Eigen::SparseMatrix<SDPA_FLOAT,0,sizeType> scalarD2SDPA(const Eigen::SparseMatrix<T,0,sizeType>& in) {
  return in.template cast<SDPA_FLOAT>();
}
PRJ_END
#endif
//SDPA-DD
#ifdef SDPA_DD_SUPPORT
#include <sdpa-dd/sdpa-dd-7.1.2/sdpa_io.h>
namespace std
{
FORCE_INLINE static dd_real sqrt(dd_real x) {
  return ::sqrt(x);
}
FORCE_INLINE static dd_real abs(dd_real x) {
  return ::abs(x);
}
}
#define SDPA_FLOAT dd_real
PRJ_BEGIN
FORCE_INLINE static scalarD SDPA2ScalarD(SDPA_FLOAT in) {
  return in.x[0];
}
template <typename T,int R,int C>
FORCE_INLINE static Eigen::Matrix<T,R,C> SDPA2ScalarD(const Eigen::Matrix<SDPA_FLOAT,R,C>& in) {
  typename Eigen::Matrix<T,R,C> ret;
  ret.resize(in.rows(),in.cols());
  for(sizeType r=0;r<in.rows();r++)
    for(sizeType c=0;c<in.cols();c++)
      ret(r,c)=in(r,c).x[0];
  return ret;
}
template <typename T>
FORCE_INLINE static Eigen::SparseMatrix<T,0,sizeType> SDPA2ScalarD(const Eigen::SparseMatrix<SDPA_FLOAT,0,sizeType>& in) {
  typename Eigen::SparseMatrix<T,0,sizeType> ret;
  typedef Eigen::Triplet<T,sizeType> STrip;
  typedef ParallelVector<STrip> STrips;
  STrips trips;
  ret.resize(in.rows(),in.cols());
  for(sizeType k=0; k<in.outerSize(); ++k)
    for(typename Eigen::SparseMatrix<SDPA_FLOAT,0,sizeType>::InnerIterator it(in,k); it; ++it)
      trips.push_back(STrip(it.row(),it.col(),it.value().x[0]));
  ret.setFromTriplets(trips.begin(),trips.end());
  return ret;
}
template <typename T,int R,int C>
FORCE_INLINE static typename Eigen::Matrix<SDPA_FLOAT,R,C> scalarD2SDPA(const Eigen::Matrix<T,R,C>& in) {
  typename Eigen::Matrix<SDPA_FLOAT,R,C> ret;
  ret.resize(in.rows(),in.cols());
  for(sizeType r=0;r<in.rows();r++)
    for(sizeType c=0;c<in.cols();c++)
      ret(r,c)=in(r,c);
  return ret;
}
template <typename T>
FORCE_INLINE static Eigen::SparseMatrix<SDPA_FLOAT,0,sizeType> scalarD2SDPA(const Eigen::SparseMatrix<T,0,sizeType>& in) {
  typename Eigen::SparseMatrix<SDPA_FLOAT,0,sizeType> ret;
  typedef Eigen::Triplet<SDPA_FLOAT,sizeType> STrip;
  typedef ParallelVector<STrip> STrips;
  STrips trips;
  ret.resize(in.rows(),in.cols());
  for(sizeType k=0; k<in.outerSize(); ++k)
    for(typename Eigen::SparseMatrix<T,0,sizeType>::InnerIterator it(in,k); it; ++it)
      trips.push_back(STrip(it.row(),it.col(),it.value()));
  ret.setFromTriplets(trips.begin(),trips.end());
  return ret;
}
PRJ_END
#endif
//SDPA-QD
#ifdef SDPA_QD_SUPPORT
#include <sdpa-qd/sdpa-qd-7.1.2/sdpa_io.h>
namespace std
{
FORCE_INLINE static qd_real sqrt(qd_real x) {
  return ::sqrt(x);
}
FORCE_INLINE static qd_real abs(qd_real x) {
  return ::abs(x);
}
}
#define SDPA_FLOAT qd_real
PRJ_BEGIN
FORCE_INLINE static scalarD SDPA2ScalarD(SDPA_FLOAT in) {
  return in.x[0];
}
template <typename T,int R,int C>
FORCE_INLINE static Eigen::Matrix<T,R,C> SDPA2ScalarD(const Eigen::Matrix<SDPA_FLOAT,R,C>& in) {
  typename Eigen::Matrix<T,R,C> ret;
  ret.resize(in.rows(),in.cols());
  for(sizeType r=0;r<in.rows();r++)
    for(sizeType c=0;c<in.cols();c++)
      ret(r,c)=in(r,c).x[0];
  return ret;
}
template <typename T>
FORCE_INLINE static Eigen::SparseMatrix<T,0,sizeType> SDPA2ScalarD(const Eigen::SparseMatrix<SDPA_FLOAT,0,sizeType>& in) {
  typename Eigen::SparseMatrix<T,0,sizeType> ret;
  typedef Eigen::Triplet<T,sizeType> STrip;
  typedef ParallelVector<STrip> STrips;
  STrips trips;
  ret.resize(in.rows(),in.cols());
  for(sizeType k=0; k<in.outerSize(); ++k)
    for(typename Eigen::SparseMatrix<SDPA_FLOAT,0,sizeType>::InnerIterator it(in,k); it; ++it)
      trips.push_back(STrip(it.row(),it.col(),it.value().x[0]));
  ret.setFromTriplets(trips.begin(),trips.end());
  return ret;
}
template <typename T,int R,int C>
FORCE_INLINE static typename Eigen::Matrix<SDPA_FLOAT,R,C> scalarD2SDPA(const Eigen::Matrix<T,R,C>& in) {
  typename Eigen::Matrix<SDPA_FLOAT,R,C> ret;
  ret.resize(in.rows(),in.cols());
  for(sizeType r=0;r<in.rows();r++)
    for(sizeType c=0;c<in.cols();c++)
      ret(r,c)=in(r,c);
  return ret;
}
template <typename T>
FORCE_INLINE static Eigen::SparseMatrix<SDPA_FLOAT,0,sizeType> scalarD2SDPA(const Eigen::SparseMatrix<T,0,sizeType>& in) {
  typename Eigen::SparseMatrix<SDPA_FLOAT,0,sizeType> ret;
  typedef Eigen::Triplet<SDPA_FLOAT,sizeType> STrip;
  typedef ParallelVector<STrip> STrips;
  STrips trips;
  ret.resize(in.rows(),in.cols());
  for(sizeType k=0; k<in.outerSize(); ++k)
    for(typename Eigen::SparseMatrix<T,0,sizeType>::InnerIterator it(in,k); it; ++it)
      trips.push_back(STrip(it.row(),it.col(),it.value()));
  ret.setFromTriplets(trips.begin(),trips.end());
  return ret;
}
PRJ_END
#endif
//SDPA-GMP
#ifdef SDPA_GMP_SUPPORT
#include "gmpxx.h"
namespace std
{
FORCE_INLINE static mpf_class sqrt(mpf_class x) {
  return ::sqrt(x);
}
FORCE_INLINE static mpf_class abs(mpf_class x) {
  return ::abs(x);
}
}
#define SDPA_FLOAT mpf_class
PRJ_BEGIN
FORCE_INLINE static scalarD SDPA2ScalarD(SDPA_FLOAT in) {
  return in.get_d();
}
template <typename T,int R,int C>
FORCE_INLINE static Eigen::Matrix<T,R,C> SDPA2ScalarD(const Eigen::Matrix<SDPA_FLOAT,R,C>& in) {
  typename Eigen::Matrix<T,R,C> ret;
  ret.resize(in.rows(),in.cols());
  for(sizeType r=0;r<in.rows();r++)
    for(sizeType c=0;c<in.cols();c++)
      ret(r,c)=in(r,c).get_d();
  return ret;
}
template <typename T>
FORCE_INLINE static Eigen::SparseMatrix<T,0,sizeType> SDPA2ScalarD(const Eigen::SparseMatrix<SDPA_FLOAT,0,sizeType>& in) {
  typename Eigen::SparseMatrix<T,0,sizeType> ret;
  typedef Eigen::Triplet<T,sizeType> STrip;
  typedef ParallelVector<STrip> STrips;
  STrips trips;
  ret.resize(in.rows(),in.cols());
  for(sizeType k=0; k<in.outerSize(); ++k)
    for(typename Eigen::SparseMatrix<SDPA_FLOAT,0,sizeType>::InnerIterator it(in,k); it; ++it)
      trips.push_back(STrip(it.row(),it.col(),it.value().get_d()));
  ret.setFromTriplets(trips.begin(),trips.end());
  return ret;
}
template <typename T,int R,int C>
FORCE_INLINE static typename Eigen::Matrix<SDPA_FLOAT,R,C> scalarD2SDPA(const Eigen::Matrix<T,R,C>& in) {
  typename Eigen::Matrix<SDPA_FLOAT,R,C> ret;
  ret.resize(in.rows(),in.cols());
  for(sizeType r=0;r<in.rows();r++)
    for(sizeType c=0;c<in.cols();c++)
      ret(r,c)=in(r,c);
  return ret;
}
template <typename T>
FORCE_INLINE static Eigen::SparseMatrix<SDPA_FLOAT,0,sizeType> scalarD2SDPA(const Eigen::SparseMatrix<T,0,sizeType>& in) {
  typename Eigen::SparseMatrix<SDPA_FLOAT,0,sizeType> ret;
  typedef Eigen::Triplet<SDPA_FLOAT,sizeType> STrip;
  typedef ParallelVector<STrip> STrips;
  STrips trips;
  ret.resize(in.rows(),in.cols());
  for(sizeType k=0; k<in.outerSize(); ++k)
    for(typename Eigen::SparseMatrix<T,0,sizeType>::InnerIterator it(in,k); it; ++it)
      trips.push_back(STrip(it.row(),it.col(),it.value()));
  ret.setFromTriplets(trips.begin(),trips.end());
  return ret;
}
PRJ_END
#endif

PRJ_BEGIN

struct SDPSolution : public SerializableBase
{
  typedef Eigen::Matrix<SDPA_FLOAT,-1,1> Vec;
  typedef Eigen::Matrix<SDPA_FLOAT,6,-1> Mat6X;
  typedef Eigen::Matrix<SDPA_FLOAT,-1,-1> DMat;
  typedef Eigen::SparseMatrix<SDPA_FLOAT,0,sizeType> SMat;
  typedef Eigen::Triplet<SDPA_FLOAT,sizeType> STrip;
  typedef ParallelVector<STrip> STrips;
  typedef std::vector<Mat6Xd,Eigen::aligned_allocator<Mat6Xd>> Vsss;
  typedef std::vector<DMat,Eigen::aligned_allocator<DMat>> Ess;
  typedef std::vector<Vec,Eigen::aligned_allocator<Vec>> Sss;
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  DMat vssFZ(sizeType i) const;
  Vec sssZ(sizeType i) const;
  Vec sssF(sizeType i) const;
  Vec sssInvF1Z2(sizeType i) const;
  Vec sssInvZ2F1(sizeType i) const;
  void debugFZ() const;
  //data
  boost::shared_ptr<sdpa::Solutions> _currentPt;
  Sss _sssZ1,_sssZ2,_sssF1,_sssF2;
  Ess _vssFZ1,_vssFZ2,_vssFZ3;
  Vec _F,_Z,_X;
};

PRJ_END

#endif

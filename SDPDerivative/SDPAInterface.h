#ifndef SDPA_INTERFACE_H
#define SDPA_INTERFACE_H

#ifdef SDPA_SUPPORT
#include "CommonFile/MathBasic.h"
#include "SDPSolution.h"
#include <Eigen/Sparse>
#include <sdpa_call.h>

PRJ_BEGIN

template <typename T>
struct PSDConeTraits
{
  typedef Eigen::Matrix<T,-1,1> Vec;
  typedef Eigen::Matrix<T,-1,-1> Mat;
  typedef Eigen::SparseMatrix<T,0,sizeType> SMat;
  typedef std::vector<SMat> PSDCone;
};
struct SDPAInterface
{
public:
  typedef PSDConeTraits<double>::Vec Vec;
  typedef PSDConeTraits<double>::Mat Mat;
  typedef PSDConeTraits<double>::SMat SMat;
  typedef PSDConeTraits<double>::PSDCone PSDCone;
  SDPAInterface();
  void reset(sdpa::Parameter::parameterType type=sdpa::Parameter::PARAMETER_DEFAULT);
  void solve(bool output=true,sdpa::Solutions* ptInit=NULL);
  void check() const;
  template <typename T>
  void setProblem(const typename PSDConeTraits<T>::Vec& b,const std::vector<typename PSDConeTraits<T>::PSDCone>& cones);
  void getSolution(SDPSolution& sol,scalarD eps=0.0f) const;
  Eigen::Map<const Mat> Z(int bid) const;
  Eigen::Map<const Mat> F(int bid) const;
  Eigen::Map<const Vec> x() const;
private:
  Mat F0(int bid) const;
  Mat FI(int bid,int mid) const;
  Mat convert(const sdpa::SparseMatrix& m) const;
  void printMat(const std::string& name,const Mat& m) const;
  std::vector<PSDCone> _PSDCones;
  boost::shared_ptr<SDPA> _problem;
  SDPA::ParameterType _type;
  Vec _b;
};

PRJ_END
#endif

#endif

#ifndef SOS_PROBLEM_H
#define SOS_PROBLEM_H

#include "SOSPolynomial.h"
#include "MosekInterface.h"

PRJ_BEGIN

//we solve for X with Y being polynomial variables
template <typename T,char LABELX,char LABELY>
class SOSProblem : public SerializableBase
{
public:
  DECL_MOSEK_TYPES
  typedef SOSTerm<T,LABELX> TermX;
  typedef SOSPolynomial<T,LABELX> PolyX;
  typedef SOSTerm<PolyX,LABELY> TermXY;
  typedef SOSPolynomial<PolyX,LABELY> PolyXY;
  typedef Eigen::SparseMatrix<T,0,sizeType> SMat;
  typedef std::vector<PolyXY> PolyXYs;
  typedef std::vector<SMat> PSDCone;
  typedef typename PolyX::VEC VECX;
  typedef typename PolyXY::VEC VEC;
  typedef typename PolyXY::MAT MAT;
  typedef typename PolyX::COLD COLD;
  typedef Eigen::Map<const COLD> COLDCM;
  //convert to ScalarType by evaluation
  typedef SOSProblem<typename ScalarOfT<T>::Type,LABELX,LABELY> SOSProblemScalar;
  friend class SOSProblem<SOSPolynomial<T,'V'>,LABELX,LABELY>;
  //Problem
  SOSProblem();
  SOSProblem(const PolyXYs& preCondGE,const PolyXYs& preCondEQ,const PolyXY& postCond,sizeType desiredHalfOrder,bool forceQuadratic=false);
  void reset(const PolyXYs& preCondGE,const PolyXYs& preCondEQ,PolyXY postCond,sizeType desiredHalfOrder,bool forceQuadratic=false);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  //convert to ScalarType by evaluation
  void evaluate(SOSProblemScalar& other,COLDCM vals,COLDCM* valsDeriv) const;
  static void evaluateSMat(const SMat& curr,typename SOSProblemScalar::SMat& other,COLDCM vals,COLDCM* valsDeriv=NULL);
  static void evaluateVec(const VECX& curr,typename SOSProblemScalar::VECX& other,COLDCM vals);
  //the first cone is the main condition, and the other cones are lagrangian multiplier cones
  const std::vector<MAT>& PSDCones() const;
  sizeType nrVarXLambda() const;
  sizeType nrVarX() const;
  sizeType orderX() const;
  bool isLinear() const;
  bool isQuadratic() const;
  std::string printCones() const;
  std::string printAugLagCons() const;
  const VEC& augLagCons() const;
  const Coli& cs() const;
  const SMat& A() const;
  const VECX& A0() const;
  //writeVTK
  void writeQ1AVTK(const std::string& path) const;  //special routine for writing the A pattern of Q1LowerBound
protected:
  //helper
  void buildPSDConeCoefs();
  void buildAugLagCons(PolyXY& postCond);
  std::string printCone(const MAT& cone) const;
  PolyXY buildLagrangian(sizeType& nrVar,sizeType order,MAT& coefMat) const;
  void buildMonomial(PolyXY& ret,sizeType remainingOrder,TermXY term,int vid) const;
  PolyXY buildMonomial(sizeType maxOrder) const;
  MAT sosCoef(const PolyXY& poly) const;
  void updateTermMap();
  //param
  sizeType _nrVarX,_nrVarY;
  sizeType _orderX,_orderY;
  sizeType _nrVarXLambda;
  //temporary: cones
  PolyXY _monomialY;
  std::map<TermXY,Vec2i> _termMap;
  std::vector<MAT> _PSDCones;
  VEC _augLagCons;
  //temporary: assembled cones
  Coli _cs;
  SMat _A,_ADeriv;
  VECX _A0;
};

PRJ_END

#endif

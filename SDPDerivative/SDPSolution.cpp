#include "SDPSolution.h"
#include "DebugGradient.h"
#include "SparseUtils.h"

USE_PRJ_NAMESPACE

bool SDPSolution::read(std::istream& is,IOData* dat)
{
  sizeType n;
  readBinaryData(n,is,dat);
  _sssZ1.resize(n);
  _sssZ2.resize(n);
  _sssF1.resize(n);
  _sssF2.resize(n);
  _vssFZ1.resize(n);
  _vssFZ2.resize(n);
  _vssFZ3.resize(n);
  for(sizeType i=0; i<n; i++) {
    Eigen::Matrix<scalarD,-1,1> sssZ1,sssZ2,sssF1,sssF2;
    Eigen::Matrix<scalarD,-1,-1> vssFZ1,vssFZ2,vssFZ3;
    readBinaryData(sssZ1,is,dat);
    readBinaryData(sssZ2,is,dat);
    readBinaryData(sssF1,is,dat);
    readBinaryData(sssF2,is,dat);
    readBinaryData(vssFZ1,is,dat);
    readBinaryData(vssFZ2,is,dat);
    readBinaryData(vssFZ3,is,dat);

    _sssZ1[i]=scalarD2SDPA<scalarD,-1,1>(sssZ1);
    _sssZ2[i]=scalarD2SDPA<scalarD,-1,1>(sssZ2);
    _sssF1[i]=scalarD2SDPA<scalarD,-1,1>(sssF1);
    _sssF2[i]=scalarD2SDPA<scalarD,-1,1>(sssF2);
    _vssFZ1[i]=scalarD2SDPA<scalarD,-1,-1>(vssFZ1);
    _vssFZ2[i]=scalarD2SDPA<scalarD,-1,-1>(vssFZ2);
    _vssFZ3[i]=scalarD2SDPA<scalarD,-1,-1>(vssFZ3);
  }

  Eigen::Matrix<scalarD,-1,1> F,Z,X;
  readBinaryData(F,is,dat);
  readBinaryData(Z,is,dat);
  readBinaryData(X,is,dat);

  _F=scalarD2SDPA<scalarD,-1,1>(F);
  _Z=scalarD2SDPA<scalarD,-1,1>(Z);
  _X=scalarD2SDPA<scalarD,-1,1>(X);
  return is.good();
}
bool SDPSolution::write(std::ostream& os,IOData* dat) const
{
  sizeType n=(sizeType)_sssZ1.size();
  writeBinaryData(n,os,dat);
  for(sizeType i=0; i<n; i++) {
    writeBinaryData(SDPA2ScalarD<scalarD,-1,1>(_sssZ1[i]),os,dat);
    writeBinaryData(SDPA2ScalarD<scalarD,-1,1>(_sssZ2[i]),os,dat);
    writeBinaryData(SDPA2ScalarD<scalarD,-1,1>(_sssF1[i]),os,dat);
    writeBinaryData(SDPA2ScalarD<scalarD,-1,1>(_sssF2[i]),os,dat);
    writeBinaryData(SDPA2ScalarD<scalarD,-1,-1>(_vssFZ1[i]),os,dat);
    writeBinaryData(SDPA2ScalarD<scalarD,-1,-1>(_vssFZ2[i]),os,dat);
    writeBinaryData(SDPA2ScalarD<scalarD,-1,-1>(_vssFZ3[i]),os,dat);
  }
  writeBinaryData(SDPA2ScalarD<scalarD,-1,1>(_F),os,dat);
  writeBinaryData(SDPA2ScalarD<scalarD,-1,1>(_Z),os,dat);
  writeBinaryData(SDPA2ScalarD<scalarD,-1,1>(_X),os,dat);
  return os.good();
}
boost::shared_ptr<SerializableBase> SDPSolution::copy() const
{
  return boost::shared_ptr<SerializableBase>(new SDPSolution);
}
std::string SDPSolution::type() const
{
  return typeid(SDPSolution).name();
}
SDPSolution::DMat SDPSolution::vssFZ(sizeType i) const {
  DMat ret;
  ret.resize(_vssFZ1[i].rows(),_vssFZ1[i].rows());
  ret.block(0,0,_vssFZ1[i].rows(),_vssFZ1[i].cols())=_vssFZ1[i];
  ret.block(0,_vssFZ1[i].cols(),_vssFZ2[i].rows(),_vssFZ2[i].cols())=_vssFZ2[i];
  ret.block(0,_vssFZ1[i].cols()+_vssFZ2[i].cols(),_vssFZ3[i].rows(),_vssFZ3[i].cols())=_vssFZ3[i];
  return ret;
}
SDPSolution::Vec SDPSolution::sssZ(sizeType i) const {
  Vec ret=Vec::Zero(_vssFZ1[i].rows());
  ret.segment(0,_sssZ1[i].size())=_sssZ1[i];
  ret.segment(_sssZ1[i].size(),_sssZ2[i].size())=_sssZ2[i];
  return ret;
}
SDPSolution::Vec SDPSolution::sssF(sizeType i) const {
  Vec ret=Vec::Zero(_vssFZ1[i].rows());
  ret.segment(ret.size()-_sssF1[i].size()-_sssF2[i].size(),_sssF1[i].size())=_sssF1[i];
  ret.segment(ret.size()-_sssF2[i].size(),_sssF2[i].size())=_sssF2[i];
  return ret;
}
SDPSolution::Vec SDPSolution::sssInvF1Z2(sizeType i) const {
  return _sssF1[i].cwiseInverse().asDiagonal()*_sssZ2[i];
}
SDPSolution::Vec SDPSolution::sssInvZ2F1(sizeType i) const {
  return _sssZ2[i].cwiseInverse().asDiagonal()*_sssF1[i];
}
void SDPSolution::debugFZ() const
{
  DEFINE_NUMERIC_DELTA_T(SDPA_FLOAT)
  for(sizeType i=0,off=0; i<(sizeType)_vssFZ1.size(); i++) {
    sizeType rows=_vssFZ1[i].rows();
    DMat ZRef=symmetricKronecker<SDPA_FLOAT>(fromSVec<SDPA_FLOAT>(_Z.segment(off,rows)));
    DMat FRef=symmetricKronecker<SDPA_FLOAT>(fromSVec<SDPA_FLOAT>(_F.segment(off,rows)));
    DMat Z=vssFZ(i)*sssZ(i).asDiagonal()*vssFZ(i).transpose();
    DMat F=vssFZ(i)*sssF(i).asDiagonal()*vssFZ(i).transpose();
    DEBUG_GRADIENT("Z",ZRef.norm(),(ZRef-Z).norm()/std::max<SDPA_FLOAT>(ZRef.norm(),EPS))
    DEBUG_GRADIENT("F",FRef.norm(),(FRef-F).norm()/std::max<SDPA_FLOAT>(FRef.norm(),EPS))
    INFOV("Strict complementary safety: %10.10f",sssZ(i).cwiseMin(sssF(i)).maxCoeff())
    if(sssInvF1Z2(i).size()>0) {
      std::cout << sssInvF1Z2(i) << std::endl;
      INFOV("InvF1Z2 Range: (%10.10f,%10.10f)",sssInvF1Z2(i).minCoeff(),sssInvF1Z2(i).maxCoeff())
    }
    off+=rows;
  }
}

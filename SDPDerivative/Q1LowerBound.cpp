#include "Q1LowerBound.h"
#include "SDPAInterface.h"
#include "SDPADDInterface.h"
#include "SDPAQDInterface.h"
#include "SDPAGMPInterface.h"
#include "SYSVInterface.h"
#include "DebugGradient.h"
#include "SparseUtils.h"
#include "Utils.h"
#include "mosek.h"
#include <Eigen/Eigen>
#include <CommonFile/Timing.h>

USE_PRJ_NAMESPACE

#define SIMPLIFIED
#define DEBUG_MODE 0
static void MSKAPI printstrdummy(void *handle,const char str[]) {}
static void MSKAPI printstr(void *handle,const char str[]) {
  printf("%s",str);
}
Q1LowerBound::Q1LowerBound() {}
Q1LowerBound::Q1LowerBound(const Mat6d& M,scalarD mu,sizeType halfOrder,sizeType nrPt,sizeType nrDir)
  :_invM(M.inverse()),_mu(mu),_halfOrder(halfOrder),_nrPt(nrPt),_nrDir(nrDir)
{
  //b-r>=0
  SOSProbTpl::PolyXY postCond;
  postCond+=SOSProbTpl::TermXY(ScalarOfT<SOSProbTpl::PolyX>::convert(1),6,1);
  postCond-=SOSProbTpl::TermXY(SOSProbTpl::PolyX(SOSProbTpl::TermX(ScalarOfT<PolyV>::convert(1),0,1)));
  if(DEBUG_MODE)
    std::cout << "postCond=" << postCond.compactString() << std::endl;
  //a^TMa=1
  SOSProbTpl::PolyXYs preCondEQ(1);
  for(sizeType r=0; r<6; r++)
    for(sizeType c=0; c<6; c++)
      preCondEQ[0]+=SOSProbTpl::TermXY(ScalarOfT<SOSProbTpl::PolyX>::convert(_invM(r,c)),r,1)*SOSProbTpl::TermXY(ScalarOfT<SOSProbTpl::PolyX>::convert(1),c,1);
  preCondEQ[0]-=SOSProbTpl::TermXY(ScalarOfT<SOSProbTpl::PolyX>::convert(1));
  if(DEBUG_MODE)
    std::cout << "EQCond=" << preCondEQ[0].compactString() << std::endl;
  //V^Ta-b>=0
  SOSProbTpl::PolyXYs preCondGE;
  if(mu==0 || nrDir>3) {
    ASSERT((mu==0 && nrDir==1) || (mu>0 && nrDir>3))
    //linearized frictional cone
    preCondGE.resize(nrPt*nrDir);
    for(sizeType c=0; c<nrPt*nrDir; c++) {
      preCondGE[c]+=SOSProbTpl::TermXY(ScalarOfT<SOSProbTpl::PolyX>::convert(1),6,1);
      for(sizeType r=0; r<6; r++)
        preCondGE[c]+=SOSProbTpl::TermXY(SOSProbTpl::TermX(PolyV(TermV(1,r+c*6,1))),r,1);
      if(DEBUG_MODE)
        std::cout << "GECond"+std::to_string(c)+"=" << preCondGE[c].compactString() << std::endl;
    }
  } else {
    ASSERT(mu>0 && nrDir==3)
    //quadratic frictional cone
#ifdef SIMPLIFIED
    preCondGE.resize(nrPt);
#else
    preCondGE.resize(nrPt*2);
#endif
    for(sizeType c=0; c<nrPt; c++) {
      //normal
      sizeType c0=c+nrPt*0;
      SOSProbTpl::PolyXY n=SOSProbTpl::TermXY(ScalarOfT<SOSProbTpl::PolyX>::convert(1),6,1);
      for(sizeType r=0; r<6; r++)
        n+=SOSProbTpl::TermXY(SOSProbTpl::TermX(PolyV(TermV(1,r+c0*6,1))),r,1);
      //tangent1
      sizeType c1=c+nrPt*1;
      SOSProbTpl::PolyXY t1=ScalarOfT<SOSProbTpl::PolyXY>::convert(0);
      for(sizeType r=0; r<6; r++)
        t1+=SOSProbTpl::TermXY(SOSProbTpl::TermX(PolyV(TermV(1,r+c1*6,1))),r,1);
      //tangent2
      sizeType c2=c+nrPt*2;
      SOSProbTpl::PolyXY t2=ScalarOfT<SOSProbTpl::PolyXY>::convert(0);
      for(sizeType r=0; r<6; r++)
        t2+=SOSProbTpl::TermXY(SOSProbTpl::TermX(PolyV(TermV(1,r+c2*6,1))),r,1);
#ifdef SIMPLIFIED
      preCondGE[c]=n-(t1*t1+t2*t2)*ScalarOfT<SOSProbTpl::PolyXY>::convert(mu*mu);
      if(DEBUG_MODE)
        std::cout << "GECond"+std::to_string(c)+"=" << preCondGE[c].compactString() << std::endl;
#else
      preCondGE[c*2+0]=n*n-(t1*t1+t2*t2)*ScalarOfT<SOSProbTpl::PolyXY>::convert(mu*mu);
      if(DEBUG_MODE)
        std::cout << "GECond"+std::to_string(c)+"=" << preCondGE[c*2+0].compactString() << std::endl;
      preCondGE[c*2+1]=n;
      if(DEBUG_MODE)
        std::cout << "GECond"+std::to_string(c)+"=" << preCondGE[c*2+1].compactString() << std::endl;
#endif
    }
  }
  _probTpl.reset(preCondGE,preCondEQ,postCond,_halfOrder);
}
bool Q1LowerBound::read(std::istream& is,IOData* dat)
{
  readBinaryData(_invM,is);
  readBinaryData(_mu,is);
  readBinaryData(_halfOrder,is);
  readBinaryData(_nrPt,is);
  readBinaryData(_nrDir,is);
  readBinaryData(_probTpl,is);
  return is.good();
}
bool Q1LowerBound::write(std::ostream& os,IOData* dat) const
{
  writeBinaryData(_invM,os);
  writeBinaryData(_mu,os);
  writeBinaryData(_halfOrder,os);
  writeBinaryData(_nrPt,os);
  writeBinaryData(_nrDir,os);
  writeBinaryData(_probTpl,os);
  return os.good();
}
boost::shared_ptr<SerializableBase> Q1LowerBound::copy() const
{
  return boost::shared_ptr<SerializableBase>(new Q1LowerBound);
}
std::string Q1LowerBound::type() const
{
  return typeid(Q1LowerBound).name();
}
//compute value
void Q1LowerBound::debugBuildProb()
{
  DEFINE_NUMERIC_DELTA_T(scalarD)
  Mat6Xd vss=Mat6Xd::Random(6,_nrPt*_nrDir);
  SOSProb probBF=buildProbBF(vss);
  SOSProb prob=buildProb(vss);
  DEBUG_GRADIENT("coefsTypeII.A",prob.A().norm(),(prob.A()-probBF.A()).norm())
  DEBUG_GRADIENT("coefsTypeII.A0",prob.A0().norm(),(prob.A0()-probBF.A0()).norm())
  sizeType sz=prob.A().rows()-prob.cs()[0];
  SMat Id;
  Id.resize(sz,sz);
  Id.setIdentity();
  SMat ABlk=prob.A().block(prob.A().rows()-sz,prob.A().cols()-sz,sz,sz);
  DEBUG_GRADIENT("coefsTypeII.ABlk",ABlk.norm(),(ABlk-Id).norm())
}
Mat6Xd Q1LowerBound::buildVss(const Mat3Xd& pss,const Mat3Xd& nss) const
{
  Mat6Xd vss;
  sizeType off=0;
  vss.setZero(6,pss.cols()*nrDir());
  for(sizeType i=0; i<pss.cols(); i++,off++) {
    sizeType id;
    Vec3d n=nss.col(i).normalized();
    n.cwiseAbs().minCoeff(&id);
    Vec3d t1=Vec3d::Unit(id).cross(n).normalized();
    Vec3d t2=n.cross(t1);
    if(nrDir()==3) {
      vss.col(off+pss.cols()*0)=concat<Vec>(n,pss.col(i).cross(n));
      vss.col(off+pss.cols()*1)=concat<Vec>(t1,pss.col(i).cross(t1));
      vss.col(off+pss.cols()*2)=concat<Vec>(t2,pss.col(i).cross(t2));
    } else {
      for(sizeType j=0; j<nrDir(); j++) {
        scalarD theta=M_PI*2*j/nrDir();
        Vec3d d=n+t1*std::cos(theta)*_mu+t2*std::sin(theta)*_mu;
        vss.col(off+j*pss.cols())=concat<Vec>(d,pss.col(i).cross(d));
      }
    }
  }
  ASSERT(off==pss.cols())
  return vss;
}
Q1LowerBound::SOSProb Q1LowerBound::buildProb(const Mat6Xd& vss,const Mat6Xd* vssDeriv) const
{
  SOSProb ret;
  Eigen::Map<const Cold> vssM(vss.data(),vss.size());
  Eigen::Map<const Cold> vssDerivM(vssDeriv?vssDeriv->data():NULL,vssDeriv?vssDeriv->size():0);
  _probTpl.evaluate(ret,vssM,vssDeriv?&vssDerivM:NULL);
  return ret;
}
Q1LowerBound::SOSProb Q1LowerBound::buildProbBF(const Mat6Xd& vss) const
{
  //b-r>=0
  SOSProb::PolyXY postCond;
  postCond+=SOSProb::TermXY(SOSProb::TermX(1),6,1);
  postCond-=SOSProb::TermXY(SOSProb::PolyX(SOSProb::TermX(1,0,1)));
  if(DEBUG_MODE)
    std::cout << "postCond=" << postCond.compactString() << std::endl;
  //a^TMa=1
  SOSProb::PolyXYs preCondEQ(1);
  for(sizeType r=0; r<6; r++)
    for(sizeType c=0; c<6; c++)
      preCondEQ[0]+=SOSProb::TermXY(SOSProb::TermX(_invM(r,c)),r,1)*SOSProb::TermXY(SOSProb::TermX(1),c,1);
  preCondEQ[0]-=SOSProb::TermXY(SOSProb::PolyX(SOSProb::TermX(1)));
  if(DEBUG_MODE)
    std::cout << "EQCond=" << preCondEQ[0].compactString() << std::endl;
  //V^Ta-b>=0
  SOSProb::PolyXYs preCondGE(vss.cols());
  for(sizeType c=0; c<vss.cols(); c++) {
    preCondGE[c]+=SOSProb::TermXY(SOSProb::TermX(1),6,1);
    for(sizeType r=0; r<6; r++)
      preCondGE[c]+=SOSProb::TermXY(SOSProb::TermX(vss(r,c)),r,1);
    if(DEBUG_MODE)
      std::cout << "GECond"+std::to_string(c)+"=" << preCondGE[c].compactString() << std::endl;
  }
  SOSProb ret;
  ret.reset(preCondGE,preCondEQ,postCond,_halfOrder);
  return ret;
}
void Q1LowerBound::buildCones(const SOSProb& prob,std::vector<SOSProb::PSDCone>& cones) const
{
  sizeType r,c,N;
  std::vector<STrips> trips;
  cones.resize(prob.cs().size());
  static const scalarD invSqrt2=1/std::sqrt((scalarD)2.0f);
  for(sizeType vid=0; vid<=prob.nrVarXLambda(); vid++) {
    trips.assign(prob.cs().size(),STrips());
    //assign
    if(vid==0) {
      for(sizeType bid=0,off=0; bid<prob.cs().size(); off+=prob.cs()[bid],bid++) {
        N=NSVecToN(prob.cs()[bid]);
        for(sizeType x=0; x<prob.cs()[bid]; x++) {
          TriToIJ(N,x,r,c);
          if(r==c)
            trips[bid].push_back(STrip(r,c,prob.A0()[x+off]));
          else {
            trips[bid].push_back(STrip(r,c,prob.A0()[x+off]*invSqrt2));
            trips[bid].push_back(STrip(c,r,prob.A0()[x+off]*invSqrt2));
          }
        }
      }
    } else {
      sizeType off=0;
      sizeType bid=0;
      N=NSVecToN(prob.cs()[bid]);
      for(typename SMat::InnerIterator it(prob.A(),vid-1); it; ++it) {
        while(it.row()>=off+prob.cs()[bid]) {
          off+=prob.cs()[bid];
          bid++;
          N=NSVecToN(prob.cs()[bid]);
        }
        TriToIJ(N,it.row()-off,r,c);
        if(r==c)
          trips[bid].push_back(STrip(r,c,it.value()));
        else {
          trips[bid].push_back(STrip(r,c,it.value()*invSqrt2));
          trips[bid].push_back(STrip(c,r,it.value()*invSqrt2));
        }
      }
    }
    //assemble
    for(sizeType bid=0; bid<prob.cs().size(); bid++) {
      N=NSVecToN(prob.cs()[bid]);
      cones[bid].push_back(SMat());
      cones[bid].back().resize(N,N);
      cones[bid].back().setFromTriplets(trips[bid].begin(),trips[bid].end());
    }
  }
}
void Q1LowerBound::writeProbSDPA(const SOSProb& prob,const std::string& path) const
{
  std::vector<SOSProb::PSDCone> cones;
  boost::filesystem::ofstream os(path);
  buildCones(prob,cones);
  os << prob.nrVarXLambda() << " = mDIM" << std::endl;
  os << cones.size() << " = nBOLCK" << std::endl;
  //bLOCKsTRUCT
  for(sizeType i=0; i<(sizeType)cones.size(); i++)
    os << cones[i][0].rows() << (i==(sizeType)cones.size()-1?" = bLOCKsTRUCT":" ");
  os << std::endl;
  //c
  Vec c=computeC();
  os << "{";
  for(sizeType i=0; i<c.size(); i++)
    os << c[i] << (i==(sizeType)c.size()-1?"}":",");
  os << std::endl;
  //blocks
  for(sizeType v=0; v<=prob.nrVarXLambda(); v++) {
    os << "{" << std::endl;
    for(sizeType i=0; i<(sizeType)cones.size(); i++) {
      const Matd m=cones[i][v].toDense()*(v==0?-1:1);
      for(sizeType r=0; r<m.rows(); r++) {
        os << (r==0?"{ ":"  ") << "{";
        for(sizeType c=0; c<m.cols(); c++)
          os << m(r,c) << (c==m.cols()-1?"}":",");
        if(r==m.rows()-1)
          os << " }";
        os << std::endl;
      }
    }
    os << "}" << std::endl;
  }
}
scalarD Q1LowerBound::solveProb(const SOSProb& prob,SDPSolution& sol,scalarD eps,bool output) const
{
  //return solveProbMosek(prob,sol,eps,output);
  return solveProbSDPA(prob,sol,eps,output);
}
scalarD Q1LowerBound::solveProbSDPA(const SOSProb& prob,SDPSolution& sol,scalarD eps,bool output) const
{
  //solve SDP
  SDPAInterface sdpa;
  std::vector<SOSProb::PSDCone> cones;
  buildCones(prob,cones);
  sdpa.setProblem<scalarD>(computeC(),cones);
  sdpa.solve(output,sol._currentPt?sol._currentPt.get():NULL);
  //sdpa.check();
  sdpa.getSolution(sol,eps);
  return SDPA2ScalarD(sol._X[0]);
}
scalarD Q1LowerBound::solveProbMosek(const SOSProb& prob,SDPSolution& sol,scalarD eps,bool output) const
{
  std::vector<SOSProb::PSDCone> cones;
  buildCones(prob,cones);
  Vec coefs=computeC();
#define CHECK_CALL(CALL) if(CALL!=MSK_RES_OK){ASSERT_MSGV(false,"Fall when calling: %s",STRINGIFY_OMP(CALL))}
  MSKint64t idx;
  MSKenv_t env=NULL;
  MSKtask_t task=NULL;
  MSKrealt falpha=1.0;
  MSKint32t NUMCONS=coefs.size();
  MSKint32t NUMBARVAR=prob.cs().size();
  std::vector<MSKint32t> DIMBARVAR(NUMBARVAR);
  for(MSKint32t i=0; i<NUMBARVAR; i++)
    DIMBARVAR[i]=cones[i][0].rows();
  CHECK_CALL(MSK_makeenv(&env,NULL))
  CHECK_CALL(MSK_maketask(env,NUMCONS,0,&task))
  if(output)
    MSK_linkfunctotaskstream(task,MSK_STREAM_LOG,NULL,printstr);
  else MSK_linkfunctotaskstream(task,MSK_STREAM_LOG,NULL,printstrdummy);
  CHECK_CALL(MSK_appendbarvars(task,NUMBARVAR,&DIMBARVAR[0]))
  CHECK_CALL(MSK_appendcons(task,coefs.size()))
  std::vector<MSKint32t> ci,cj;
  std::vector<MSKrealt> cv;
  //objective
  for(MSKint32t i=0; i<NUMBARVAR; i++) {
    ci.clear();
    cj.clear();
    cv.clear();
    const SOSProb::SMat& c=cones[i][0];
    for(sizeType k=0; k<c.outerSize(); ++k)
      for(typename SOSProb::SMat::InnerIterator it(c,k); it; ++it)
        if(it.row()>=it.col()) {
          ci.push_back(it.row());
          cj.push_back(it.col());
          cv.push_back(-it.value());
        }
    CHECK_CALL(MSK_appendsparsesymmat(task,DIMBARVAR[i],cv.size(),&ci[0],&cj[0],&cv[0],&idx))
    CHECK_CALL(MSK_putbarcj(task,i,1,&idx,&falpha))
  }
  //linear constraint
  for(MSKint32t j=0; j<NUMCONS; j++) {
    for(MSKint32t i=0; i<NUMBARVAR; i++) {
      ci.clear();
      cj.clear();
      cv.clear();
      const SOSProb::SMat& c=cones[i][j+1];
      for(sizeType k=0; k<c.outerSize(); ++k)
        for(typename SOSProb::SMat::InnerIterator it(c,k); it; ++it)
          if(it.row()>=it.col()) {
            ci.push_back(it.row());
            cj.push_back(it.col());
            cv.push_back(it.value());
          }
      CHECK_CALL(MSK_appendsparsesymmat(task,DIMBARVAR[i],cv.size(),&ci[0],&cj[0],&cv[0],&idx))
      CHECK_CALL(MSK_putbaraij(task,j,i,1,&idx,&falpha))
    }
    CHECK_CALL(MSK_putconbound(task,j,MSK_BK_FX,coefs[j],coefs[j]))
  }
  //solve
  MSKsolstae solsta;
  MSKrescodee trmcode;
  CHECK_CALL(MSK_putobjsense(task,MSK_OBJECTIVE_SENSE_MAXIMIZE))
  CHECK_CALL(MSK_optimizetrm(task,&trmcode))
  MSK_solutionsummary(task,MSK_STREAM_MSG);
  MSK_getsolsta(task,MSK_SOL_ITR,&solsta);
  if(solsta==MSK_SOL_STA_OPTIMAL) {
    //getX
    MSKrealt* yy=(MSKrealt*)MSK_calloctask(task,NUMCONS,sizeof(MSKrealt));
    CHECK_CALL(MSK_gety(task,MSK_SOL_ITR,yy))
    sol._X=scalarD2SDPA<MSKrealt,-1,1>(Eigen::Map<const Eigen::Matrix<MSKrealt,-1,1>>(yy,NUMCONS));
    MSK_freetask(task,yy);
    //getF,Z
    const scalarD sqrt2=std::sqrt((scalarD)2);
    sol._F.resize(prob.A().rows());
    sol._Z.resize(prob.A().rows());
    sol._vssFZ1.resize(NUMBARVAR);
    sol._vssFZ2.resize(NUMBARVAR);
    sol._vssFZ3.resize(NUMBARVAR);
    sol._sssZ1.resize(NUMBARVAR);
    sol._sssZ2.resize(NUMBARVAR);
    sol._sssF1.resize(NUMBARVAR);
    sol._sssF2.resize(NUMBARVAR);
    Matd FI,ZI;
    Vec EF,EZ;
    MSKrealt* ss=(MSKrealt*)MSK_calloctask(task,prob.cs().maxCoeff(),sizeof(MSKrealt));
    for(MSKint32t j=0,offF=0,offZ=0; j<NUMBARVAR; j++) {
      //F
      FI.resize(DIMBARVAR[j],DIMBARVAR[j]);
      CHECK_CALL(MSK_getbarsj(task,MSK_SOL_ITR,j,ss))
      for(MSKint32t r=0,offSS=0; r<DIMBARVAR[j]; r++)
        for(MSKint32t c=r; c<DIMBARVAR[j]; offSS++,c++) {
          sol._F[offF++]=r==c?-ss[offSS]:-ss[offSS]*sqrt2;
          FI(r,c)=FI(c,r)=-ss[offSS];
        }
      //Z
      ZI.resize(DIMBARVAR[j],DIMBARVAR[j]);
      CHECK_CALL(MSK_getbarxj(task,MSK_SOL_ITR,j,ss))
      for(MSKint32t r=0,offSS=0; r<DIMBARVAR[j]; r++)
        for(MSKint32t c=r; c<DIMBARVAR[j]; offSS++,c++) {
          sol._Z[offZ++]=r==c?ss[offSS]:ss[offSS]*sqrt2;
          ZI(r,c)=ZI(c,r)=ss[offSS];
        }
      //find eigenvalues
      FI=fromSVec<scalarD>(SDPA2ScalarD<scalarD,-1,1>(sol._F.segment(offF-prob.cs()[j],prob.cs()[j])));
      ZI=fromSVec<scalarD>(SDPA2ScalarD<scalarD,-1,1>(sol._Z.segment(offZ-prob.cs()[j],prob.cs()[j])));
      Eigen::SelfAdjointEigenSolver<Matd> eig(FI+ZI);
      EF=(eig.eigenvectors().transpose()*(FI*eig.eigenvectors())).diagonal();
      EZ=(eig.eigenvectors().transpose()*(ZI*eig.eigenvectors())).diagonal();
      symmetricKroneckerEigensystemTrisection<SDPA_FLOAT>
      (scalarD2SDPA<scalarD,-1,-1>(eig.eigenvectors()),
       scalarD2SDPA<scalarD,-1,1>(EF),
       scalarD2SDPA<scalarD,-1,1>(EZ),
       sol._vssFZ1[j],sol._vssFZ2[j],sol._vssFZ3[j],
       sol._sssZ1[j],sol._sssZ2[j],sol._sssF1[j],sol._sssF2[j],eps);
    }
    MSK_freetask(task,ss);
    //check(prob,sol);
  } else {
    sol._X.setZero(1);
  }
  MSK_deletetask(&task);
  MSK_deleteenv(&env);
#undef CHECK_CALL
  return SDPA2ScalarD(sol._X[0]);
}
void Q1LowerBound::check(const SOSProb& prob,const SDPSolution& sol) const
{
  std::vector<SOSProb::PSDCone> cones;
  buildCones(prob,cones);
  Vec coefs=computeC();
  DEFINE_NUMERIC_DELTA_T(scalarD)
  for(sizeType i=0,off=0; i<prob.cs().size(); off+=prob.cs()[i],i++) {
    Matd F=SDPA2ScalarD<scalarD,-1,-1>(fromSVec<SDPA_FLOAT>(sol._F.segment(off,prob.cs()[i])));
    Matd Z=SDPA2ScalarD<scalarD,-1,-1>(fromSVec<SDPA_FLOAT>(sol._Z.segment(off,prob.cs()[i])));
    Matd FRef=cones[i][0].toDense();
    for(sizeType j=0; j<coefs.size(); j++)
      FRef+=cones[i][j+1].toDense()*SDPA2ScalarD(sol._X[j]);
    DEBUG_GRADIENT("F",F.norm(),(FRef-F).norm())
    DEBUG_GRADIENT("FZ",F.norm(),(F*Z).cwiseAbs().maxCoeff())
  }
  for(sizeType i=0; i<coefs.size(); i++) {
    scalarD FIZ=0;
    for(sizeType j=0,off=0; j<prob.cs().size(); off+=prob.cs()[j],j++) {
      Matd Z=SDPA2ScalarD<scalarD,-1,-1>(fromSVec<SDPA_FLOAT>(sol._Z.segment(off,prob.cs()[j])));
      FIZ+=(cones[j][i+1].toDense()*Z).trace();
    }
    INFOV("BI: %f BIRef: %f",FIZ,coefs[i])
  }
}
sizeType Q1LowerBound::nrDir() const
{
  return _nrDir;
}
sizeType Q1LowerBound::nrPt() const
{
  return _nrPt;
}
//compute jacobian
void Q1LowerBound::debugJG(sizeType nrIter) const
{
  Vec DZ,DX;
  Mat6Xd vss;
  SDPSolution sol;
  INFO("BeginDebugJG")
  DEFINE_NUMERIC_DELTA_T(scalarD)
  for(sizeType i=0; i<nrIter; i++) {
    INFOV("Iter%ld",i)
    vss.setRandom(6,nrPt()*nrDir());
    SOSProb prob=buildProb(vss);
    sol._Z.setRandom(prob.A().rows());
    sol._X.setRandom(prob.A().cols());
    DZ.setRandom(prob.A().rows());
    DX.setRandom(prob.A().cols());
    sol._F=scalarD2SDPA<scalarD,-1,1>(prob.A()*SDPA2ScalarD<scalarD,-1,1>(sol._X)+prob.A0());
    const Vec G=SDPA2ScalarD<scalarD,-1,1>(computeG(prob,sol));
    const SMat J=SDPA2ScalarD<scalarD>(computeJG(prob,sol));
    if(i==0)
      writeJGVTK("JG.vtk",J);
    //gradient with respect to Z,X
    sol._Z+=scalarD2SDPA<scalarD,-1,1>(DZ*DELTA);
    sol._X+=scalarD2SDPA<scalarD,-1,1>(DX*DELTA);
    sol._F=scalarD2SDPA<scalarD,-1,1>(prob.A()*SDPA2ScalarD<scalarD,-1,1>(sol._X)+prob.A0());
    const Vec G2=SDPA2ScalarD<scalarD,-1,1>(computeG(prob,sol));
    DEBUG_GRADIENT("dJGdZX",(J*concat(DX,DZ)).norm(),(J*concat(DX,DZ)-(G2-G)/DELTA).norm())
  }
  Vsss dvss(1);
  for(sizeType i=0; i<nrIter; i++) {
    INFOV("Iter%ld",i)
    vss.setRandom(6,nrPt()*nrDir());
    dvss[0].setRandom(6,nrPt()*nrDir());
    SOSProb prob=buildProb(vss);
    sol._Z.setRandom(prob.A().rows());
    sol._X.setRandom(prob.A().cols());
    sol._F=scalarD2SDPA<scalarD,-1,1>(prob.A()*SDPA2ScalarD<scalarD,-1,1>(sol._X)+prob.A0());
    const Vec G=SDPA2ScalarD<scalarD,-1,1>(computeG(prob,sol));
    const Vec DGDEps=SDPA2ScalarD<scalarD,-1,1>(computeDGDEps(vss,dvss,sol));
    //gradient with respect to Vss
    vss+=dvss[0]*DELTA;
    prob=buildProb(vss);
    sol._F=scalarD2SDPA<scalarD,-1,1>(prob.A()*SDPA2ScalarD<scalarD,-1,1>(sol._X)+prob.A0());
    const Vec G2=SDPA2ScalarD<scalarD,-1,1>(computeG(prob,sol));
    DEBUG_GRADIENT("dJGdVss",DGDEps.norm(),(DGDEps-(G2-G)/DELTA).norm())
  }
  INFO("EndDebugJG")
}
SDPSolution::SMat Q1LowerBound::computeJG(const SOSProb& prob,const SDPSolution& sol) const
{
  SDPSolution::DMat ZI,FI;
  SDPSolution::SMat ret;
  SDPSolution::STrips tripsZIAll,trips;
  SDPSolution::SMat A=scalarD2SDPA(prob.A());
  for(sizeType i=0,off=0; i<prob.cs().size(); off+=prob.cs()[i],i++) {
    ZI=symmetricKroneckerSVec<SDPA_FLOAT>(sol._Z.segment(off,prob.cs()[i]));
    addBlock(tripsZIAll,off,off,ZI);
    //bottom right
    FI=symmetricKroneckerSVec<SDPA_FLOAT>(sol._F.segment(off,prob.cs()[i]));
    addBlock(trips,off+A.cols(),off+A.cols(),FI);
  }
  SDPSolution::SMat ZIAll;
  ZIAll.resize(A.rows(),A.rows());
  ZIAll.setFromTriplets(tripsZIAll.begin(),tripsZIAll.end());
  //bottom left
  addBlock<SDPA_FLOAT,0,sizeType>(trips,A.cols(),0,ZIAll*A);
  //top right
  addBlock<SDPA_FLOAT,0,sizeType>(trips,0,A.cols(),A.transpose());
  //assemble
  ret.resize(A.rows()+A.cols(),A.rows()+A.cols());
  ret.setFromTriplets(trips.begin(),trips.end());
  return ret;
}
SDPSolution::DMat Q1LowerBound::computeDGDEps(const Mat6Xd& vss,const Vsss& dVdEps,const SDPSolution& sol) const
{
  SOSProb prob;
  SDPSolution::SMat ABlk1;
  SDPSolution::SMat A;
  SDPSolution::DMat ret=SDPSolution::DMat::Zero(_probTpl.A().rows()+_probTpl.A().cols(),(sizeType)dVdEps.size());
  SDPSolution::DMat ZI=symmetricKroneckerSVec<SDPA_FLOAT>(sol._Z.segment(0,_probTpl.cs()[0]));
  for(sizeType i=0; i<(sizeType)dVdEps.size(); i++) {
    prob=buildProb(vss,&dVdEps[i]);
    A=scalarD2SDPA(prob.A());
    ABlk1=A.block(0,0,prob.cs()[0],A.cols());
    ret.col(i).segment(0,A.cols())=ABlk1.transpose()*sol._Z.segment(0,prob.cs()[0]);
    ret.col(i).segment(A.cols(),prob.cs()[0])=ZI*(ABlk1*sol._X);
  }
  return ret;
}
SDPSolution::Vec Q1LowerBound::computeG(const SOSProb& prob,const SDPSolution& sol) const
{
  SDPSolution::DMat ZI;
  SDPSolution::SMat A=scalarD2SDPA(prob.A());
  SDPSolution::Vec ret=SDPSolution::Vec::Zero(A.rows()+A.cols());
  ret.segment(0,A.cols())=A.transpose()*sol._Z-scalarD2SDPA<scalarD,-1,1>(computeC());
  for(sizeType i=0,off=0; i<prob.cs().size(); off+=prob.cs()[i],i++) {
    ZI=symmetricKroneckerSVec<SDPA_FLOAT>(sol._Z.segment(off,prob.cs()[i]));
    ret.segment(off+A.cols(),prob.cs()[i])=ZI*sol._F.segment(off,prob.cs()[i]);
  }
  ASSERT(prob.cs().sum()==A.rows())
  return ret;
}
void Q1LowerBound::writeJGVTK(const std::string& path,const SMat& JG) const
{
  Coli blk;
  blk.resize(4);
  blk[1]=_probTpl.cs().sum()-_probTpl.cs()[2];
  blk[0]=_probTpl.nrVarXLambda()-blk[1];
  blk[2]=_probTpl.cs()[0];
  blk[3]=_probTpl.cs().sum()-_probTpl.cs()[2];
  writeSMatVTK(JG,path,&blk,&blk);
}
Q1LowerBound::Vec Q1LowerBound::computeC() const
{
  Vec ret=-Vec::Unit(_probTpl.nrVarXLambda(),0);
  //ret.segment(1,ret.size()-1).setConstant(1E-2f);
  return ret;
}
//compute jacobian eigen
void Q1LowerBound::debugJGEigen(sizeType nrIter) const
{
  Mat6Xd vss;
  Vsss vsss;
  INFO("BeginDebugJGEigen")
  DEFINE_NUMERIC_DELTA_T(scalarD)
  for(sizeType i=0; i<nrIter; i++) {
    INFOV("Iter%ld",i)
    SDPSolution sol;
    vss.setRandom(6,nrPt()*nrDir());
    vsss.assign(1,Mat6Xd::Random(6,nrPt()*nrDir()));
    SOSProb prob=buildProb(vss);
    solveProb(prob,sol);
    //reference
    const SMat JRef=SDPA2ScalarD<scalarD>(computeJG(prob,sol));
    const Matd RHSRef=SDPA2ScalarD<scalarD,-1,-1>(computeDGDEps(vss,vsss,sol));
    //Eigen
    const SMat V=SDPA2ScalarD<scalarD>(computeVJGEigen(sol));
    const SMat J=SDPA2ScalarD<scalarD>(computeSJGEigen(sol)*computeJGEigen(prob,sol));
    const Matd RHS=SDPA2ScalarD<scalarD,-1,-1>(computeSJGEigen(sol)*computeDGDEpsEigen(vss,vsss,sol));
    //test
    if(i==0)
      writeJGVTK("JGEigen.vtk",J);
    const SMat VTJRefV=V.transpose()*SMat(JRef*V);
    const Matd VTRHSRef=V.transpose()*RHSRef;
    DEBUG_GRADIENT("JGEigen",JRef.norm(),(VTJRefV-J).norm())
    DEBUG_GRADIENT("RHSEigen",RHSRef.norm(),(VTRHSRef-RHS).norm())
  }
  INFO("EndDebugJGEigen")
}
SDPSolution::SMat Q1LowerBound::computeSJGEigen(const SDPSolution& sol) const
{
  SDPSolution::SMat ret;
  SDPSolution::STrips trips;
  sizeType off=_probTpl.A().cols();
  addBlockId<SDPA_FLOAT>(trips,0,0,_probTpl.A().cols(),1);
  for(sizeType i=0; i<_probTpl.cs().size(); i++) {
    addBlockI(trips,off,off,sol._sssZ1[i]);
    off+=sol._vssFZ1[i].cols();
    addBlockI(trips,off,off,sol._sssZ2[i]);
    off+=sol._vssFZ2[i].cols();
    addBlockId<SDPA_FLOAT>(trips,off,off,sol._vssFZ3[i].cols(),1);
    off+=sol._vssFZ3[i].cols();
  }
  ASSERT(off==_probTpl.A().rows()+_probTpl.A().cols())
  //assemble
  ret.resize(_probTpl.A().rows()+_probTpl.A().cols(),_probTpl.A().rows()+_probTpl.A().cols());
  ret.setFromTriplets(trips.begin(),trips.end());
  return ret;
}
SDPSolution::SMat Q1LowerBound::computeVJGEigen(const SDPSolution& sol) const
{
  SDPSolution::SMat ret;
  SDPSolution::STrips trips;
  sizeType off=_probTpl.A().cols();
  addBlockId<SDPA_FLOAT>(trips,0,0,_probTpl.A().cols(),1);
  for(sizeType i=0; i<_probTpl.cs().size(); i++) {
    sizeType offc=off;
    addBlock(trips,off,offc,sol._vssFZ1[i]);
    offc+=sol._vssFZ1[i].cols();
    addBlock(trips,off,offc,sol._vssFZ2[i]);
    offc+=sol._vssFZ2[i].cols();
    addBlock(trips,off,offc,sol._vssFZ3[i]);
    offc+=sol._vssFZ3[i].cols();
    ASSERT(offc==off+_probTpl.cs()[i])
    off=offc;
  }
  //assemble
  ret.resize(_probTpl.A().rows()+_probTpl.A().cols(),_probTpl.A().rows()+_probTpl.A().cols());
  ret.setFromTriplets(trips.begin(),trips.end());
  return ret;
}
SDPSolution::SMat Q1LowerBound::computeJGEigen(const SOSProb& prob,const SDPSolution& sol) const
{
  sizeType offr,offc;
  SDPSolution::SMat ret;
  SDPSolution::STrips trips;
  SDPSolution::SMat A=scalarD2SDPA(prob.A());
  SDPSolution::SMat ABlk1=A.block(0,0,prob.cs()[0],A.cols());
  SDPSolution::DMat ABlk1TV1=ABlk1.transpose()*sol.vssFZ(0);
  //top right
  addBlock(trips,0,A.cols(),ABlk1TV1);
  offr=A.cols()-(prob.cs().sum()-prob.cs()[0]);
  offc=A.cols()+prob.cs()[0];
  for(sizeType i=1; i<prob.cs().size(); offr+=prob.cs()[i],offc+=prob.cs()[i],i++)
    addBlock(trips,offr,offc,sol.vssFZ(i));
  ASSERT(offr==A.cols() && offc==A.rows()+A.cols())
  //bottom right
  offr=A.cols();
  for(sizeType i=0; i<prob.cs().size(); i++) {
    offr+=sol._vssFZ1[i].cols();
    addBlockI(trips,offr,offr,sol.sssInvZ2F1(i));
    offr+=sol._vssFZ2[i].cols();
    addBlockI(trips,offr,offr,sol._sssF2[i]);
    offr+=sol._vssFZ3[i].cols();
  }
  ASSERT(offr==A.rows()+A.cols())
  //bottom left
  addBlock(trips,A.cols(),0,ABlk1TV1.transpose().block(0,0,sol._vssFZ1[0].cols()+sol._vssFZ2[0].cols(),A.cols()));
  offr=A.cols()+prob.cs()[0];
  offc=A.cols()-(prob.cs().sum()-prob.cs()[0]);
  for(sizeType i=1; i<prob.cs().size(); offc+=prob.cs()[i],i++) {
    addBlock(trips,offr,offc,sol._vssFZ1[i].transpose());
    offr+=sol._vssFZ1[i].cols();
    addBlock(trips,offr,offc,sol._vssFZ2[i].transpose());
    offr+=sol._vssFZ2[i].cols();
    offr+=sol._vssFZ3[i].cols();
  }
  ASSERT(offr==A.rows()+A.cols() && offc==A.cols())
  //assemble
  ret.resize(A.rows()+A.cols(),A.rows()+A.cols());
  ret.setFromTriplets(trips.begin(),trips.end());
  return ret;
}
SDPSolution::DMat Q1LowerBound::computeDGDEpsEigen(const Mat6Xd& vss,const Vsss& dVdEps,const SDPSolution& sol) const
{
  SOSProb prob;
  SDPSolution::SMat ABlk1;
  SDPSolution::Vec ABlk1X;
  SDPSolution::SMat A;
  SDPSolution::DMat ret=SDPSolution::DMat::Zero(_probTpl.A().rows()+_probTpl.A().cols(),(sizeType)dVdEps.size());
  for(sizeType i=0; i<(sizeType)dVdEps.size(); i++) {
    prob=buildProb(vss,&dVdEps[i]);
    A=scalarD2SDPA(prob.A());
    ABlk1=A.block(0,0,prob.cs()[0],A.cols());
    ret.col(i).segment(0,A.cols())=ABlk1.transpose()*sol._Z.segment(0,prob.cs()[0]);
    ABlk1X=ABlk1*sol._X;
    sizeType off=A.cols();
    ret.col(i).segment(off,sol._vssFZ1[0].cols())=sol._vssFZ1[0].transpose()*ABlk1X;
    off+=sol._vssFZ1[0].cols();
    ret.col(i).segment(off,sol._vssFZ2[0].cols())=sol._vssFZ2[0].transpose()*ABlk1X;
  }
  return ret;
}
//compute jacobian symmetric
void Q1LowerBound::debugJGSymmetric(sizeType nrIter) const
{
  Mat6Xd vss;
  Vsss vsss;
  SOSProb prob;
  Vec DXDEps,DXDEpsRef;
  INFO("BeginDebugJGSymmetric")
  DEFINE_NUMERIC_DELTA_T(scalarD)
  for(sizeType i=0; i<nrIter; i++) {
    INFOV("Iter%ld",i)
    SDPSolution sol;
    //problem
    std::string pathProb="Problem"+std::to_string(i)+".dat";
    std::string pathSol="Solution"+std::to_string(i)+".dat";
    if(exists(pathProb) && exists(pathSol)) {
      boost::filesystem::ifstream is(pathProb,ios::binary);
      readBinaryData(vss,is);
      prob=buildProb(vss);
      sol.SerializableBase::read(pathSol);
    } else {
      vss.setRandom(6,nrPt()*nrDir());
      prob=buildProb(vss);
      solveProb(prob,sol);
      boost::filesystem::ofstream os(pathProb,ios::binary);
      writeBinaryData(vss,os);
      sol.SerializableBase::write(pathSol);
    }
    //analysis
    std::string pathDXDEps="DXDEps"+std::to_string(i)+".dat";
    if(exists(pathDXDEps)) {
      boost::filesystem::ifstream is(pathDXDEps,ios::binary);
      readBinaryData(vsss,is);
      readBinaryData(DXDEps,is);
      SMat J=SDPA2ScalarD<scalarD>(computeJGEigen(prob,sol));
      Matd RHS=SDPA2ScalarD<scalarD,-1,-1>(computeDGDEpsEigen(vss,vsss,sol));
      INFOV("EigenRes: %f",(J*DXDEps+RHS).cwiseAbs().maxCoeff())
    } else {
      vsss.assign(1,Mat6Xd::Random(6,nrPt()*nrDir()));
      SMat J=SDPA2ScalarD<scalarD>(computeJGEigen(prob,sol));
      Eigen::SparseQR<SMat,Eigen::COLAMDOrdering<sizeType>> invJ(J);
      Matd RHS=SDPA2ScalarD<scalarD,-1,-1>(computeDGDEpsEigen(vss,vsss,sol));
      DXDEps=-invJ.solve(RHS);
      INFOV("EigenRes: %f",(J*DXDEps+RHS).cwiseAbs().maxCoeff())
      boost::filesystem::ofstream os(pathDXDEps,ios::binary);
      writeBinaryData(vsss,os);
      writeBinaryData(DXDEps,os);
    }
    //analysis reference
    DXDEpsRef=DXDEps.segment(0,prob.A().cols());
    for(sizeType i=0,off=prob.A().cols(); i<prob.cs().size(); off+=prob.cs()[i],i++)
      DXDEpsRef=concat<Vec>(DXDEpsRef,DXDEps.segment(off,sol._vssFZ1[i].cols()));
    Vec RHS=SDPA2ScalarD<scalarD,-1,1>(computeDGDEpsSymmetric(vss,vsss,prob,sol));
    Vec RES=SDPA2ScalarD<scalarD,-1,1>(computeJGSymmetric(prob,sol))*DXDEpsRef+RHS;
    DEBUG_GRADIENT("JGSymmetric",RHS.norm(),RES.cwiseAbs().maxCoeff())
  }
  INFO("EndDebugJGSymmetric")
}
SDPSolution::DMat Q1LowerBound::computeJGSymmetric(const SOSProb& prob,const SDPSolution& sol) const
{
  SDPSolution::DMat ret;
  SDPSolution::SMat A=scalarD2SDPA(prob.A());
  sizeType rows=A.cols();
  for(sizeType i=0; i<(sizeType)sol._vssFZ1.size(); i++)
    rows+=sol._vssFZ1[i].cols();
  ret.setZero(rows,rows);
  //diagonal block
  SDPSolution::SMat ABlk1=A.block(0,0,prob.cs()[0],A.cols());
  SDPSolution::DMat V21T_ABlk1=sol._vssFZ2[0].transpose()*ABlk1;
  addBlock(ret,0,0,-V21T_ABlk1.transpose()*(sol.sssInvF1Z2(0).asDiagonal()*V21T_ABlk1));
  sizeType off=A.cols()-(prob.cs().sum()-prob.cs()[0]);
  for(sizeType i=1; i<prob.cs().size(); off+=prob.cs()[i],i++)
    addBlock(ret,off,off,-sol._vssFZ2[i]*(sol.sssInvF1Z2(i).asDiagonal()*sol._vssFZ2[i].transpose()));
  //off-diagonal block
  SDPSolution::DMat V11T_ABlk1=sol._vssFZ1[0].transpose()*ABlk1;
  addBlock(ret,A.cols(),0,V11T_ABlk1);
  addBlock(ret,0,A.cols(),V11T_ABlk1.transpose());
  sizeType offr=A.cols()+sol._vssFZ1[0].cols();
  sizeType offc=A.cols()-(prob.cs().sum()-prob.cs()[0]);
  for(sizeType i=1; i<prob.cs().size(); offr+=sol._vssFZ1[i].cols(),offc+=prob.cs()[i],i++) {
    addBlock(ret,offr,offc,sol._vssFZ1[i].transpose());
    addBlock(ret,offc,offr,sol._vssFZ1[i]);
  }
  return ret;
}
SDPSolution::DMat Q1LowerBound::computeDGDEpsSymmetric(const Mat6Xd& vss,const Vsss& dVdEps,const SOSProb& prob,const SDPSolution& sol) const
{
  SDPSolution::DMat ret;
  SDPSolution::SMat ABlk1;
  SDPSolution::SMat ABlk1Diff;
  SDPSolution::Vec ABlk1DiffX,invF1B2;
  SDPSolution::SMat A=scalarD2SDPA(prob.A());
  SOSProb probDiff;
  sizeType rows=_probTpl.nrVarXLambda();
  for(sizeType i=0; i<(sizeType)sol._vssFZ1.size(); i++)
    rows+=sol._vssFZ1[i].cols();
  ret.setZero(rows,(sizeType)dVdEps.size());
  for(sizeType i=0; i<(sizeType)dVdEps.size(); i++) {
    probDiff=buildProb(vss,&dVdEps[i]);
    ABlk1=A.block(0,0,prob.cs()[0],A.cols());
    ABlk1Diff=scalarD2SDPA<scalarD>(probDiff.A().block(0,0,prob.cs()[0],A.cols()));
    ABlk1DiffX=ABlk1Diff*sol._X;
    //first, second entry: b1
    ret.col(i).segment(0,A.cols())=ABlk1Diff.transpose()*sol._Z.segment(0,prob.cs()[0]);
    //first, second entry: -{\partial{F^1}/\partial{X}}^T*{V_2^1}*invSssF1*{V_2^1}^T*b2
    invF1B2=sol._vssFZ2[0]*(sol.sssInvF1Z2(0).asDiagonal()*(sol._vssFZ2[0].transpose()*ABlk1DiffX));
    ret.col(i).segment(0,A.cols())-=ABlk1.transpose()*invF1B2;
    //third entry: invSssZ1*{V_1^1}^T*b2
    ret.col(i).segment(A.cols(),sol._vssFZ1[0].cols())=sol._vssFZ1[0].transpose()*ABlk1DiffX;
  }
  return ret;
}
//sensitivity
SDPSolution::Vec Q1LowerBound::computeDXDEps(const Mat6Xd& vss,const Vsss& dVdEps,const SOSProb& prob,const SDPSolution& sol) const
{
  SDPSolution::SMat J=computeJG(prob,sol);
  //Eigen::SparseQR<SDPSolution::SMat,Eigen::COLAMDOrdering<sizeType>> invJ(J);
  SDPSolution::DMat RHS=computeDGDEps(vss,dVdEps,sol);
  //return -invJ.solve(RHS).row(0);
  SDPSolution::DMat JD=J.toDense();
  SDPSolution::DMat SOL=JD.fullPivLu().solve(RHS);
  if(DEBUG_MODE) {
    INFOV("Res: %f",SDPA2ScalarD((JD*SOL-RHS).norm()))
  }
  return -SOL.row(0);
}
SDPSolution::Vec Q1LowerBound::computeDXDEpsEigen(const Mat6Xd& vss,const Vsss& dVdEps,const SOSProb& prob,const SDPSolution& sol) const
{
  SDPSolution::SMat J=computeJGEigen(prob,sol);
  //Eigen::SparseQR<SDPSolution::SMat,Eigen::COLAMDOrdering<sizeType>> invJ(J);
  SDPSolution::DMat RHS=computeDGDEpsEigen(vss,dVdEps,sol);
  //return -invJ.solve(RHS).row(0);
  SDPSolution::DMat JD=J.toDense();
  SDPSolution::DMat SOL=JD.fullPivLu().solve(RHS);
  if(DEBUG_MODE) {
    INFOV("Res: %f",SDPA2ScalarD((JD*SOL-RHS).norm()))
  }
  return -SOL.row(0);
}
SDPSolution::Vec Q1LowerBound::computeDXDEpsSymmetric(const Mat6Xd& vss,const Vsss& dVdEps,const SOSProb& prob,const SDPSolution& sol) const
{
  SDPSolution::DMat J=computeJGSymmetric(prob,sol),JTmp=J;
  SDPSolution::DMat RHS=computeDGDEpsSymmetric(vss,dVdEps,prob,sol),RHSTmp=RHS;
  SYSVInterface<SDPA_FLOAT>::solve(J,RHS);
  if(DEBUG_MODE) {
    INFOV("Res: %f",SDPA2ScalarD((JTmp*RHS-RHSTmp).norm()))
  }
  return -RHS.row(0);
}
void Q1LowerBound::debugDXDEps(sizeType nrIter) const
{
  Vsss dvss;
  Mat6Xd vss;
  Vec DXDEps;
  INFO("BeginDebugDXDEps")
  DEFINE_NUMERIC_DELTA_T(scalarD)
  for(sizeType i=0; i<nrIter; i++) {
    vss.setRandom(6,nrPt()*nrDir());
    dvss.assign(1,Mat6Xd::Random(6,nrPt()*nrDir()));
    //step1
    TBEG("Solve Q1");
    SDPSolution sol;
    SOSProb prob=buildProb(vss);
    scalarD Q1=solveProb(prob,sol,0.0f,false);
    if(Q1<=0) {
      i--;
      continue;
    }
    TEND();
    //step2
    TBEG("Solve DXDEps");
    //DXDEps=SDPA2ScalarD<scalarD,-1,1>(computeDXDEps(vss,dvss,prob,sol));
    //DXDEps=SDPA2ScalarD<scalarD,-1,1>(computeDXDEpsEigen(vss,dvss,prob,sol));
    DXDEps=SDPA2ScalarD<scalarD,-1,1>(computeDXDEpsSymmetric(vss,dvss,prob,sol));
    TEND();
    //step3
    TBEG("Solve Q12");
    SDPSolution sol2;
    //sol2._currentPt=sol._currentPt;
    SOSProb prob2=buildProb(vss+dvss[0]*DELTA);
    scalarD Q12=solveProb(prob2,sol2,0.0f,false);
    TEND();
    INFOV("Iter%ld Q1: %f Q12: %f DEDEps: %f Err: %f",i,Q1,Q12,DXDEps[0],DXDEps[0]-(Q12-Q1)/DELTA)
  }
  INFO("EndDebugDXDEps")
}
#undef DEBUG_MODE

#ifdef SDPA_DD_SUPPORT
#include "internal/SOSPolynomialScalarQ.h"
#include "SDPADDInterface.h"
#include "mpack/mlapack_dd.h"
#include "SparseUtils.h"

USE_PRJ_NAMESPACE

using namespace sdpa;

SDPAInterface::SDPAInterface()
{
  reset();
}
void SDPAInterface::reset(sdpa::Parameter::parameterType type)
{
  _param.setDefaultParameter(type);
  _com=ComputeTime();
}
void SDPAInterface::solve(bool output,sdpa::Solutions* ptInit)
{
  TimeStart(TOTAL_TIME_START1);
  static const double KAPPA=1.2;
  int m=_inputData.b.nDim;
  int nrSDPCone=(int)_blockStruct.size();
  FILE* fpOut=fopen("OutputSDP.txt","w");
  FILE* Display=output?stdout:NULL;//fopen("Display.txt","w");
  Newton newton(m,nrSDPCone,&_blockStruct[0],0,NULL,0);
  // 2008/03/12 kazuhide nakata
  Chordal chordal;
  // rMessage("ordering bMat: start");
  chordal.ordering_bMat(m,nrSDPCone,_inputData,fpOut);
  // rMessage("ordering bMat: end");
  newton.initialize_bMat(m,chordal,_inputData,fpOut);
  chordal.terminate();
  // rMessage("newton.computeFormula_SDP: start");
  newton.computeFormula_SDP(_inputData,0.0,KAPPA);
  // rMessage("newton.computeFormula_SDP: end");
  // set initial solutions.
  WorkVariables work;
  DenseLinearSpace initPt_xMat;
  DenseLinearSpace initPt_zMat;
  _currentPt.initialize(m,nrSDPCone,&_blockStruct[0],0,NULL,0,_param.lambdaStar,_com);
  work.initialize(m,nrSDPCone,&_blockStruct[0],0,NULL,0);
  if(ptInit) {
    ComputeTime com;
    _currentPt.yVec.copyFrom(ptInit->yVec);
    _currentPt.xMat.copyFrom(ptInit->xMat);
    _currentPt.zMat.copyFrom(ptInit->zMat);
    _currentPt.computeInverse(work,com);
    initPt_xMat.copyFrom(_currentPt.xMat);
    initPt_zMat.copyFrom(_currentPt.zMat);
  }
  Residuals initRes(m,nrSDPCone,&_blockStruct[0],0,NULL,0,_inputData,_currentPt);
  Residuals currentRes;
  currentRes.copyFrom(initRes);
  // rMessage("initial currentRes = ");
  // currentRes.display(Display);
  StepLength alpha;
  DirectionParameter beta(_param.betaStar);
  Switch reduction(Switch::ON);
  AverageComplementarity mu(_param.lambdaStar);
  if(ptInit)
    mu.initialize(_currentPt);
  // rMessage("init mu"); mu.display();
  RatioInitResCurrentRes theta(_param,initRes);
  SolveInfo solveInfo(_inputData,_currentPt,mu.initial,_param.omegaStar);
  Phase phase(initRes,solveInfo,_param,_currentPt.nDim);
  int pIteration = 0;
  IO::printHeader(fpOut,Display);
  // -----------------------------------------------------
  // Here is MAINLOOP
  // -----------------------------------------------------
  TimeStart(MAIN_LOOP_START1);
  // explicit maxIteration
  // param.maxIteration = 2;
  while (phase.updateCheck(currentRes, solveInfo, _param)
         && pIteration < _param.maxIteration) {
    // rMessage(" turn hajimari " << pIteration );
    // Mehrotra's Predictor
    TimeStart(MEHROTRA_PREDICTOR_START1);
    // set variable of Mehrotra
    reduction.MehrotraPredictor(phase);
    beta.MehrotraPredictor(phase, reduction, _param);
    // rMessage("reduction = "); reduction.display();
    // rMessage("phase = "); phase.display();
    // rMessage("beta.predictor.value = " << beta.value);
    // rMessage(" mu = " << mu.current);
    // rMessage("currentPt = "); currentPt.display();
    bool isSuccessCholesky;
    isSuccessCholesky = newton.Mehrotra(Newton::PREDICTOR,
                                        _inputData, _currentPt,
                                        currentRes,
                                        mu, beta, reduction,
                                        phase,work,_com);
    if (isSuccessCholesky == false) {
      break;
    }
    // rMessage("newton predictor = "); newton.display();
    TimeEnd(MEHROTRA_PREDICTOR_END1);
    _com.Predictor += TimeCal(MEHROTRA_PREDICTOR_START1,
                              MEHROTRA_PREDICTOR_END1);
    TimeStart(STEP_PRE_START1);
    alpha.MehrotraPredictor(_inputData, _currentPt, phase, newton,
                            work, _com);
    // rMessage("alpha predictor = "); alpha.display();
    TimeStart(STEP_PRE_END1);
    _com.StepPredictor += TimeCal(STEP_PRE_START1,STEP_PRE_END1);
    // rMessage("alphaStar = " << param.alphaStar);
    // Mehrotra's Corrector
    // rMessage(" Corrector ");
    TimeStart(CORRECTOR_START1);
    beta.MehrotraCorrector(phase,alpha,_currentPt, newton,mu,_param);
    // rMessage("beta corrector = " << beta.value);
#if 1 // 2007/08/29 kazuhide nakata
    // add stopping criteria: objValPrimal < ObjValDual
    //	if ((pIteration > 10) &&
    if ((phase.value == SolveInfo::pdFEAS) &&
        ((beta.value > 5)||(solveInfo.objValPrimal < solveInfo.objValDual))) {
      break;
    }
#endif
    newton.Mehrotra(Newton::CORRECTOR,
                    _inputData, _currentPt, currentRes,
                    mu, beta, reduction, phase,work,_com);
    // rMessage("currentPt = "); currentPt.display();
    // rMessage("newton corrector = "); newton.display();
    TimeEnd(CORRECTOR_END1);
    _com.Corrector += TimeCal(CORRECTOR_START1,
                              CORRECTOR_END1);
    TimeStart(CORRECTOR_STEP_START1);
    alpha.MehrotraCorrector(_inputData, _currentPt, phase,
                            reduction, newton, mu, theta,
                            work, _param, _com);
    // rMessage("alpha corrector = "); alpha.display();
    TimeEnd(CORRECTOR_STEP_END1);
    _com.StepCorrector += TimeCal(CORRECTOR_STEP_START1,
                                  CORRECTOR_STEP_END1);
    // the end of Corrector
    IO::printOneIteration(pIteration, mu, theta, solveInfo,
                          alpha, beta, fpOut, Display);
    if (_currentPt.update(alpha,newton,work,_com)==false) {
      // if step length is too short,
      // we finish algorithm
      rMessage("cannot move");
      //   memo by kazuhide nakata
      //   StepLength::MehrotraCorrector
      //   thetaMax*mu.initial -> thetamax*thetaMax*mu.initial
      break;
    }
    // rMessage("currentPt = "); currentPt.display();
    // rMessage("updated");
    theta.update(reduction,alpha);
    mu.update(_currentPt);
    currentRes.update(m,_inputData, _currentPt, _com);
    theta.update_exact(initRes,currentRes);
    if(ptInit) {
      solveInfo.update(_inputData, initPt_xMat, initPt_zMat, _currentPt,
                       currentRes, mu, theta, _param);
    } else {
      solveInfo.update(_param.lambdaStar,_inputData, _currentPt,
                       currentRes, mu, theta, _param);
    }
    // 2007/09/18 kazuhide nakata
    // print information of ObjVal, residual, gap, complementarity
    //	solveInfo.check(inputData, currentPt, currentRes, mu, theta, param);
    pIteration++;
    fflush(stdout);
  } // end of MAIN_LOOP
  TimeEnd(MAIN_LOOP_END1);
  _com.MainLoop = TimeCal(MAIN_LOOP_START1, MAIN_LOOP_END1);
  currentRes.compute(m,_inputData,_currentPt);
  TimeEnd(TOTAL_TIME_END1);
  _com.TotalTime = TimeCal(TOTAL_TIME_START1, TOTAL_TIME_END1);
#if REVERSE_PRIMAL_DUAL
  phase.reverse();
#endif
  //IO::printLastInfo(pIteration, mu, theta, solveInfo, alpha, beta,
  //                  currentRes, phase, _currentPt, _com.TotalTime,
  //                  _inputData, work, _com, _param, fpOut, Display);
  // _com.display(fpOut);
  fclose(fpOut);
}
void SDPAInterface::check()
{
  int m=_inputData.b.nDim;
  int nrSDPCone=(int)_blockStruct.size();
  std::vector<dd_real> ZDotFI(m,0.0);
  //print y
  Vec xVec=xG();
  INFO("Solution Check:")
  printMat("x",xVec);
  //print X
  for(sizeType i=1; i<nrSDPCone; i++) {
    Mat f=FG(i);
    Mat z=ZG(i);
    Mat fRef=F0(i);
    for(sizeType j=0; j<m; j++) {
      ZDotFI[j]+=(FI(i,j)*z.transpose()).trace();
      fRef+=FI(i,j)*xVec[j];
    }
    printMat("z"+std::to_string(i),z);
    printMat("f"+std::to_string(i),f);
    printMat("fRef"+std::to_string(i),fRef);
    printMat("z"+std::to_string(i)+"*f"+std::to_string(i),z*f);
  }
  for(sizeType j=0; j<m; j++)
    printf("ZDotFI=%10.10f, BI=%10.10f\n",ZDotFI[j].x[0],_inputData.b.ele[j].x[0]);
}
template <typename T>
void SDPAInterface::setProblem(const typename PSDConeTraits<T>::Vec& b,const std::vector<typename PSDConeTraits<T>::PSDCone>& cones)
{
  _inputData=InputData();
  int nrSDPCone=(int)cones.size();
  int m=(int)b.size();
  //b
  _inputData.initialize_bVec(m);
  Eigen::Map<Vec>(_inputData.b.ele,_inputData.b.nDim)=b.template cast<dd_real>();
  //A,C
  _blockStruct.resize(nrSDPCone);
  _SDP_index.assign(m+1,std::vector<int>());
  for(int i=0; i<nrSDPCone; i++) {
    _blockStruct[i]=cones[i][0].rows();
    countNonZeros<T>(cones[i],i);
  }
  _inputData.A=new sdpa::SparseLinearSpace[m];
  std::vector<int> SDP_sp_index(nrSDPCone);
  std::vector<int> SDP_sp_blockStruct(nrSDPCone);
  std::vector<int> SDP_sp_NonZeroNumber(nrSDPCone);
  for(sizeType k=0 ; k<m+1; k++) {
    std::sort(_SDP_index[k].begin(),_SDP_index[k].end());
    int SDP_sp_nBlock=0;
    sizeType previous_index=-1;
    sizeType index;
    for(sizeType i=0; i<(sizeType)_SDP_index[k].size(); i++) {
      index=_SDP_index[k][i];
      if(previous_index!=index) {
        SDP_sp_index[SDP_sp_nBlock]=index;
        SDP_sp_blockStruct[SDP_sp_nBlock]=_blockStruct[index];
        SDP_sp_NonZeroNumber[SDP_sp_nBlock]=1;
        previous_index=index;
        SDP_sp_nBlock++;
      } else {
        SDP_sp_NonZeroNumber[SDP_sp_nBlock-1]++;
      }
    }
    if (k==0)
      _inputData.C.initialize(SDP_sp_nBlock,&SDP_sp_index[0],&SDP_sp_blockStruct[0],&SDP_sp_NonZeroNumber[0],0,NULL,NULL,NULL,0,NULL);
    else
      _inputData.A[k-1].initialize(SDP_sp_nBlock,&SDP_sp_index[0],&SDP_sp_blockStruct[0],&SDP_sp_NonZeroNumber[0],0,NULL,NULL,NULL,0,NULL);
  }
  for(int i=0; i<nrSDPCone; i++)
    fillInputData<T>(cones[i],i);
  //init index
  _inputData.initialize_index(nrSDPCone,0,0,_com);
  //if possible , change C and A to Dense
  //_inputData.C.changeToDense();
  //for(int i=0; i<m; ++i)
  //  _inputData.A[i].changeToDense();
}
void SDPAInterface::getSolution(SDPSolution& sol,scalarD eps) const
{
  sol._currentPt.reset(new Solutions);
  sol._currentPt->copyFrom(const_cast<Solutions&>(_currentPt));
  int nrPSDCones=(int)_blockStruct.size(),nrFZ=0;
  for(sizeType i=0; i<nrPSDCones; i++)
    nrFZ+=_blockStruct[i]*(_blockStruct[i]+1)/2;
  sol._F.resize(nrFZ);
  sol._Z.resize(nrFZ);
  sol._X=convert<SDPSolution::Vec,Vec>(xG());
  sol._vssFZ1.resize(nrPSDCones);
  sol._vssFZ2.resize(nrPSDCones);
  sol._vssFZ3.resize(nrPSDCones);
  sol._sssF1.resize(nrPSDCones);
  sol._sssF2.resize(nrPSDCones);
  sol._sssZ1.resize(nrPSDCones);
  sol._sssZ2.resize(nrPSDCones);
  for(sizeType i=0,off=0; i<nrPSDCones; i++) {
    Mat FI=FG(i);
    Mat ZI=ZG(i);
    Vec FISVec=toSVec<dd_real>(FI);
    Vec ZISVec=toSVec<dd_real>(ZI);
    sol._F.segment(off,FISVec.size())=convert<SDPSolution::DMat,Mat>(FISVec);
    sol._Z.segment(off,ZISVec.size())=convert<SDPSolution::DMat,Mat>(ZISVec);
    //find eigenvalues
    Mat evec;
    Vec eval;
    {
      const char *jobz="Vectors",*uplo="Upper";
      mpackint n=FI.rows(),lda=n,lwork=-1,info;
      dd_real wkopt;
      Vec work;
      evec=FI+ZI;
      eval.resize(n);
      Rsyev(jobz,uplo,n,evec.data(),lda,eval.data(),&wkopt,&lwork,&info);
      lwork=(mpackint)wkopt.x[0];
      work.resize(lwork);
      Rsyev(jobz,uplo,n,evec.data(),lda,eval.data(),work.data(),&lwork,&info);
    }
    Vec Z1,Z2,F1,F2;
    Mat vssFZ1,vssFZ2,vssFZ3;
    Vec EF=(evec.transpose()*(FI*evec)).diagonal();
    Vec EZ=(evec.transpose()*(ZI*evec)).diagonal();
    symmetricKroneckerEigensystemTrisection<dd_real>
    (evec,EF,EZ,vssFZ1,vssFZ2,vssFZ3,Z1,Z2,F1,F2,eps);
    //convert
    sol._sssZ1[i]=convert<SDPSolution::Vec,Vec>(Z1);
    sol._sssZ2[i]=convert<SDPSolution::Vec,Vec>(Z2);
    sol._sssF1[i]=convert<SDPSolution::Vec,Vec>(F1);
    sol._sssF2[i]=convert<SDPSolution::Vec,Vec>(F2);
    sol._vssFZ1[i]=convert<SDPSolution::DMat,Mat>(vssFZ1);
    sol._vssFZ2[i]=convert<SDPSolution::DMat,Mat>(vssFZ2);
    sol._vssFZ3[i]=convert<SDPSolution::DMat,Mat>(vssFZ3);
    off+=FISVec.size();
  }
}
template <typename T>
void SDPAInterface::fillInputData(const typename PSDConeTraits<T>::PSDCone& cone,int bid)
{
  int m=_inputData.b.nDim;
  ASSERT((int)cone.size()==m+1)
  for(sizeType i=0; i<(sizeType)cone.size(); i++) {
    const typename PSDConeTraits<T>::SMat& FI=cone[i];
    for(sizeType k=0; k<FI.outerSize(); ++k)
      for(typename PSDConeTraits<T>::SMat::InnerIterator it(FI,k); it; ++it)
        if(it.col()>=it.row() && it.value()!=0.0) {
          if(i==0)
            _inputData.C.setElement_SDP(bid,it.col(),it.row(),it.value());
          else _inputData.A[i-1].setElement_SDP(bid,it.col(),it.row(),it.value());
        }
  }
}
template <typename T>
void SDPAInterface::countNonZeros(const typename PSDConeTraits<T>::PSDCone& cone,int bid)
{
  int m=_inputData.b.nDim;
  ASSERT((int)cone.size()==m+1)
  for(sizeType i=0; i<(sizeType)cone.size(); i++) {
    const typename PSDConeTraits<T>::SMat& FI=cone[i];
    for(sizeType k=0; k<FI.outerSize(); ++k)
      for(typename PSDConeTraits<T>::SMat::InnerIterator it(FI,k); it; ++it)
        if(it.col()>=it.row() && it.value()!=0.0)
          _SDP_index[i].push_back(bid);
  }
}
Eigen::Map<const SDPAInterface::Mat> SDPAInterface::ZG(int bid) const
{
  const DenseMatrix& m=_currentPt.xMat.SDP_block[bid];
  ASSERT(m.nRow==m.nCol && bid<_currentPt.xMat.SDP_nBlock)
  return Eigen::Map<const SDPAInterface::Mat>(m.de_ele,m.nRow,m.nCol);
}
Eigen::Map<const SDPAInterface::Mat> SDPAInterface::FG(int bid) const
{
  const DenseMatrix& m=_currentPt.zMat.SDP_block[bid];
  ASSERT(m.nRow==m.nCol && bid<_currentPt.zMat.SDP_nBlock)
  return Eigen::Map<const SDPAInterface::Mat>(m.de_ele,m.nRow,m.nCol);
}
SDPAInterface::Vec SDPAInterface::xG() const
{
#if REVERSE_PRIMAL_DUAL
  return -Eigen::Map<const Vec>(_currentPt.yVec.ele,_currentPt.yVec.nDim);
#else
  return Eigen::Map<const Vec>(_currentPt.yVec.ele,_currentPt.yVec.nDim);
#endif
}
//helper
SDPAInterface::Mat SDPAInterface::F0(int bid) const
{
  return convert(_inputData.C,bid);
}
SDPAInterface::Mat SDPAInterface::FI(int bid,int mid) const
{
  return convert(_inputData.A[mid],bid);
}
SDPAInterface::Mat SDPAInterface::convert(const SparseLinearSpace& ms,int bid) const
{
  Mat ret;
  ret.setZero(_blockStruct[bid],_blockStruct[bid]);
  const sdpa::SparseMatrix* m=NULL;
  for(sizeType i=0; i<ms.SDP_sp_nBlock; i++)
    if(ms.SDP_sp_index[i]==bid) {
      m=ms.SDP_sp_block+i;
      break;
    }
  if(m==NULL)
    return ret;
  ASSERT(m->nRow==_blockStruct[bid])
  ASSERT(m->nCol==_blockStruct[bid])
  if(m->type==sdpa::SparseMatrix::DENSE) {
    for(int r=0,i=0; r<m->nRow; r++)
      for(int c=0; c<m->nCol; c++,i++) {
        ret(r,c)=m->de_ele[i];
        ret(c,r)=m->de_ele[i];
      }
  } else {
    for(int i=0; i<m->NonZeroCount; i++) {
      ret(m->row_index[i],m->column_index[i])=m->sp_ele[i];
      ret(m->column_index[i],m->row_index[i])=m->sp_ele[i];
    }
  }
  return ret;
}
void SDPAInterface::printMat(const std::string& name,const Mat& m) const
{
  INFO(name.c_str())
  for(sizeType r=0; r<m.rows(); r++) {
    for(sizeType c=0; c<m.cols(); c++)
      printf("%10.10f ",m(r,c).x[0]);
    std::cout << std::endl;
  }
}
//instance
PRJ_BEGIN
template void SDPAInterface::setProblem<dd_real>(const typename PSDConeTraits<dd_real>::Vec& b,const std::vector<typename PSDConeTraits<dd_real>::PSDCone>& cones);
template void SDPAInterface::setProblem<scalarF>(const typename PSDConeTraits<scalarF>::Vec& b,const std::vector<typename PSDConeTraits<scalarF>::PSDCone>& cones);
template void SDPAInterface::setProblem<scalarD>(const typename PSDConeTraits<scalarD>::Vec& b,const std::vector<typename PSDConeTraits<scalarD>::PSDCone>& cones);
PRJ_END
#endif

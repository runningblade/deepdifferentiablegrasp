from distance import filter_files
from graspPlanner import mkdir
from skimage import io
import trimesh,os,re,random,math,transforms3d,pickle
import numpy as np
import torch
import xml.etree.ElementTree as ET
import xml.dom.minidom as minidom
from hand import Hand,trimesh_to_vtk,write_vtk

class Data:
    def __init__(self,data_path,blender_path):
        self.shapes_path=data_path+'raw_meshes'
        self.scaled_shapes_path=data_path+'scaled_obj_meshes'
        self.scaled_shapes_aug_path=data_path+'scaled_obj_meshes_aug'
        self.good_shapes_path=data_path+'good_shapes'
        self.blender_path=blender_path
    
    def get_center_of_mesh(self,func_input):
        if isinstance(func_input,str):
            mesh=trimesh.load_mesh(func_input)
            vertices=mesh.vertices
        else:
            vertices=func_input
        minX=999999
        minY=999999
        minZ=999999
        maxX=-999999
        maxY=-999999
        maxZ=-999999
        for i in range(vertices.shape[0]):
            if vertices[i][0]>maxX:
                maxX=vertices[i][0]
            if vertices[i][1]>maxY:
                maxY=vertices[i][1]
            if vertices[i][2]>maxZ:
                maxZ=vertices[i][2]
            if vertices[i][0]<minX:
                minX=vertices[i][0]
            if vertices[i][1]<minY:
                minY=vertices[i][1]
            if vertices[i][2]<minZ:
                minZ=vertices[i][2]
        center=np.zeros(3)
        center[0]=(minX+maxX)*0.5
        center[1]=(minY+maxY)*0.5
        center[2]=(minZ+maxZ)*0.5
        return center
    
    def scale_shapes(self,mesh_path,mesh_center,scale,test=False):
        if os.path.exists(self.scaled_shapes_path+"/"+re.split("/",mesh_path)[-1][:-4]+"_scaled.obj"):
            return
        mesh=trimesh.load_mesh(mesh_path)
        max_extend=max(mesh.extents)*scale
        if not mesh.is_watertight and not test:
            return
        center_ori=self.get_center_of_mesh(mesh.vertices)
        mesh.vertices-=center_ori
        mesh.vertices=mesh.vertices*(1/max(mesh.extents))*max_extend
        mesh.vertices+=mesh_center
        mesh.export(self.scaled_shapes_path+"/"+re.split("/",mesh_path)[-1][:-4]+"_scaled.obj",'obj')
    
    def mesh_preprocess(self):
        shape_list=filter_files(self.shapes_path,".stl",[])
        if len(shape_list)==0:
            filter_files(self.shapes_path,".off",shape_list)
        if os.path.exists(self.scaled_shapes_path):
            return
        mkdir(self.scaled_shapes_path)
        for i in range(len(shape_list)):
            print(i)
            if os.path.getsize(shape_list[i])>1500000:
                continue
            self.scale_shapes(shape_list[i],np.asarray([0,0,0]),0.01)

    def mesh_process(self):
        scaled_shapes_list=filter_files(self.scaled_shapes_path,"_scaled.obj",[])
        binary="../ddg_build/mainObjMeshToSDF"
        for i in range(len(scaled_shapes_list)):
            print(i)
            if os.path.exists(scaled_shapes_list[i]+".BVH.dat") and\
               os.path.exists(scaled_shapes_list[i]+".PSet.dat") and\
               os.path.getsize(scaled_shapes_list[i]+".BVH.dat")>0 and\
               os.path.getsize(scaled_shapes_list[i]+".PSet.dat")>0:
                continue
            print(scaled_shapes_list[i])
            command=binary+' '+scaled_shapes_list[i]+' '+'-0.01'+' '+'0'+' '+'0'
            os.system(command)
            if os.path.exists(scaled_shapes_list[i]+".BVH.dat") and\
               os.path.exists(scaled_shapes_list[i]+".PSet.dat"):
                if os.path.getsize(scaled_shapes_list[i]+".BVH.dat")==0 or\
                   os.path.getsize(scaled_shapes_list[i]+".PSet.dat")==0:
                    os.remove(scaled_shapes_list[i]+".BVH.dat")
                    os.remove(scaled_shapes_list[i]+".PSet.dat")
            else:
                if os.path.exists(scaled_shapes_list[i]+".BVH.dat"):
                    os.remove(scaled_shapes_list[i]+".BVH.dat")
                if os.path.exists(scaled_shapes_list[i]+".PSet.dat"):
                    os.remove(scaled_shapes_list[i]+".PSet.dat")
    
    def max_extent(self):
        max_ext=-999999
        scaled_shapes_list=[]
        filter_files(self.scaled_shapes_path,'.smoothed.obj',scaled_shapes_list)
        for i in range(len(scaled_shapes_list)):
            mesh=trimesh.load_mesh(scaled_shapes_list[i])
            if max(mesh.extents)>max_ext:
                print(scaled_shapes_list[i])
                max_ext=max(mesh.extents)
                print(max_ext) 

    def render_depths_for_shapes(self):
        scaled_shapes_list=filter_files(self.scaled_shapes_path,".BVH.dat",[])
        scaled_shapes_list_=filter_files(self.scaled_shapes_path,"_scaled.obj",[])
        for i in range(len(scaled_shapes_list_)):
            if scaled_shapes_list_[i]+'.BVH.dat' not in scaled_shapes_list:
                os.remove(scaled_shapes_list_[i])
        for i in range(len(scaled_shapes_list)):
            scaled_shape_name=scaled_shapes_list[i][:-8]+'.smoothed.obj'
            output_folder=scaled_shape_name+'.depths'
            depth_list=filter_files(output_folder,".jpg",[])
            if len(depth_list)==5:
                continue
            mkdir(output_folder)
            mesh_zrot=0.0
            mesh_yrot=0.0
            mesh_xrot=0.0
            z_rotation=transforms3d.axangles.axangle2aff([0,0,1],mesh_zrot,None)
            y_rotation=transforms3d.axangles.axangle2aff([0,1,0],mesh_yrot,None)
            x_rotation=transforms3d.axangles.axangle2aff([1,0,0],mesh_xrot,None)
            mesh_R=np.matmul(x_rotation,y_rotation)
            mesh_R=np.matmul(mesh_R,z_rotation)
            render_cmd='%s --background --python %s -- --output_folder %s %s --zrot %f --yrot %f --xrot %f' \
                % (self.blender_path,"render_blender.py",output_folder,scaled_shape_name,mesh_zrot,mesh_yrot,mesh_xrot)
            os.system(render_cmd)
            image_list=[]
            filter_files(output_folder,".jpg",image_list)
            for j in range(len(image_list)):
                if "_depth_" not in image_list[j]:
                    os.remove(image_list[j])
            os.remove('Image0001.jpg')

    def generate_depths_for_shapes(self):
        scaled_shapes_list=[]
        filter_files(self.scaled_shapes_path,".BVH.dat",scaled_shapes_list)
        for i in range(len(scaled_shapes_list)):
            print('Generating depths tensor: '+str(i))
            scaled_shape_name=scaled_shapes_list[i][:-8]+'.smoothed.obj'
            if os.path.exists(scaled_shape_name+'.depth'):
                exsit_depth_tensor=torch.load(scaled_shape_name+'.depth')
                if len(exsit_depth_tensor)<5:
                    print(scaled_shape_name+'.depth')
                else:
                    continue
            depth_folder=scaled_shape_name+'.depths'
            depth_list=[]
            filter_files(depth_folder,".jpg",depth_list)
            if len(depth_list)<5:
                print(depth_folder)
            depth_tensor=torch.zeros(len(depth_list),224,224,3)
            for j in range(len(depth_list)):
                depth_tensor[j]=torch.tensor(io.imread(depth_list[j]))
            depth_tensor=depth_tensor.permute(0,3,1,2)/255.0
            torch.save(depth_tensor,scaled_shape_name+'.depth')

    def generate_grids_for_shapes(self,show):
        resolution=40
        grid_length=3.5 #user define
        scaled_shapes_list=[]
        grid=np.zeros((resolution,resolution,resolution),dtype=int)
        filter_files(self.scaled_shapes_path,".BVH.dat",scaled_shapes_list)
        for i in range(len(scaled_shapes_list)):
            print('Generating grid: '+str(i))
            original_mesh=trimesh.load(scaled_shapes_list[i][:-8])
            scaled_shape_name=scaled_shapes_list[i][:-8]+'.smoothed.obj'
            if os.path.exists(scaled_shape_name+'.grid'):
                continue
            mesh=trimesh.load(scaled_shape_name)
            translation=self.get_center_of_mesh(mesh.vertices)-self.get_center_of_mesh(original_mesh.vertices)
            pitch=grid_length/resolution
            trans_unit=np.ceil(translation/pitch).astype(int)
            max_edge=pitch/2.0
            v_obj,_=trimesh.remesh.subdivide_to_size(mesh.vertices,mesh.faces,max_edge)
            hit_obj=v_obj/pitch
            hit_obj=np.ceil(hit_obj).astype(int)
            u_obj,_=trimesh.grouping.unique_rows(hit_obj)
            if show:
                occupied=hit_obj[u_obj]*pitch
                vox_obj=trimesh.voxel.ops.multibox(occupied,pitch)
                vox_obj.visual.face_colors=[0,255,0,128]
                (mesh+vox_obj).show()
            hit_obj=hit_obj+int(resolution/2-1)-trans_unit
            for j in range(len(u_obj)):
                grid[hit_obj[u_obj[j]][0]][hit_obj[u_obj[j]][1]][hit_obj[u_obj[j]][2]]=1
            torch.save(torch.from_numpy(grid),scaled_shape_name+'.grid')

    def generate_data_for_graspit(self):
        mkdir(self.good_shapes_path)
        scaled_shapes_list=[]
        filter_files(self.scaled_shapes_path,".smoothed.obj",scaled_shapes_list)
        for i in range(len(scaled_shapes_list)):
            off_name=self.good_shapes_path+'/'+re.split("/",scaled_shapes_list[i])[-1][:-4]+'.off'
            if os.path.exists(off_name):
                continue
            mesh=trimesh.load(scaled_shapes_list[i]).apply_scale(100.0)
            mesh.export(off_name,'off')
        good_shapes_list=filter_files(self.good_shapes_path,".off",[])
        for i in range(len(good_shapes_list)):
            off_name=re.split("/",good_shapes_list[i])[-1]
            if os.path.exists(good_shapes_list[i][:-4]+".xml"):
                continue
            #create xml
            root=ET.Element('root')
            material=ET.SubElement(root,'material')
            material.text='plastic'
            mass=ET.SubElement(root,'mass')
            mass.text='100'
            cog=ET.SubElement(root,'cog')
            cog.text='0 0 0'
            geometryFile=ET.SubElement(root,'geometryFile')
            geometryFile.set('type','off')
            geometryFile.text=off_name
            #write xml
            root_str=ET.tostring(root)
            reparsed=minidom.parseString(root_str.decode())
            root_str=reparsed.toprettyxml(indent="\t")
            file=open(good_shapes_list[i][:-4]+".xml","w")
            file.write(root_str)
            file.close()
    
    def remove_repeat_shapes(self,iros_data_path):
        iros_shapes_list=[]
        filter_files(iros_data_path,'off',iros_shapes_list)
        good_off_list=[]
        filter_files(self.good_shapes_path,".off",good_off_list)
        obj_names=[]
        for i in range(len(iros_shapes_list)):
            obj_names.append(re.split('/',iros_shapes_list[i])[-1][:-4])
        for i in range(len(good_off_list)):
            obj_name=re.split("/",good_off_list[i])[-1][:-4]
            if obj_name in obj_names:
                os.remove(good_off_list[i])
                os.remove(good_off_list[i][:-4]+'.xml')
    
    def rotate_hand(self,grasp,hand_R):
        hand_original_coor=grasp[0:3].numpy()
        hand_coor_T_R=np.matmul(hand_R,hand_original_coor)
        hand_original_quat=grasp[3:7].numpy()
        hand_quat2mat=transforms3d.quaternions.quat2mat(hand_original_quat)
        hand_quat2mat_R=np.matmul(hand_R,hand_quat2mat)
        hand_mat2quat=transforms3d.quaternions.mat2quat(hand_quat2mat_R)
        extrinsic=np.concatenate((hand_coor_T_R,hand_mat2quat))
        extrinsic_dof=np.concatenate((extrinsic,grasp[7:25].numpy()))
        grasp=torch.from_numpy(extrinsic_dof)
        return grasp

    def data_aug(self):
        mkdir(self.scaled_shapes_aug_path)
        scaled_shapes_list=filter_files(self.scaled_shapes_path,"_scaled.obj",[])
        for i in range(len(scaled_shapes_list)):
            print('Data aug:'+str(i))
            scaled_shape_name=scaled_shapes_list[i].split('/')[-1][:-11]
            mesh=trimesh.load_mesh(scaled_shapes_list[i])
            grasps=torch.load(scaled_shapes_list[i]+'.smoothed.obj.grasp')
            Zgrasps=torch.load(scaled_shapes_list[i]+'.smoothed.obj.Zgrasp')
            for jj in range(2):
                for kk in range(2):
                    for mm in range(2):
                        # meshes transforms
                        scaled_shape_name_aug=self.scaled_shapes_aug_path+'/'+scaled_shape_name+'_'+str(jj)+'_'+\
                            str(kk)+'_'+str(mm)+'_scaled.obj'
                        if os.path.exists(scaled_shape_name_aug) and\
                           os.path.exists(scaled_shape_name_aug+'.smoothed.obj.grasp') and\
                           os.path.exists(scaled_shape_name_aug+'.smoothed.obj.Zgrasp'):
                            continue
                        mesh_zrot=math.pi/(jj+1)
                        mesh_yrot=math.pi/(kk+1)
                        mesh_xrot=math.pi/(mm+1)
                        z_rotation=transforms3d.axangles.axangle2aff([0,0,1],mesh_zrot,None)
                        y_rotation=transforms3d.axangles.axangle2aff([0,1,0],mesh_yrot,None)
                        x_rotation=transforms3d.axangles.axangle2aff([1,0,0],mesh_xrot,None)
                        mesh_R=np.matmul(z_rotation,y_rotation)
                        mesh_R=np.matmul(mesh_R,x_rotation)
                        mesh.apply_transform(mesh_R)
                        mesh.export(scaled_shape_name_aug,'obj')
                        # grasps transforms
                        hand_R=mesh_R[0:3][:,:3]
                        for j in range(len(grasps)):
                            grasps[j]=self.rotate_hand(grasps[j],hand_R)
                            Zgrasps[j]=self.rotate_hand(Zgrasps[j],hand_R)
                        torch.save(grasps,scaled_shape_name_aug+'.smoothed.obj.grasp')
                        torch.save(Zgrasps,scaled_shape_name_aug+'.smoothed.obj.Zgrasp')
        self.scaled_shapes_path=self.scaled_shapes_aug_path
        self.mesh_process()
        self.render_depths_for_shapes()
        self.generate_depths_for_shapes()
    
    def aug_validate(self):
        scaled_shapes_list=filter_files(self.scaled_shapes_aug_path,".smoothed.obj",[])
        self.hand=Hand("hand/ShadowHand/",0.01,use_joint_limit=True,use_quat=True,use_eigen=False)
        for i in range(len(scaled_shapes_list)):
            grasps=torch.load(scaled_shapes_list[i]+'.Zgrasp')
            mkdir(scaled_shapes_list[i]+'_gt_Zgrasp')
            obj_vtk=trimesh_to_vtk(trimesh.load(scaled_shapes_list[i]))
            write_vtk(obj_vtk,scaled_shapes_list[i]+'.vtk')
            for j in range(len(grasps)):
                self.hand.forward_kinematics(grasps[j].detach().numpy()[0:self.hand.extrinsic_size],\
                            grasps[j].detach().numpy()[self.hand.extrinsic_size:])
                hand_vtk=trimesh_to_vtk(self.hand.draw(1,False))
                write_vtk(hand_vtk,scaled_shapes_list[i]+'_gt_Zgrasp'+'/'+'gt_'+str(j)+'.vtk')
    
    def prepare_test_data(self,data_path):
        shape_list=filter_files(self.shapes_path,".stl",[])
        if len(shape_list)==0:
            filter_files(self.shapes_path,".off",shape_list)
        scaled_shapes_path=data_path+'scaled_obj_meshes_test'
        if os.path.exists(test_data_path):
            return
        mkdir(self.scaled_shapes_path)
        count=0
        for i in range(len(shape_list)):
            if 'bigbird' in shape_list[i] or 'ycb' in shape_list[i]:
                self.scale_shapes(shape_list[i],np.asarray([0,0,0]),0.01,test=True)
                print(count)
                count+=1

        scaled_shapes_list=filter_files(scaled_shapes_path,"_scaled.obj",[])
        test_aug_dir=scaled_shapes_path+'_aug'
        mkdir(test_aug_dir)
        for i in range(len(scaled_shapes_list)):
            print('Data aug:'+str(i))
            scaled_shape_name=scaled_shapes_list[i].split('/')[-1][:-11]
            for jj in range(2):
                for kk in range(2):
                    for mm in range(2):
                        # meshes transforms
                        scaled_shape_name_aug=test_aug_dir+'/'+scaled_shape_name+'_'+str(jj)+'_'+\
                            str(kk)+'_'+str(mm)+'_scaled.obj'
                        if os.path.exists(scaled_shape_name_aug):
                            continue
                        else:
                            mesh=trimesh.load_mesh(scaled_shapes_list[i])
                        mesh_zrot=math.pi/(jj+1)
                        mesh_yrot=math.pi/(kk+1)
                        mesh_xrot=math.pi/(mm+1)
                        z_rotation=transforms3d.axangles.axangle2aff([0,0,1],mesh_zrot,None)
                        y_rotation=transforms3d.axangles.axangle2aff([0,1,0],mesh_yrot,None)
                        x_rotation=transforms3d.axangles.axangle2aff([1,0,0],mesh_xrot,None)
                        mesh_R=np.matmul(z_rotation,y_rotation)
                        mesh_R=np.matmul(mesh_R,x_rotation)
                        mesh.apply_transform(mesh_R)
                        mesh.export(scaled_shape_name_aug,'obj')

        scaled_shapes_list=filter_files(test_aug_dir,"_scaled.obj",[])
        for i in range(len(scaled_shapes_list)):
            scaled_shape_name=scaled_shapes_list[i]
            output_folder=scaled_shape_name+'.depths'
            depth_list=filter_files(output_folder,".jpg",[])
            if len(depth_list)==5:
                continue
            mkdir(output_folder)
            mesh_zrot=0.0
            mesh_yrot=0.0
            mesh_xrot=0.0
            z_rotation=transforms3d.axangles.axangle2aff([0,0,1],mesh_zrot,None)
            y_rotation=transforms3d.axangles.axangle2aff([0,1,0],mesh_yrot,None)
            x_rotation=transforms3d.axangles.axangle2aff([1,0,0],mesh_xrot,None)
            mesh_R=np.matmul(x_rotation,y_rotation)
            mesh_R=np.matmul(mesh_R,z_rotation)
            render_cmd='%s --background --python %s -- --output_folder %s %s --zrot %f --yrot %f --xrot %f' \
                % (self.blender_path,"render_blender.py",output_folder,scaled_shape_name,mesh_zrot,mesh_yrot,mesh_xrot)
            os.system(render_cmd)
            image_list=[]
            filter_files(output_folder,".jpg",image_list)
            for j in range(len(image_list)):
                if "_depth_" not in image_list[j]:
                    os.remove(image_list[j])
            os.remove('Image0001.jpg')

        for i in range(len(scaled_shapes_list)):
            print('Generating depths tensor: '+str(i))
            scaled_shape_name=scaled_shapes_list[i]
            if os.path.exists(scaled_shape_name+'.depth'):
                exsit_depth_tensor=torch.load(scaled_shape_name+'.depth')
                if len(exsit_depth_tensor)<5:
                    print(scaled_shape_name+'.depth')
                else:
                    continue
            depth_folder=scaled_shape_name+'.depths'
            depth_list=[]
            filter_files(depth_folder,".jpg",depth_list)
            if len(depth_list)<5:
                print(depth_folder)
            depth_tensor=torch.zeros(len(depth_list),224,224,3)
            for j in range(len(depth_list)):
                depth_tensor[j]=torch.tensor(io.imread(depth_list[j]))
            depth_tensor=depth_tensor.permute(0,3,1,2)/255.0
            torch.save(depth_tensor,scaled_shape_name+'.depth')


if __name__=='__main__':
    data_path="/home/lm/scratch/ddg_data/grasp_dataset/"
    blender_path="~/Softwares/blender-2.79-linux-glibc219-x86_64/blender"
    data_augmentation=False
    mydata=Data(data_path,blender_path)
    mydata.mesh_preprocess()
    mydata.mesh_process()
    mydata.max_extent()
    mydata.render_depths_for_shapes()
    mydata.generate_depths_for_shapes()
    mydata.generate_grids_for_shapes(False)
    if data_augmentation:
        mydata.data_aug()
        # mydata.aug_validate()
    mydata.prepare_test_data(data_path)

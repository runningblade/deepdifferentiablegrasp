#include "ConvexHull.h"
#include <CGAL/Convex_hull_d.h>
#include <CGAL/Cartesian_d.h>
#include <CGAL/Gmpq.h>

USE_PRJ_NAMESPACE

//ConvexHull<DIM>
template <int DIM>
void ConvexHull<DIM>::insertInit(const PTS& pss)
{
  for(sizeType i=0; i<(sizeType)pss.size(); i++)
    insert(pss[i]);
}
template <int DIM>
typename ConvexHull<DIM>::PT ConvexHull<DIM>::mul(const Eigen::Matrix<scalar,DIM,6>* basis,const Vec6& p)
{
  if(basis)
    return *basis*p;
  else return p.template segment<DIM>(0);
}
template <int DIM>
Vec6 ConvexHull<DIM>::mulT(const Eigen::Matrix<scalar,DIM,6>* basis,const PT& p)
{
  if(basis)
    return basis->transpose()*p;
  else {
    Vec6d ret=Vec6d::Zero();
    ret.template segment<DIM>(0)=p;
    return ret;
  }
}

//CGALConvexHull<DIM>
template <int DIM>
CGALConvexHull<DIM>::CGALConvexHull()
{
  typedef CGAL::Gmpq NumberType;
  typedef CGAL::Convex_hull_d<CGAL::Cartesian_d<NumberType>> CGAL_QHULL;
  _hull=new CGAL_QHULL(DIM);
}
template <int DIM>
CGALConvexHull<DIM>::~CGALConvexHull<DIM>()
{
  typedef CGAL::Gmpq NumberType;
  typedef CGAL::Convex_hull_d<CGAL::Cartesian_d<NumberType>> CGAL_QHULL;
  delete reinterpret_cast<CGAL_QHULL*>(_hull);
}
template <int DIM>
typename CGALConvexHull<DIM>::PLS CGALConvexHull<DIM>::getPLS() const
{
  typedef CGAL::Gmpq NumberType;
  typedef CGAL::Convex_hull_d<CGAL::Cartesian_d<NumberType>> CGAL_QHULL;
  CGAL_QHULL* hull=reinterpret_cast<CGAL_QHULL*>(_hull);

  PLS ret;
  //count number of planes
  sizeType nrP=0;
  for(CGAL::Convex_hull_d<CGAL::Cartesian_d<NumberType>>::Facet_iterator beg=hull->facets_begin(),end=hull->facets_end(); beg!=end; beg++)
    nrP++;
  ret.resize(DIM+1,nrP);

  //fill planes
  nrP=0;
  for(CGAL::Convex_hull_d<CGAL::Cartesian_d<NumberType>>::Facet_iterator beg=hull->facets_begin(),end=hull->facets_end(); beg!=end; beg++) {
    CGAL::Cartesian_d<NumberType>::Hyperplane_d p=hull->hyperplane_supporting(beg);
    for(sizeType i=0; i<=DIM; i++)
      ret(i,nrP)=CGAL::to_double(p[i]);
    ret.col(nrP)/=ret.col(nrP).template segment<DIM>(0).norm();
    nrP++;
  }
  return ret;
}
template <int DIM>
ObjMesh CGALConvexHull<DIM>::getMesh() const
{
  typedef CGAL::Gmpq NumberType;
  typedef CGAL::Convex_hull_d<CGAL::Cartesian_d<NumberType>> CGAL_QHULL;
  CGAL_QHULL* hull=reinterpret_cast<CGAL_QHULL*>(_hull);

  ObjMesh ret;
  sizeType id=0;
  std::map<CGAL_QHULL::Vertex_handle,sizeType> vssMap;
  for(CGAL_QHULL::Vertex_iterator beg=hull->vertices_begin(),end=hull->vertices_end(); beg!=end; beg++) {
    vssMap[beg]=id++;
    const CGAL_QHULL::Point_d& pt=beg->point();
    ret.getV().push_back(Vec3(CGAL::to_double(pt[0]),CGAL::to_double(pt[1]),CGAL::to_double(pt[2])));
  }
  for(CGAL_QHULL::Facet_iterator beg=hull->facets_begin(),end=hull->facets_end(); beg!=end; beg++) {
    const typename CGAL_QHULL::Vertex_handle v0=hull->vertex_of_facet(beg,0);
    const typename CGAL_QHULL::Vertex_handle v1=hull->vertex_of_facet(beg,1);
    const typename CGAL_QHULL::Vertex_handle v2=hull->vertex_of_facet(beg,2);
    ret.getI().push_back(Vec3i(vssMap[v0],vssMap[v1],vssMap[v2]));
  }
  ret.smooth();
  return ret;
}
template <int DIM>
void CGALConvexHull<DIM>::insert(const PT& p)
{
  typedef CGAL::Gmpq NumberType;
  typedef CGAL::Convex_hull_d<CGAL::Cartesian_d<NumberType>> CGAL_QHULL;
  CGAL_QHULL* hull=reinterpret_cast<CGAL_QHULL*>(_hull);

  Eigen::Matrix<NumberType,DIM,1> pNum=p.template cast<double>().template cast<NumberType>();
  hull->insert(CGAL::Cartesian_d<NumberType>::Point_d(DIM,pNum.data(),pNum.data()+DIM));
}
template <int DIM>
scalar CGALConvexHull<DIM>::distToOrigin(PT& blockingPN)
{
  typedef CGAL::Gmpq NumberType;
  typedef CGAL::Convex_hull_d<CGAL::Cartesian_d<NumberType>> CGAL_QHULL;
  CGAL_QHULL* hull=reinterpret_cast<CGAL_QHULL*>(_hull);

  PT n;
  scalarD dist;
  scalarD Q=ScalarUtil<scalarD>::scalar_max();
  for(CGAL::Convex_hull_d<CGAL::Cartesian_d<NumberType>>::Facet_iterator beg=hull->facets_begin(),end=hull->facets_end(); beg!=end; beg++) {
    CGAL::Cartesian_d<NumberType>::Hyperplane_d p=hull->hyperplane_supporting(beg);
    if(p.dimension()!=DIM)
      continue;
    for(sizeType d=0; d<DIM; d++)
      n[d]=CGAL::to_double(p[d]);
    dist=-CGAL::to_double(p[DIM])/n.norm();
    if(dist<Q) {
      Q=dist;
      blockingPN=n;
    }
  }
  return Q;
}

//instance
#define DECL_HULL(DIM)  \
template class CGALConvexHull<DIM>;
DECL_HULL(3)

#ifndef SELF_COLLISION_H
#define SELF_COLLISION_H

#include "CommonFile/IOBasic.h"
#include "CommonFile/geom/BVHNode.h"
#include <boost/unordered_set.hpp>
#include "SDPDerivative/Utils.h"
#include "ConvexHull.h"

PRJ_BEGIN

struct ObjectSample : public SerializableBase
{
  typedef std::vector<Node<sizeType>> BVHS;
  ObjectSample();
  ObjectSample(ObjMesh mesh,scalar r);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  void writeVTK(const std::string& path) const;
  ObjMesh _mesh;
  Mat3X _pss;
  BVHS _bvh;
};
class SelfCollision : public SerializableBase
{
public:
  typedef CGALConvexHull<3>::PLS PLS;
  typedef Eigen::Matrix<scalar,-1,1> Vec;
  typedef std::vector<Node<sizeType>> BVHS;
  typedef std::vector<Mat3X4,Eigen::aligned_allocator<Mat3X4>> TSS;
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  void writeVTK(const Vec* tss,const std::string& path) const;
  void writeVTK(const TSS* tss,const std::string& path) const;
  //build
  void print() const;
  sizeType nrLink() const;
  void addLink(const ObjMesh& mesh,scalar r);
  sizeType addLink(const Mat3X& pss,scalar r);
  void prune(sizeType i,sizeType j);
  void assemble();
  //detect
  void detectVTK(const ObjectSample* obj,const Vec& tss,const std::string& path) const;
  void detectVTK(const ObjectSample* obj,const TSS& tss,const std::string& path) const;
  scalar detect(const ObjectSample* obj,const Vec& tss,Vec& dtss,
                std::function<void(const Vec3&)> f,bool deepest) const;
  scalar detect(const ObjectSample* obj,const TSS& tss,TSS& dtss,
                std::function<void(const Vec3&)> f,bool deepest) const;
  scalar detect(const ObjectSample* obj,const BVHS& bvhH,const std::vector<BVHS>& bvhP,const TSS& tss,TSS& dtss,
                std::function<void(const Vec3&)> f,bool deepest) const;
  void debugGradient(const ObjectSample* obj,bool deepest,sizeType nrIter=10) const;
  //detectObject
  scalar detect(const ObjectSample& obj,const BVHS& bvhH,
                const TSS& tss,TSS& dtss,
                std::function<void(const Vec3&)> f,bool deepest) const;
private:
  void transform(const TSS& tss,BVHS& bvhH,std::vector<BVHS>& bvhP) const;
  scalar narrowphase(const Node<sizeType>& nodeH,const BVHS& bvhP,
                     const Mat3X4& tssH,const Mat3X4& tssP,
                     Mat3X4& dtssH,Mat3X4& dtssP,
                     std::function<void(const Vec3&)> f,bool deepest) const;
  scalar narrowphase(const Node<sizeType>& nodeH,const ObjectSample& bvhP,
                     const Mat3X4& tssH,Mat3X4& dtssH,
                     std::function<void(const Vec3&)> f,bool deepest) const;
  //data
  PLS _hss;
  Mat3X _pss;
  BVHS _bvhH;
  std::vector<BVHS> _bvhP;
  std::vector<ObjMesh> _mesh;
  std::vector<sizeType> _offHss,_offPss;
  boost::unordered_set<Vec2i,Hash> _prune;
};

PRJ_END

#endif

#ifndef CONVEX_HULL_H
#define CONVEX_HULL_H

#include "CommonFile/ObjMesh.h"

PRJ_BEGIN

template <int DIM>
struct ConvexHull
{
public:
  typedef Eigen::Matrix<scalar,DIM,1> PT;
  typedef std::vector<PT,Eigen::aligned_allocator<PT>> PTS;
  typedef Eigen::Matrix<scalar,DIM+1,1> PL;
  typedef Eigen::Matrix<scalar,DIM+1,-1> PLS;
  virtual ~ConvexHull() {}
  virtual PLS getPLS() const=0;
  virtual ObjMesh getMesh() const=0;
  virtual void insert(const PT& p)=0;
  virtual void insertInit(const PTS& pss);
  virtual scalar distToOrigin(PT& blockingPN)=0;
  static PT mul(const Eigen::Matrix<scalar,DIM,6>* basis,const Vec6& p);
  static Vec6 mulT(const Eigen::Matrix<scalar,DIM,6>* basis,const PT& p);
};
template <int DIM>
struct CGALConvexHull : public ConvexHull<DIM>
{
public:
  using typename ConvexHull<DIM>::PT;
  using typename ConvexHull<DIM>::PTS;
  using typename ConvexHull<DIM>::PL;
  using typename ConvexHull<DIM>::PLS;
  CGALConvexHull();
  virtual ~CGALConvexHull();
  virtual PLS getPLS() const override;
  virtual ObjMesh getMesh() const override;
  virtual void insert(const PT& p) override;
  virtual scalar distToOrigin(PT& blockingPN) override;
private:
  void* _hull;
};

PRJ_END

#endif

#include "SelfCollision.h"
#include "CommonFile/CameraModel.h"
#include "CommonFile/geom/BVHBuilder.h"
#include "CommonFile/ParallelPoissonDiskSampling.h"
#include "SDPDerivative/DebugGradient.h"

USE_PRJ_NAMESPACE

//ObjectSample
ObjectSample::ObjectSample() {}
ObjectSample::ObjectSample(ObjMesh mesh,scalar r):_mesh(mesh)
{
  ParallelPoissonDiskSampling sample(3);
  sample.getRadius()=r;
  sample.sample(mesh);
  //insert bvh
  Node<sizeType> n;
  n._nrCell=1;
  _pss=Mat3X::Zero(3,sample.getPSet().size());
  for(sizeType i=0; i<sample.getPSet().size(); i++) {
    n._bb._minC=n._bb._maxC=sample.getPSet()[i]._pos;
    _pss.col(i)=sample.getPSet()[i]._pos;
    n._cell=i;
    _bvh.push_back(n);
  }
  //build bvhH
  BVHBuilder<Node<sizeType>,3> builder;
  builder.buildBVH(_bvh);
  for(sizeType j=(_bvh.size()+1)/2; j<(sizeType)_bvh.size(); j++)
    _bvh[j]._cell=-1;
}
bool ObjectSample::read(std::istream& is,IOData* dat)
{
  _mesh.readBinary(is);
  readBinaryData(_pss,is,dat);
  readBinaryData(_bvh,is,dat);
  return is.good();
}
bool ObjectSample::write(std::ostream& os,IOData* dat) const
{
  _mesh.writeBinary(os);
  writeBinaryData(_pss,os,dat);
  writeBinaryData(_bvh,os,dat);
  return os.good();
}
boost::shared_ptr<SerializableBase> ObjectSample::copy() const
{
  return boost::shared_ptr<SerializableBase>(new ObjectSample);
}
std::string ObjectSample::type() const
{
  return typeid(ObjectSample).name();
}
void ObjectSample::writeVTK(const std::string& path) const
{
  VTKWriter<scalar> os("ObjectSample",path,true);
  _mesh.writeVTK(os);
  std::vector<Vec3,Eigen::aligned_allocator<Vec3>> vss;
  for(sizeType i=0; i<_pss.cols(); i++)
    vss.push_back(_pss.col(i));
  os.setRelativeIndex();
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(VTKWriter<scalar>::IteratorIndex<Vec3i>(0,0,0),
                 VTKWriter<scalar>::IteratorIndex<Vec3i>((sizeType)vss.size(),0,0),
                 VTKWriter<scalar>::POINT,true);
}
//SelfCollision
bool SelfCollision::read(std::istream& is,IOData* dat)
{
  readBinaryData(_hss,is,dat);
  readBinaryData(_pss,is,dat);
  readBinaryData(_bvhH,is,dat);
  readBinaryData(_bvhP,is,dat);
  readBinaryData(_offHss,is,dat);
  readBinaryData(_offPss,is,dat);
  readBinaryData(_prune,is,dat);
  sizeType n;
  readBinaryData(n,is,dat);
  _mesh.resize(n);
  for(sizeType i=0; i<n; i++)
    _mesh[i].readBinary(is);
  return is.good();
}
bool SelfCollision::write(std::ostream& os,IOData* dat) const
{
  writeBinaryData(_hss,os,dat);
  writeBinaryData(_pss,os,dat);
  writeBinaryData(_bvhH,os,dat);
  writeBinaryData(_bvhP,os,dat);
  writeBinaryData(_offHss,os,dat);
  writeBinaryData(_offPss,os,dat);
  writeBinaryData(_prune,os,dat);
  writeBinaryData((sizeType)_mesh.size(),os,dat);
  for(sizeType i=0; i<(sizeType)_mesh.size(); i++)
    _mesh[i].writeBinary(os);
  return os.good();
}
boost::shared_ptr<SerializableBase> SelfCollision::copy() const
{
  return boost::shared_ptr<SerializableBase>(new SelfCollision);
}
std::string SelfCollision::type() const
{
  return typeid(SelfCollision).name();
}
void SelfCollision::writeVTK(const Vec* tss,const std::string& path) const
{
  if(!tss)
    writeVTK((const TSS*)NULL,path);
  else {
    Eigen::Map<const Eigen::Matrix<scalar,3,-1,Eigen::RowMajor>> tssM(tss->data(),3,nrLink()*4);
    TSS tssV(_mesh.size());
    for(sizeType i=0; i<(sizeType)_mesh.size(); i++)
      tssV[i]=tssM.block<3,4>(0,i*4);
    writeVTK(&tssV,path);
  }
}
void SelfCollision::writeVTK(const TSS* tss,const std::string& path) const
{
  recreate(path);
  ObjMesh meshAll;
  std::vector<Vec3,Eigen::aligned_allocator<Vec3>> vss;
  for(sizeType i=0; i<(sizeType)_mesh.size(); i++) {
    //hss
    ObjMesh mesh=_mesh[i];
    if(tss) {
      mesh.getPos()=tss->at(i).block<3,1>(0,3);
      mesh.getT()=tss->at(i).block<3,3>(0,0);
      mesh.applyTrans(Vec3::Zero());
    }
    meshAll.addMesh(mesh);
    //pss
    for(sizeType j=_offPss[i]; j<_offPss[i+1]; j++)
      if(tss)
        vss.push_back(transformHomo<scalar>(tss->at(i),_pss.col(j)));
      else vss.push_back(_pss.col(j));
  }
  meshAll.writeVTK(path+"/shape.vtk",true);
  VTKWriter<scalar> os("points",path+"/sample.vtk",true);
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(VTKWriter<scalar>::IteratorIndex<Vec3i>(0,0,0),
                 VTKWriter<scalar>::IteratorIndex<Vec3i>((sizeType)vss.size(),0,0),
                 VTKWriter<scalar>::POINT);
}
//build
void SelfCollision::print() const
{
  INFO("Links:")
  for(sizeType i=0; i<(sizeType)_offHss.size()-1; i++) {
    INFOV("Link%ld has %ld faces and %ld samples!",i,_offHss[i+1]-_offHss[i],_offPss[i+1]-_offPss[i])
  }
  INFO("Prunes:")
  for(boost::unordered_set<Vec2i,Hash>::const_iterator beg=_prune.begin(),end=_prune.end(); beg!=end; beg++) {
    INFOV("LinkPair: %ld %ld",(*beg)[0],(*beg)[1])
  }
}
sizeType SelfCollision::nrLink() const
{
  sizeType nrLink=(sizeType)_offHss.size()-1;
  return nrLink;
}
void SelfCollision::addLink(const ObjMesh& mesh,scalar r)
{
  Mat3X pss=Mat3X::Zero(3,(sizeType)mesh.getV().size());
  for(sizeType i=0; i<(sizeType)mesh.getV().size(); i++)
    pss.col(i)=mesh.getV()[i];
  addLink(pss,r);
}
sizeType SelfCollision::addLink(const Mat3X& pss,scalar r)
{
  //construct convex hull
  CGALConvexHull<3> hsol;
  CGALConvexHull<3>::PTS pssv;
  for(sizeType i=0; i<pss.cols(); i++)
    pssv.push_back(pss.col(i));
  hsol.insertInit(pssv);
  //insert bvh
  Node<sizeType> n;
  for(sizeType i=0; i<pss.cols(); i++)
    n._bb.setUnion(pss.col(i));
  n._cell=(sizeType)_offHss.size();
  n._nrCell=1;
  _bvhH.push_back(n);
  //insert
  _hss=concatCol<PLS>(_hss,hsol.getPLS());
  _offHss.push_back(hsol.getPLS().cols());

  //construct point samples
  ObjMesh mesh=hsol.getMesh();
  ParallelPoissonDiskSampling sample(3);
  sample.getRadius()=r;
  sample.sample(mesh);
  //insert bvh
  BVHS bvhP;
  Mat3X sss=Mat3X::Zero(3,sample.getPSet().size());
  for(sizeType i=0; i<sample.getPSet().size(); i++) {
    n._bb._minC=n._bb._maxC=sample.getPSet()[i]._pos;
    sss.col(i)=sample.getPSet()[i]._pos;
    n._cell=_pss.cols()+i;
    bvhP.push_back(n);
  }
  _bvhP.push_back(bvhP);
  //insert
  _pss=concatCol<Mat3X>(_pss,sss);
  _offPss.push_back(sss.cols());
  _mesh.push_back(mesh);
  return (sizeType)_offHss.size()-1;
}
void SelfCollision::prune(sizeType i,sizeType j)
{
  _prune.insert(Vec2i(i,j));
  _prune.insert(Vec2i(j,i));
}
void SelfCollision::assemble()
{
  sizeType nrLink=(sizeType)_offHss.size();
  for(sizeType i=1; i<nrLink; i++) {
    _offHss[i]+=_offHss[i-1];
    _offPss[i]+=_offPss[i-1];
  }
  _offHss.insert(_offHss.begin(),0);
  _offPss.insert(_offPss.begin(),0);
  //build bvhP
  for(sizeType i=0; i<nrLink; i++) {
    BVHBuilder<Node<sizeType>,3> builder;
    builder.buildBVH(_bvhP[i]);
    for(sizeType j=(_bvhP[i].size()+1)/2; j<(sizeType)_bvhP[i].size(); j++)
      _bvhP[i][j]._cell=-1;
  }
  //build bvhH
  BVHBuilder<Node<sizeType>,3> builder;
  builder.buildBVH(_bvhH);
  for(sizeType j=(_bvhH.size()+1)/2; j<(sizeType)_bvhH.size(); j++)
    _bvhH[j]._cell=-1;
}
//detect
void SelfCollision::detectVTK(const ObjectSample* obj,const Vec& tss,const std::string& path) const
{
  Eigen::Map<const Eigen::Matrix<scalar,3,-1,Eigen::RowMajor>> tssM(tss.data(),3,nrLink()*4);
  TSS tssV(_mesh.size());
  for(sizeType i=0; i<(sizeType)_mesh.size(); i++)
    tssV[i]=tssM.block<3,4>(0,i*4);
  detectVTK(obj,tssV,path);
}
void SelfCollision::detectVTK(const ObjectSample* obj,const TSS& tss,const std::string& path) const
{
  TSS dtss;
  std::vector<Vec3,Eigen::aligned_allocator<Vec3>> vss;
  detect(obj,tss,dtss,[&](const Vec3& pt) {
    vss.push_back(pt);
  },false);
  VTKWriter<scalar> os("detect",path+"/detect.vtk",true);
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(VTKWriter<scalar>::IteratorIndex<Vec3i>(0,0,0),
                 VTKWriter<scalar>::IteratorIndex<Vec3i>((sizeType)vss.size(),0,0),
                 VTKWriter<scalar>::POINT);
}
scalar SelfCollision::detect(const ObjectSample* obj,const Vec& tss,Vec& dtss,
                             std::function<void(const Vec3&)> f,bool deepest) const
{
  Eigen::Map<const Eigen::Matrix<scalar,3,-1,Eigen::RowMajor>> tssM(tss.data(),3,nrLink()*4);
  Eigen::Map<Eigen::Matrix<scalar,3,-1,Eigen::RowMajor>> dtssM(dtss.data(),3,nrLink()*4);
  TSS tssV(_mesh.size());
  TSS dtssV(_mesh.size());
  for(sizeType i=0; i<(sizeType)_mesh.size(); i++)
    tssV[i]=tssM.block<3,4>(0,i*4);
  scalar ret=detect(obj,tssV,dtssV,f,deepest);
  for(sizeType i=0; i<(sizeType)_mesh.size(); i++)
    dtssM.block<3,4>(0,i*4)=dtssV[i];
  return ret;
}
scalar SelfCollision::detect(const ObjectSample* obj,const TSS& tss,TSS& dtss,
                             std::function<void(const Vec3&)> f,bool deepest) const
{
  BVHS bvhH;
  std::vector<BVHS> bvhP;
  transform(tss,bvhH,bvhP);
  return detect(obj,bvhH,bvhP,tss,dtss,f,deepest);
}
scalar SelfCollision::detect(const ObjectSample* obj,const BVHS& bvhH,const std::vector<BVHS>& bvhP,const TSS& tss,TSS& dtss,
                             std::function<void(const Vec3&)> f,bool deepest) const
{
  scalar ret=0;
  dtss.assign(tss.size(),Mat3X4::Zero());
  std::stack<std::pair<sizeType,sizeType>> ss;
  ss.push(std::make_pair((sizeType)bvhH.size()-1,(sizeType)bvhH.size()-1));
  while(!ss.empty()) {
    sizeType aid=ss.top().first;
    sizeType bid=ss.top().second;
    const Node<sizeType>& a=bvhH[aid];
    const Node<sizeType>& b=bvhH[bid];
    ss.pop();
    if(!a._bb.intersect(b._bb))
      continue;
    else if(a._cell>=0 && b._cell>=0) {
      if(a._cell!=b._cell) {
        if(_prune.find(Vec2i(a._cell,b._cell))!=_prune.end())
          continue;
        ret+=narrowphase(a,bvhP[b._cell],tss[aid],tss[bid],dtss[aid],dtss[bid],f,deepest);
        ret+=narrowphase(b,bvhP[a._cell],tss[bid],tss[aid],dtss[bid],dtss[aid],f,deepest);
      }
    } else if(a._cell>=0) {
      ss.push(std::make_pair(aid,b._l));
      ss.push(std::make_pair(aid,b._r));
    } else if(b._cell>=0) {
      ss.push(std::make_pair(a._l,bid));
      ss.push(std::make_pair(a._r,bid));
    } else {
      ss.push(std::make_pair(a._l,b._l));
      ss.push(std::make_pair(a._r,b._l));
      ss.push(std::make_pair(a._l,b._r));
      ss.push(std::make_pair(a._r,b._r));
    }
  }
  if(obj)
    ret+=detect(*obj,bvhH,tss,dtss,f,deepest);
  return ret;
}
void SelfCollision::debugGradient(const ObjectSample* obj,bool deepest,sizeType nrIter) const
{
  DEFINE_NUMERIC_DELTA_T(scalar)
  for(sizeType i=0; i<nrIter; i++) {
    //F
    Vec tss=Vec::Random((sizeType)_mesh.size()*12);
    Vec dtss=Vec::Random((sizeType)_mesh.size()*12);
    scalar F=detect(obj,tss,dtss,[&](const Vec3&) {},deepest);
    //F2
    Vec Dtss=Vec::Random((sizeType)_mesh.size()*12);
    Vec dtss2=Vec::Random((sizeType)_mesh.size()*12);
    scalar F2=detect(obj,tss+Dtss*DELTA,dtss2,[&](const Vec3&) {},deepest);
    DEBUG_GRADIENT("dtss",dtss.dot(Dtss),dtss.dot(Dtss)-(F2-F)/DELTA)
  }
}
//detectObject
scalar SelfCollision::detect(const ObjectSample& obj,const BVHS& bvhH,
                             const TSS& tss,TSS& dtss,
                             std::function<void(const Vec3&)> f,bool deepest) const
{
  scalar ret=0;
  std::stack<sizeType> ss;
  ss.push((sizeType)bvhH.size()-1);
  while(!ss.empty()) {
    sizeType id=ss.top();
    const Node<sizeType>& n=bvhH[id];
    ss.pop();
    if(!n._bb.intersect(obj._bvh.back()._bb))
      continue;
    else if(n._cell>=0)
      ret+=narrowphase(n,obj,tss[id],dtss[id],f,deepest);
    else {
      ss.push(n._l);
      ss.push(n._r);
    }
  }
  return ret;
}
//helper
void SelfCollision::transform(const TSS& tss,BVHS& bvhH,std::vector<BVHS>& bvhP) const
{
  bvhH=_bvhH;
  bvhP=_bvhP;
  sizeType nrLink=(sizeType)_offHss.size()-1;
  for(sizeType i=0; i<nrLink; i++) {
    //bvhP
    for(sizeType f=_offPss[i],fi=0; f<_offPss[i+1]; f++,fi++)
      bvhP[i][fi]._bb._minC=bvhP[i][fi]._bb._maxC=transformHomo<scalar>(tss[i],_pss.col(f));
    BVHQuery<sizeType> query(bvhP[i],3,-1);
    query.updateBVH();
    //bvhH
    Vec3 pt;
    BBox<scalar> ret;
    for(sizeType x=0; x<2; x++)
      for(sizeType y=0; y<2; y++)
        for(sizeType z=0; z<2; z++) {
          pt[0]=(x==0) ? _bvhH[i]._bb._minC[0] : _bvhH[i]._bb._maxC[0];
          pt[1]=(y==0) ? _bvhH[i]._bb._minC[1] : _bvhH[i]._bb._maxC[1];
          pt[2]=(z==0) ? _bvhH[i]._bb._minC[2] : _bvhH[i]._bb._maxC[2];
          ret.setUnion(transformHomo<scalar>(tss[i],pt));
        }
    bvhH[i]._bb=ret;
  }
  BVHQuery<sizeType> query(bvhH,3,-1);
  query.updateBVH();
}
scalar SelfCollision::narrowphase(const Node<sizeType>& nodeH,const BVHS& bvhP,
                                  const Mat3X4& tssH,const Mat3X4& tssP,
                                  Mat3X4& dtssH,Mat3X4& dtssP,
                                  std::function<void(const Vec3&)> f,bool deepest) const
{
  scalar ret=0;
  std::stack<sizeType> ss;
  sizeType maxIdGlobal=-1,pidGlobal=-1;
  scalar maxDistGlobal=ScalarUtil<scalar>::scalar_max();
  ss.push((sizeType)bvhP.size()-1);
  while(!ss.empty()) {
    sizeType nid=ss.top();
    const Node<sizeType>& n=bvhP[nid];
    ss.pop();
    if(!n._bb.intersect(nodeH._bb))
      continue;
    else if(n._cell>=0) {
      sizeType pid=n._cell,maxId=-1;
      scalar maxDist=-ScalarUtil<scalar>::scalar_max();
      Vec3 pTransP=transformHomo<scalar>(tssP,_pss.col(pid));
      Vec3 pTrans=transformHomoInvT<scalar>(tssH,pTransP);
      for(sizeType h=_offHss[nodeH._cell]; h<_offHss[nodeH._cell+1]; h++) {
        scalar dist=_hss.col(h).segment<3>(0).dot(pTrans)+_hss(3,h);
        if(dist>maxDist) {
          maxDist=dist;
          maxId=h;
        }
      }
      if(maxDist<0 && !deepest) {
        Vec3 RHdH=tssH.block<3,3>(0,0)*_hss.col(maxId).segment<3>(0);
        dtssH.block<3,3>(0,0)+=(pTransP-tssH.block<3,1>(0,3))*_hss.col(maxId).segment<3>(0).transpose();
        dtssH.block<3,1>(0,3)-=RHdH;
        dtssP.block<3,3>(0,0)+=RHdH*_pss.col(pid).transpose();
        dtssP.block<3,1>(0,3)+=RHdH;
        ret+=maxDist;
        f(pTransP);
      }
      if(maxDist<maxDistGlobal) {
        pidGlobal=pid;
        maxIdGlobal=maxId;
        maxDistGlobal=maxDist;
      }
    } else {
      ss.push(n._l);
      ss.push(n._r);
    }
  }
  if(maxDistGlobal<0 && deepest) {
    Vec3 pTransP=transformHomo<scalar>(tssP,_pss.col(pidGlobal));
    Vec3 RHdH=tssH.block<3,3>(0,0)*_hss.col(maxIdGlobal).segment<3>(0);
    dtssH.block<3,3>(0,0)+=(pTransP-tssH.block<3,1>(0,3))*_hss.col(maxIdGlobal).segment<3>(0).transpose();
    dtssH.block<3,1>(0,3)-=RHdH;
    dtssP.block<3,3>(0,0)+=RHdH*_pss.col(pidGlobal).transpose();
    dtssP.block<3,1>(0,3)+=RHdH;
    ret+=maxDistGlobal;
    f(pTransP);
  }
  return ret;
}
scalar SelfCollision::narrowphase(const Node<sizeType>& nodeH,const ObjectSample& bvhP,
                                  const Mat3X4& tssH,Mat3X4& dtssH,
                                  std::function<void(const Vec3&)> f,bool deepest) const
{
  scalar ret=0;
  std::stack<sizeType> ss;
  sizeType maxIdGlobal=-1,pidGlobal=-1;
  scalar maxDistGlobal=ScalarUtil<scalar>::scalar_max();
  ss.push((sizeType)bvhP._bvh.size()-1);
  while(!ss.empty()) {
    sizeType nid=ss.top();
    const Node<sizeType>& n=bvhP._bvh[nid];
    ss.pop();
    if(!n._bb.intersect(nodeH._bb))
      continue;
    else if(n._cell>=0) {
      sizeType pid=n._cell,maxId=-1;
      scalar maxDist=-ScalarUtil<scalar>::scalar_max();
      Vec3 pTrans=transformHomoInvT<scalar>(tssH,bvhP._pss.col(pid));
      for(sizeType h=_offHss[nodeH._cell]; h<_offHss[nodeH._cell+1]; h++) {
        scalar dist=_hss.col(h).segment<3>(0).dot(pTrans)+_hss(3,h);
        if(dist>maxDist) {
          maxDist=dist;
          maxId=h;
        }
      }
      if(maxDist<0 && !deepest) {
        Vec3 RHdH=tssH.block<3,3>(0,0)*_hss.col(maxId).segment<3>(0);
        dtssH.block<3,3>(0,0)+=(bvhP._pss.col(pid)-tssH.block<3,1>(0,3))*_hss.col(maxId).segment<3>(0).transpose();
        dtssH.block<3,1>(0,3)-=RHdH;
        ret+=maxDist;
        f(bvhP._pss.col(pid));
      }
      if(maxDist<maxDistGlobal) {
        pidGlobal=pid;
        maxIdGlobal=maxId;
        maxDistGlobal=maxDist;
      }
    } else {
      ss.push(n._l);
      ss.push(n._r);
    }
  }
  if(maxDistGlobal<0 && deepest) {
    Vec3 RHdH=tssH.block<3,3>(0,0)*_hss.col(maxIdGlobal).segment<3>(0);
    dtssH.block<3,3>(0,0)+=(bvhP._pss.col(pidGlobal)-tssH.block<3,1>(0,3))*_hss.col(maxIdGlobal).segment<3>(0).transpose();
    dtssH.block<3,1>(0,3)-=RHdH;
    ret+=maxDistGlobal;
    f(bvhP._pss.col(pidGlobal));
  }
  return ret;
}

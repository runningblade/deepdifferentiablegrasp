import graspPlanner
import torch
if __name__=='__main__':
    # torch.cuda.set_device(0)
    parser=graspPlanner.get_parser()
    args=parser.parse_args()
    args.mode='nn'
    args.hand='ShadowHand'
    args.distance_fn='exact'
    args.optimizer='adam'
    args.Q1Type='Q1Upper'
    args.joint_limit=True
    args.collision=2
    args.dir_res=4
    args.num_views=5
    args.mu=0.7
    args.cnn_name='resnet50'
    args.input_dir='/home/lm/scratch/ddg_data/grasp_dataset/scaled_obj_meshes'
    args.use_pretrained=True
    args.use_quat=True
    args.input_type='depth'
    args.use_good_gt=True
    args.ex_name='nn_depth_all_meshes_pretrain_aug_goodgt'
    args.bs=8
    args.lr=1e-4
    args.alpha=3.0
    args.M=1e-3
    args.beta=0
    args.resume=3
    args.only_train=False
    args.output_grasps=False
    args.use_gpu=False
    myPlanner=graspPlanner.GraspPlanner(args)
    myPlanner.plan()
#include "CommonFile/GridOp.h"
#include "CommonFile/ObjMesh.h"
#include "CommonFile/ImplicitFunc.h"
#include "DistanceExact/ImplicitFuncMeshExact.h"
#include "DistanceExact/SelfCollision.h"
#include "SDPDerivative/Utils.h"
#include <boost/filesystem/fstream.hpp>
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

int main(int argc,char** argv)
{
  if(argc < 4) {
    INFO("Usage: mainObjMeshToSDF [pathObj] [cellSz] [resMax] [margin]")
    return -1;
  } else {
    //compute
    ObjMesh mesh;
    scalar cellSz=boost::lexical_cast<scalar>(argv[2]);
    sizeType resMax=boost::lexical_cast<sizeType>(argv[3]);
    sizeType MARGIN=10;
    if(argc > 4)
      MARGIN=std::max(MARGIN,boost::lexical_cast<sizeType>(argv[4]));
    boost::filesystem::ifstream is(argv[1]);
    mesh.read(is,false,false);
    mesh.smooth();
    mesh.makeUniform();
    mesh.smooth();
    if(mesh.getVolume()<0) {
      mesh.insideOut();
      mesh.smooth();
    }
    mesh.getPos()=-mesh.getCentroid();
    mesh.applyTrans(Vec3::Zero());
    mesh.write(std::string(argv[1])+".smoothed.obj");
    //mesh.writeVTK(std::string(argv[1])+".smoothed.vtk",true,true);

    //create grid
    Vec3i nrC;
    BBox<scalar> bb=mesh.getBB();
    bb._minC-=Vec3::Constant(cellSz*MARGIN);
    if(cellSz>0) {
      Vec3 nrCF=bb.getExtent()/cellSz;
      nrC=ceilV<Vec3>(nrCF)+Vec3i::Constant(MARGIN*2);
      bb._maxC=bb._minC+nrC.cast<scalar>()*cellSz;
      ASSERT_MSGV(nrC.maxCoeff()<resMax,"Resolution too high: (resMax=%d) (res=%d,%d,%d)",resMax,nrC[0],nrC[1],nrC[2])
      INFOV("MARGIN=%d res=%d,%d,%d",MARGIN,nrC[0],nrC[1],nrC[2])
    } else {
      INFO("Not generating grid, just creating BVH!")
    }

    //create BVH
    ObjMeshGeomCellExact cellExact;
    if(!exists(std::string(argv[1])+".BVH.dat")) {
      ObjMeshGeomCell cell(Mat4::Identity(),mesh,bb.getExtent().norm(),true);
      cellExact=ObjMeshGeomCellExact(cell);
      cellExact.SerializableBase::write(std::string(argv[1])+".BVH.dat");
    } else cellExact.SerializableBase::read(std::string(argv[1])+".BVH.dat");

    //create sampled PSet
    if(std::abs(cellSz)>0) {
      ObjectSample sample(mesh,std::abs(cellSz));
      sample.SerializableBase::write(std::string(argv[1])+".PSet.dat");
      INFO("Generating Poisson samples!")
    }

    //fill grid
    if(cellSz>0) {
      ScalarField toBeSampled;
      toBeSampled.reset(nrC,bb,0);
      ImplicitFuncMeshExact cellExactFunc(cellExact);
      ImplicitFuncReinit reinit(toBeSampled,cellExactFunc);
      GridOp<scalar,scalar>::write3DScalarGridVTK(std::string(argv[1])+".SDF.vtk",reinit._ls);
      boost::filesystem::ofstream os(std::string(argv[1])+".SDF.dat");
      reinit._ls.write(os,NULL);

      //determine whether the level set is robust or not
      scalar minV,maxV;
      reinit._ls.minMax(minV,maxV);
      scalar medialAxisDist=minV;
      for(sizeType x=1; x<reinit._ls.getNrPoint().x()-1; x++)
        for(sizeType y=1; y<reinit._ls.getNrPoint().y()-1; y++)
          for(sizeType z=1; z<reinit._ls.getNrPoint().z()-1; z++) {
            scalar ls0=reinit._ls.get(Vec3i(x,y,z));
            if(ls0 >= 0)
              continue;
            scalar lsnx=reinit._ls.get(Vec3i(x-1,y,z));
            scalar lspx=reinit._ls.get(Vec3i(x+1,y,z));
            scalar lsny=reinit._ls.get(Vec3i(x,y-1,z));
            scalar lspy=reinit._ls.get(Vec3i(x,y+1,z));
            scalar lsnz=reinit._ls.get(Vec3i(x,y,z-1));
            scalar lspz=reinit._ls.get(Vec3i(x,y,z+1));
            if(ls0<lsnx && ls0<lspx && ls0<lsny && ls0<lspy && ls0<lsnz && ls0<lspz)
              medialAxisDist=std::max(medialAxisDist,ls0);
          }
      INFOV("MedialAxisDist=%f!",medialAxisDist)
    }
  }
  return 0;
}

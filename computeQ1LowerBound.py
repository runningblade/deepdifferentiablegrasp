import numpy as np
import ctypes as ct
from distanceExact import find_lib_path
import random,math,torch,computeQ1UpperBound
from cryptography.hazmat.primitives.asymmetric.padding import PSS
torch.set_default_dtype(torch.float64)

class ComputeQ1Layer(torch.autograd.Function):
    #load ctype
    clib=ct.cdll.LoadLibrary(find_lib_path())
    clib.debugMode.argtypes=[]
    clib.debugMode.restype=ct.c_bool
    clib.surrogateMode.argtypes=[]
    clib.surrogateMode.restype=ct.c_bool
    clib.initQ1LowerBound.argtypes=[ct.POINTER(ct.c_double),ct.c_double,ct.c_int,ct.c_int,ct.c_int]
    clib.computeQ1LowerBound.argtypes=[ct.POINTER(ct.c_double),ct.POINTER(ct.c_double),ct.c_int,ct.POINTER(ct.c_double)]
    clib.computeQ1LowerBound.restype=ct.c_double
    nrPt=0
    nrDir=0
    alpha=0.0
    
    @staticmethod
    def init(M,mu,nrPt,alpha):
        #M: [6,6]
        #mu: scalar
        #alpha: scalar
        if M is None:
            M=np.eye(6,6,dtype=np.float64)
        else: M=np.linalg.inv(M).astype(np.float64)
        MPtr=M.ctypes.data_as(ct.POINTER(ct.c_double))
        nrDir=3 if mu>0 else 1
        ComputeQ1Layer.clib.initQ1LowerBound(MPtr,mu,2,nrPt,nrDir)
        ComputeQ1Layer.nrPt=nrPt
        ComputeQ1Layer.nrDir=nrDir
        ComputeQ1Layer.alpha=alpha
        ComputeQ1Layer.crossx=computeQ1UpperBound.ComputeQ1Layer().crossx
        ComputeQ1Layer.crossy=computeQ1UpperBound.ComputeQ1Layer().crossy
        ComputeQ1Layer.crossz=computeQ1UpperBound.ComputeQ1Layer().crossz
        ComputeQ1Layer.tangents=[torch.tensor([1,0,0]),torch.tensor([0,1,0]),torch.tensor([0,0,1])]
        ComputeQ1Layer.dVssdPNDSingle=torch.zeros(nrPt*(3*2+1),nrPt*nrDir,6)
        ComputeQ1Layer.dQ1dPNDSingle=torch.zeros(nrPt*(3*2+1))
        assert nrDir==1 or nrDir==3
    
    @staticmethod
    def tangent12(nss):
        vals,ids=torch.min(torch.abs(nss),dim=2)
        orthos=[None,None,None]
        for d in range(3):
            tdim=torch.where(ids==d,torch.ones(nss.shape[0],nss.shape[1]),torch.zeros(nss.shape[0],nss.shape[1]))
            orthos[d]=tdim.view(nss.shape[0],nss.shape[1],1)*ComputeQ1Layer.tangents[d].view(1,1,3)
        #cross
        nssx,nssy,nssz=torch.split(nss,[1,1,1],dim=2)
        nssc=nssx.view([-1,ComputeQ1Layer.nrPt,1,1])*ComputeQ1Layer.crossx+ \
            nssy.view([-1,ComputeQ1Layer.nrPt,1,1])*ComputeQ1Layer.crossy+  \
            nssz.view([-1,ComputeQ1Layer.nrPt,1,1])*ComputeQ1Layer.crossz
        t1n=torch.matmul(nssc,(orthos[0]+orthos[1]+orthos[2]).unsqueeze(3)).squeeze(3)
        t1=t1n/torch.norm(t1n,p=None,dim=2,keepdim=True)
        t2n=torch.matmul(nssc,t1.unsqueeze(3)).squeeze(3)
        t2=t2n/torch.norm(t2n,p=None,dim=2,keepdim=True)
        return t1,t2
        
    @staticmethod
    def forward(ctx, pss, dss, nss):
        #we need torch's default autograd function to compute \partial{vss}/\partial{pss,dss,nss}
        pss.requires_grad_()
        dss.requires_grad_()
        nss.requires_grad_()
        torch.set_grad_enabled(True)
        #pss: [b,3,np]
        #dss: [b,np]
        #nss: [b,np,3]
        assert ComputeQ1Layer.nrPt==pss.shape[2]
        Q1=torch.zeros(pss.shape[0])
        dQ1dPND=torch.zeros(pss.shape[0],ComputeQ1Layer.dQ1dPNDSingle.shape[0])
        #compute vss
        coef=torch.exp(-ComputeQ1Layer.alpha*torch.abs(dss))
        pss=pss.transpose(1,2)
        #cross product
        pssx,pssy,pssz=torch.split(pss,[1,1,1],dim=2)
        pssc =pssx.view([-1,ComputeQ1Layer.nrPt,1,1])*ComputeQ1Layer.crossx
        pssc+=pssy.view([-1,ComputeQ1Layer.nrPt,1,1])*ComputeQ1Layer.crossy
        pssc+=pssz.view([-1,ComputeQ1Layer.nrPt,1,1])*ComputeQ1Layer.crossz
        if ComputeQ1Layer.nrDir==1:
            psscn=torch.matmul(pssc,nss.unsqueeze(3)).squeeze(3)
            vss=torch.cat((nss,psscn),dim=2)*coef.unsqueeze(2)
        else:
            psscn=torch.matmul(pssc,nss.unsqueeze(3)).squeeze(3)
            vssn=torch.cat((nss,psscn),dim=2)*coef.unsqueeze(2)
            #t1
            t1ss,t2ss=ComputeQ1Layer.tangent12(nss)
            pssct1=torch.matmul(pssc,t1ss.unsqueeze(3)).squeeze(3)
            vsst1=torch.cat((t1ss,pssct1),dim=2)*coef.unsqueeze(2)
            #t2
            pssct2=torch.matmul(pssc,t2ss.unsqueeze(3)).squeeze(3)
            vsst2=torch.cat((t2ss,pssct2),dim=2)*coef.unsqueeze(2)
            #interleave
            vss=torch.cat((vssn,vsst1,vsst2),dim=1)
        #compute Q1
        offP=ComputeQ1Layer.nrPt*0
        offD=ComputeQ1Layer.nrPt*3
        offN=ComputeQ1Layer.nrPt*4
        for i in range(pss.shape[0]):
            #ComputeQ1Layer.dVssdPNDSingle=torch.zeros(nrPt*(3*2+1),6,nrPt*nrDir)
            for offV in range(vss.shape[1]):
                iPt=offV%ComputeQ1Layer.nrPt
                for d in range(3):
                    dVdd,dVdn=torch.autograd.grad(vss[i,offV,d],(dss,nss),retain_graph=True,allow_unused=True)
                    ComputeQ1Layer.dVssdPNDSingle[offD+iPt               ,offV,d]=dVdd[i,iPt]
                    ComputeQ1Layer.dVssdPNDSingle[offN+iPt*3:offN+iPt*3+3,offV,d]=dVdn[i,iPt,:]
                for d in range(3,6):
                    dVdp,dVdd,dVdn=torch.autograd.grad(vss[i,offV,d],(pss,dss,nss),retain_graph=True,allow_unused=True)
                    ComputeQ1Layer.dVssdPNDSingle[iPt*3:iPt*3+3          ,offV,d]=dVdp[i,iPt,:]
                    ComputeQ1Layer.dVssdPNDSingle[offD+iPt               ,offV,d]=dVdd[i,iPt]
                    ComputeQ1Layer.dVssdPNDSingle[offN+iPt*3:offN+iPt*3+3,offV,d]=dVdn[i,iPt,:]
            if ComputeQ1Layer.clib.debugMode():
                for vid in range(vss.shape[1]):
                    print('vss%3d: %+1.10f %+1.10f %+1.10f %+1.10f %+1.10f %+1.10f'%  \
                          (vid,vss[i,vid,0],vss[i,vid,1],vss[i,vid,2],vss[i,vid,3],vss[i,vid,4],vss[i,vid,5]))
            vssPtr=vss[i:].detach().numpy().ctypes.data_as(ct.POINTER(ct.c_double))
            dVssdPNDSinglePtr=ComputeQ1Layer.dVssdPNDSingle.numpy().ctypes.data_as(ct.POINTER(ct.c_double))
            dQ1dPNDSinglePtr=ComputeQ1Layer.dQ1dPNDSingle.numpy().ctypes.data_as(ct.POINTER(ct.c_double))
            Q1[i]=ComputeQ1Layer.clib.computeQ1LowerBound(vssPtr,dVssdPNDSinglePtr,ComputeQ1Layer.dVssdPNDSingle.shape[0],dQ1dPNDSinglePtr)
            dQ1dPND[i,:]=ComputeQ1Layer.dQ1dPNDSingle
        #result
        ctx.save_for_backward(Q1,dQ1dPND)
        return Q1
    
    @staticmethod
    def backward(ctx, Q1G):
        Q1,dQ1dPND=ctx.saved_tensors
        dQ1dPND=torch.unsqueeze(Q1G,dim=1)*dQ1dPND
        pssG=torch.reshape(dQ1dPND[:,ComputeQ1Layer.nrPt*0:ComputeQ1Layer.nrPt*3],(-1,ComputeQ1Layer.nrPt,3)).transpose(1,2)
        dssG=torch.reshape(dQ1dPND[:,ComputeQ1Layer.nrPt*3:ComputeQ1Layer.nrPt*4],(-1,ComputeQ1Layer.nrPt))
        nssG=torch.reshape(dQ1dPND[:,ComputeQ1Layer.nrPt*4:ComputeQ1Layer.nrPt*7],(-1,ComputeQ1Layer.nrPt,3))
        return pssG,dssG,nssG
        
    @staticmethod
    def grad_check(pss,dss,nss):
        pss=torch.Tensor(pss).transpose(1,2)
        dss=torch.Tensor(dss)
        nss=torch.Tensor(nss)
        pssTest=torch.autograd.Variable(torch.randn(pss.shape),requires_grad=True)
        dssTest=torch.autograd.Variable(torch.randn(dss.shape),requires_grad=True)
        nssTest=torch.autograd.Variable(torch.randn(nss.shape),requires_grad=True)
        if ComputeQ1Layer.clib.surrogateMode():
            print('AutoGradCheck=',torch.autograd.gradcheck(ComputeQ1Layer.apply,(pssTest,dssTest,nssTest),eps=1e-6,atol=1e-6,rtol=1e-5,raise_exception=True))
        else:
            print('The exact Q1LowerBound\'s accuracy is not suffice for gradient computation!')
            print('Recompile using RANDOM_Q1_SURROGATE to do the grad_check!')
        
if __name__=='__main__':
    for i in range(2):
        useMetric=i==0
        if not useMetric:
            M=None
        else:
            M=np.random.random_sample((6,6))
            M+=M.T
        ComputeQ1Layer.init(M,0.10,10,0.1)
        psss=[]
        dsss=[]
        nsss=[]
        for b in range(2):
            pss=[]
            dss=[]
            nss=[]
            for i in range(ComputeQ1Layer.nrPt):
                pss.append(np.random.random_sample((3))*2-1)
                dss.append(random.uniform(-1,1))
                nss.append(np.random.random_sample((3))*2-1)
                nss[-1]/=np.linalg.norm(nss[-1])
            psss.append(pss)
            dsss.append(dss)
            nsss.append(nss)
            ComputeQ1Layer.grad_check(psss,dsss,nsss)
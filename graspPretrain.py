import argparse,os,trimesh,re,pickle,torch,json,random,progressbar
from distance import filter_files
import numpy as np
import xml.etree.ElementTree as ET
import torch.optim as optim
from distanceExact import DistanceExact
from graspPlanner import mkdir,adjust_learning_rate,write_txt,write_vtk,points_to_vtk,config_from_xml
from network import DepthBasedCNN,GridBasedCNN
from hand import Hand,trimesh_to_vtk,write_vtk
from tensorboardX import SummaryWriter

class Data:
    def __init__(self,data_path,data_augmentation):
        self.data_path=data_path
        self.shapes_path=data_path+'raw_meshes'
        self.scaled_shapes_path=data_path+'scaled_obj_meshes'
        self.good_shapes_path=data_path+'good_shapes'
        self.grasp_path=data_path+'output'
        self.data_augmentation=data_augmentation
        self.hand=Hand("hand/ShadowHand/",0.01,use_joint_limit=True,use_quat=True,use_eigen=False)

    def get_center_of_mesh(self,func_input):
        if isinstance(func_input,str):
            mesh=trimesh.load_mesh(func_input)
            vertices=mesh.vertices
        else:
            vertices=func_input
        minX=999999
        minY=999999
        minZ=999999
        maxX=-999999
        maxY=-999999
        maxZ=-999999
        for i in range(vertices.shape[0]):
            if vertices[i][0]>maxX:
                maxX=vertices[i][0]
            if vertices[i][1]>maxY:
                maxY=vertices[i][1]
            if vertices[i][2]>maxZ:
                maxZ=vertices[i][2]
            if vertices[i][0]<minX:
                minX=vertices[i][0]
            if vertices[i][1]<minY:
                minY=vertices[i][1]
            if vertices[i][2]<minZ:
                minZ=vertices[i][2]
        center=np.zeros(3)
        center[0]=(minX+maxX)*0.5
        center[1]=(minY+maxY)*0.5
        center[2]=(minZ+maxZ)*0.5
        return center

    def prepare_grasp_gt(self,iros_data_path,new_data_path):
        iros_xml_fold_list=os.listdir(iros_data_path)
        iros_sorted_xml_fold_list=[]
        for i in range(len(iros_xml_fold_list)):
            elements=iros_xml_fold_list[i]
            obj_name=elements[:-10]
            if obj_name not in iros_sorted_xml_fold_list:
                iros_sorted_xml_fold_list.append(obj_name)
        new_obj_list=os.listdir('../ddg_data/grasp_dataset/scaled_obj_meshes')
        for i in range(len(new_obj_list)):
            if new_obj_list[i][:-11] in iros_sorted_xml_fold_list:
                command='cp -r '+iros_data_path+'/'+new_obj_list[i][:-11]+'* '+new_data_path+'/'
                os.system(command)

    def modify_generated_grasp(self,robot_path,smoothed_scaled_mesh_path): # can only be used once
        scaled_shapes_list=[]
        filter_files(self.scaled_shapes_path,"_scaled.obj",scaled_shapes_list)
        for i in range(len(scaled_shapes_list)):
            scaled_mesh=trimesh.load(scaled_shapes_list[i]).apply_scale(100)
            smoothed_scaled_mesh=trimesh.load(scaled_shapes_list[i]+'.smoothed.obj').apply_scale(100)
            print(self.get_center_of_mesh(scaled_mesh.vertices))
            print(self.get_center_of_mesh(smoothed_scaled_mesh.vertices))
            obj_name=re.split("/",scaled_shapes_list[i])[-1][:-11]
            for ii in range(5):
                for jj in range(5):
                    for kk in range(5):
                        obj_grasp_path=self.grasp_path+'/'+obj_name+'.xml_'+str(ii)+'_'+str(jj)+'_'+str(kk)
                        if os.path.exists(obj_grasp_path):
                            grasp_list=filter_files(obj_grasp_path,".xml",[])
                            for n in range(len(grasp_list)):
                                tree=ET.parse(grasp_list[n])
                                root=tree.getroot()
                                root[0][0].text=smoothed_scaled_mesh_path+'/'+obj_name+'_scaled.obj.smoothed.xml'
                                root[1][0].text=robot_path
                                extrinsic=re.findall(r"\-*\d+\.?\d*",root[1][2][0].text)
                                translation=self.get_center_of_mesh(smoothed_scaled_mesh.vertices)-\
                                    self.get_center_of_mesh(scaled_mesh.vertices)
                                x=float(extrinsic[4])+translation[0]
                                y=float(extrinsic[5])+translation[1]
                                z=float(extrinsic[6])+translation[2]
                                extrinsic[4]=str(x)
                                extrinsic[5]=str(y)
                                extrinsic[6]=str(z)
                                trans='('
                                trans=trans+extrinsic[0]+' '
                                trans=trans+extrinsic[1]+' '
                                trans=trans+extrinsic[2]+' '
                                trans=trans+extrinsic[3]+')['
                                trans=trans+extrinsic[4]+' '
                                trans=trans+extrinsic[5]+' '
                                trans=trans+extrinsic[6]+']'
                                root[1][2][0].text=trans
                                tree.write(grasp_list[n])
            print('Finish '+str(i)+': '+obj_name)

    def grasp_select(self,data_path):
        graspfiles = os.listdir(data_path)
        for i in range(20):
                grasp = 'grasp' + str(i) + '.xml'
                if grasp in graspfiles:
                        return grasp

    def generate_xml_list(self):
        xml_fold_list=os.listdir(self.grasp_path)
        sorted_xml_fold_list=[]
        if os.path.exists(self.data_path+'/best_xmls'):
            with open(self.data_path+'/best_xmls', 'rb') as fp:
                self.xml_list=pickle.load(fp)
            return
        else:
            for i in range(len(xml_fold_list)):
                elements=xml_fold_list[i]
                obj_name=elements[:-10]
                if obj_name not in sorted_xml_fold_list:
                    sorted_xml_fold_list.append(obj_name)
            all_xmls = {}
            for i in range(len(sorted_xml_fold_list)):
                obj_name=sorted_xml_fold_list[i]
                folders=[]
                for j in range(len(xml_fold_list)):
                    if len(folders)>=100:
                        continue
                    if sorted_xml_fold_list[i] in xml_fold_list[j]:
                        grasp_path=self.grasp_path+'/'+xml_fold_list[j]
                        grasp=self.grasp_select(grasp_path)
                        if grasp is None:
                            continue
                        folders.append(grasp_path+'/'+grasp)
                all_xmls[obj_name] = folders
                print(i)
            with open(self.data_path+'/best_xmls', 'wb') as fp:
                pickle.dump(all_xmls, fp)
        self.xml_list=all_xmls

    def generate_all_xml_list(self):
        xml_fold_list=os.listdir(self.grasp_path)
        sorted_xml_fold_list=[]
        if os.path.exists(self.data_path+'/all_xmls'):
            with open(self.data_path+'/all_xmls', 'rb') as fp:
                self.xml_list=pickle.load(fp)
            return
        else:
            for i in range(len(xml_fold_list)):
                elements=xml_fold_list[i]
                obj_name=elements[:-10]
                if obj_name not in sorted_xml_fold_list:
                    sorted_xml_fold_list.append(obj_name)
            all_xmls = {}
            for i in range(len(sorted_xml_fold_list)):
                obj_name=sorted_xml_fold_list[i]
                folders=[]
                for j in range(len(xml_fold_list)):
                    if sorted_xml_fold_list[i] in xml_fold_list[j]:
                        grasp_path=self.grasp_path+'/'+xml_fold_list[j]
                        xml_list=filter_files(grasp_path,'.xml',[])
                        for k in range(len(xml_list)):
                            folders.append(xml_list[k])
                all_xmls[obj_name] = folders
                print(i)
            with open(self.data_path+'/all_xmls', 'wb') as fp:
                pickle.dump(all_xmls, fp)
        self.xml_list=all_xmls

    def generate_nn_data(self):
        self.generate_xml_list()
        scaled_shapes_list=filter_files(self.scaled_shapes_path,".smoothed.obj",[])
        for i in range(len(scaled_shapes_list)):
            smoothed_obj_name=re.split("/",scaled_shapes_list[i])[-1]
            obj_name=smoothed_obj_name[:-24]
            grasp_xmls=self.xml_list[obj_name]
            grasp=[]
            for j in range(len(grasp_xmls)):
                config,_=config_from_xml(self.hand,grasp_xmls[j])
                grasp.append(config)
            grasp_torch=torch.Tensor(grasp)
            torch.save(grasp_torch,self.scaled_shapes_path+'/'+smoothed_obj_name+'.grasp')
            print('Finish '+str(i))

    def generate_z_positive_nn_data(self):
        self.generate_all_xml_list()
        scaled_shapes_list=filter_files('../ddg_data/grasp_dataset/scaled_obj_meshes','.smoothed.obj',[])
        min_z_positive_count=1000000
        for i in range(len(scaled_shapes_list)):
            smoothed_obj_name=re.split("/",scaled_shapes_list[i])[-1]
            obj_name=smoothed_obj_name[:-24]
            grasp_xmls=self.xml_list[obj_name]
            z_positive_count=0
            z_positive_grasp_xmls=[]
            grasps=[]
            for j in range(len(grasp_xmls)):
                config,_=config_from_xml(self.hand,grasp_xmls[j])
                if config[2]>0:
                    z_positive_count+=1
                    z_positive_grasp_xmls.append(grasp_xmls[j])
            z_positive_grasp_xmls_ordered=[]
            for j in range(20):
                for k in range(len(z_positive_grasp_xmls)):
                    grasp='grasp'+str(j)+'.xml'
                    if grasp in z_positive_grasp_xmls[k]:
                        z_positive_grasp_xmls_ordered.append(z_positive_grasp_xmls[k])
            for j in range(100):
                config,_=config_from_xml(self.hand,z_positive_grasp_xmls_ordered[j])
                grasps.append(config)
            grasps_torch=torch.Tensor(grasps)
            torch.save(grasps_torch,'../ddg_data/scaled_obj_meshes/'+smoothed_obj_name+'.Zgrasp')
            if z_positive_count<min_z_positive_count:
                min_z_positive_count=z_positive_count
            print('Finish '+obj_name+': '+str(z_positive_count))
        print('Min z_positive count: '+str(min_z_positive_count))

class CombinedLoss(torch.nn.Module):
    def __init__(self):
        super(CombinedLoss,self).__init__()

    def forward(self,distance_fn,predict_grasp,grasp_labels,mesh_paths):
        predict_grasp=predict_grasp.view(predict_grasp.shape[0],1,predict_grasp.shape[1])
        dist,_=torch.min(torch.norm(grasp_labels-predict_grasp,2,2)**2,-1)
        dist=dist/predict_grasp.shape[2]
        return dist.mean()

class GraspPretrain():
    def __init__(self,args):
        self.args=args
        self.loss_fn=CombinedLoss()
        self.distance_fn=DistanceExact
        #create hand
        self.hand=Hand("hand/"+self.args.hand+"/",0.01,use_joint_limit=self.args.joint_limit,\
                use_quat=True,use_eigen=self.args.use_eigen)
        self.network=None
        #output directory
        self.ex_name=self.args.ex_name
        self.ex_dir=self.args.output_dir+self.ex_name
        mkdir(self.ex_dir)
        #read objects for distance calculation
        self.object_paths=filter_files(self.args.input_dir,'.BVH.dat',[],abs=True)
        self.train_file_list=[]
        self.test_file_list=[]
        for i in range(len(self.object_paths)):
            if 'bigbird' in self.object_paths[i] or 'ycb' in self.object_paths[i]:
                self.test_file_list.append(self.object_paths[i][:-8])
            else: self.train_file_list.append(self.object_paths[i][:-8])
        #build optimized variables
        self.input_type=self.args.input_type
        if self.input_type=='grid':
            self.network=GridBasedCNN(self.args.hand,self.hand)
        else: 
            self.network=DepthBasedCNN(self.args.hand,self.args.cnn_name,self.args.num_views,True,self.hand)
        #try loading
        use_gpu=self.args.use_gpu
        only_train=self.args.only_train
        if self.args.resume>=1:
            model_list=filter_files(self.ex_dir,".pt",[])
            assert os.path.exists(self.ex_dir) or len(model_list)>0,("No trained model or config file found!")
            self.network=torch.load(self.ex_dir+"/model.pt",map_location=lambda storage,loc:storage)
            if self.args.resume>=2:
                assert os.path.exists(self.ex_dir) or len(model_list)>0 or os.path.exists(self.ex_dir+"/config.json"), \
                ("No trained model or config file found!")
                with open(os.path.join(self.ex_dir,"config.json")) as json_file:
                    args=argparse.Namespace(**json.load(json_file))
                args.resume=self.args.resume
                self.args=args
            else:
                config_f=open(os.path.join(self.ex_dir,"config.json"),"w")
                json.dump(vars(self.args),config_f)
                config_f.close()
        else:
            config_f=open(os.path.join(self.ex_dir,"config.json"),"w")
            json.dump(vars(self.args),config_f)
            config_f.close()
            torch.save(self.network,self.ex_dir+'/init.pt')
        self.args.use_gpu=use_gpu
        self.args.only_train=only_train

    def plan(self):
        if self.args.resume<3:
            os.system('rm -rf '+self.ex_dir+'/train')
            os.system('rm -rf '+self.ex_dir+'/test')
            train_writer=SummaryWriter(self.ex_dir+'/train')
            test_writer=SummaryWriter(self.ex_dir+'/test')
            optimizer=optim.Adam(self.network.parameters(),lr=self.args.lr)
            #main loop
            best_error=99999
            for epoch in range(1,self.args.max_epoch):
                adjust_learning_rate(optimizer,epoch,self.args.lr,0.99,400)
                loss=self.train_with_torch(optimizer)
                if self.args.only_train:
                    error=0
                else: error=self.test_with_torch()
                train_writer.add_scalar('Train loss',loss,epoch)
                test_writer.add_scalar('Test error',error,epoch)
                print('Train average loss: ',loss)
                print('Test average error: ',error)
                print('Finish epoch: ',epoch)
                if self.args.only_train:
                    error=loss
                if error<best_error:
                    best_error=error
                    print('Best error at epoch: ',epoch,'=',best_error)
                    print('Save model..................')
                    print("##########################################")
                    torch.save(self.network,self.ex_dir+'/model.pt')
            train_writer.close()
            test_writer.close()
        else:
            self.evaluate()
    
    def load_data(self,start_index,size,file_list):
        if len(file_list)-start_index<size:
            size=len(file_list)-start_index
        paths=file_list[start_index:start_index+size]
        self.object_paths=[]
        for i in range(size):
            self.object_paths.append(paths[i]+'.BVH.dat')
        batch_grasp=torch.zeros(size,100,25)
        if self.args.input_type=='grid':
            batch_data=torch.zeros(size,1,40,40,40)
            for i in range(size):
                batch_data[i]=torch.load(paths[i]+'.smoothed.obj.grid')
                if self.args.use_good_gt:
                    batch_grasp[i]=torch.load(paths[i]+'.smoothed.obj.grasp')
                else:
                    batch_grasp[i]=torch.load(paths[i]+'.smoothed.obj.Zgrasp')
                batch_grasp[i][:,self.hand.extrinsic_size:]=batch_grasp[i][:,self.hand.extrinsic_size:]/40.0
        if self.args.input_type=='depth':
            batch_data=torch.zeros(size*self.args.num_views,3,224,224)
            for i in range(size):
                depth_tensor=torch.load(paths[i]+'.smoothed.obj.depth')
                assert self.args.num_views<=depth_tensor.shape[0],\
                    ('number of view must less than rendered image views')
                for k in range(self.args.num_views):
                    batch_data[i*self.args.num_views+k]=torch.Tensor(depth_tensor[k])
                if self.args.use_good_gt:
                    batch_grasp[i]=torch.load(paths[i]+'.smoothed.obj.grasp')
                else:
                    batch_grasp[i]=torch.load(paths[i]+'.smoothed.obj.Zgrasp')
                batch_grasp[i][:,self.hand.extrinsic_size:]=batch_grasp[i][:,self.hand.extrinsic_size:]/40.0
        return batch_data,batch_grasp

    def get_loss(self,input_data,grasp_data):
        predict_grasp=self.network.forward(input_data)
        extrinsic,dof=torch.split(predict_grasp,[self.hand.extrinsic_size,predict_grasp.shape[1]-self.hand.extrinsic_size],dim=1)
        dof=dof/40.0
        modified_predict_grasp=torch.cat((extrinsic,dof),1)
        loss=self.loss_fn(self.distance_fn,modified_predict_grasp,grasp_data,self.object_paths)
        return loss
        
    def train_with_torch(self,optimizer):
        average_loss=0
        batch_size=self.args.bs
        random.shuffle(self.train_file_list)
        p=progressbar.ProgressBar()
        p.start(len(self.train_file_list))
        for i in range(0,len(self.train_file_list),batch_size):
            input_data,grasp_data=self.load_data(i,batch_size,self.train_file_list)
            if self.args.use_gpu:
                input_data=input_data.cuda()
                grasp_data=grasp_data.cuda()
                self.network=self.network.cuda()
            self.network.train()
            optimizer.zero_grad()
            loss=self.get_loss(input_data,grasp_data)
            loss.backward()
            optimizer.step()
            average_loss+=loss
            p.update(i)
        p.finish()
        print('---------Finish one training epoch---------')
        average_loss/=len(self.train_file_list)
        return average_loss.item()
    
    def test_with_torch(self):
        with torch.no_grad():
            average_error=0
            batch_size=self.args.bs
            random.shuffle(self.test_file_list)
            p=progressbar.ProgressBar()
            p.start(len(self.test_file_list))
            self.network=self.network.eval()
            for i in range(0,len(self.test_file_list),batch_size):
                input_data,grasp_data=self.load_data(i,batch_size,self.test_file_list)
                if self.args.use_gpu:
                    input_data=input_data.cuda()
                    grasp_data=grasp_data.cuda()
                    self.network=self.network.cuda()
                error=self.get_loss(input_data,grasp_data)
                average_error+=error
                p.update(i)
            p.finish()
            print('---------Finish one test epoch---------')
            average_error/=len(self.test_file_list)
            return average_error.item()

    def generate_results(self,mesh_path,input_data,grasp_data,hand,gt):
        model_dir=self.ex_dir
        self.ex_dir=self.ex_dir+'/'+mesh_path.split('/')[-1]
        mkdir(self.ex_dir)
        if self.args.use_gpu:
            input_data=input_data.cuda()
        final_predict_grasp=self.network.forward(input_data)
        final,_,_=self.hand.forward(final_predict_grasp)
        self.init_network=torch.load(model_dir+'/init.pt',map_location=lambda storage,loc:storage)
        if self.args.use_gpu:
            self.init_network=self.init_network.cuda()
        self.init_network=self.init_network.eval()
        init_predict_grasp=self.init_network.forward(input_data)
        init,_,_=self.hand.forward(init_predict_grasp)
        self.distance_fn.mesh_paths=self.object_paths
        d_ori=self.distance_fn.apply(init)
        d_final=self.distance_fn.apply(final)

        original_contacts_vtk=points_to_vtk(init.transpose(1,2))
        write_vtk(original_contacts_vtk,self.ex_dir+'/original_contact.vtk')
        final_contacts_vtk=points_to_vtk(final.transpose(1,2))
        write_vtk(final_contacts_vtk,self.ex_dir+'/final_contact.vtk')

        self.hand.forward_kinematics(init_predict_grasp[0].cpu().detach().numpy()[0:self.hand.extrinsic_size], \
                    init_predict_grasp[0].cpu().detach().numpy()[self.hand.extrinsic_size:])            
        hand_vtk=trimesh_to_vtk(self.hand.draw(1,False))
        write_vtk(hand_vtk,self.ex_dir+'/original_hand.vtk')

        self.hand.forward_kinematics(final_predict_grasp[0].cpu().detach().numpy()[0:self.hand.extrinsic_size], \
                                        final_predict_grasp[0].cpu().detach().numpy()[self.hand.extrinsic_size:])
        hand_vtk=trimesh_to_vtk(self.hand.draw(1,False))
        write_vtk(hand_vtk,self.ex_dir+'/final_hand.vtk')

        obj_vtk=trimesh_to_vtk(trimesh.load(mesh_path+'.smoothed.obj'))
        write_vtk(obj_vtk,self.ex_dir+'/mesh.vtk')

        write_txt(str(d_ori[2]),self.ex_dir+'/Distance_original.txt')
        write_txt(str(d_final[2]),self.ex_dir+'/Distance_final.txt')

        if gt:
            mkdir(self.ex_dir+'/gt_grasp')
            grasp_data[0][:,self.hand.extrinsic_size:]=grasp_data[0][:,self.hand.extrinsic_size:]*10.0
            for i in range(len(grasp_data[0])):    
                self.hand.forward_kinematics(grasp_data[0][i].detach().numpy()[0:self.hand.extrinsic_size], \
                                    grasp_data[0][i].detach().numpy()[self.hand.extrinsic_size:])
                hand_vtk=trimesh_to_vtk(self.hand.draw(1,False))
                write_vtk(hand_vtk,self.ex_dir+'/gt_grasp'+'/'+'gt_'+str(i)+'.vtk')
        self.ex_dir=model_dir

    def evaluate(self):
        with torch.no_grad():
            if self.args.use_gpu:
                self.network=self.network.cuda()
            self.network=self.network.eval()
            if self.args.only_train:
                eval_file_list=self.train_file_list
            else: eval_file_list=self.test_file_list
            for i in range(len(eval_file_list)):
                input_data,grasp_data=self.load_data(i,1,eval_file_list)
                mesh_path=eval_file_list[i]
                self.generate_results(mesh_path,input_data,grasp_data,self.hand,False)
                print('Finished '+mesh_path)

def get_parser():
    parser=argparse.ArgumentParser()
    parser.add_argument("-ex_name",type=str,help="experiment name",default="ex")
    parser.add_argument("-optimizer",type=str,help="optimizer",default="adam")
    parser.add_argument("-distance_fn",type=str,help="function for distance calculation",default="exact")  
    parser.add_argument("-lr",type=float,help="learning rate",default=1e-2)
    parser.add_argument("-hand",type=str,help="ShadowHand or BarrettHand",default="ShadowHand")
    parser.add_argument("-joint_limit",action="store_true",help="if set, apply joint limit to hand")
    parser.add_argument("-input_dir",type=str,default="../ddg_data/grasp_dataset/scaled_obj_meshes")
    parser.add_argument("-output_dir",type=str,default="../ddg_results/")
    parser.add_argument("-input_type",type=str,help="input data type: grids or depths",default="grid")
    parser.add_argument("-bs",type=int,help="batch size",default=2)
    parser.add_argument("-use_pretrained",action="store_true",help="if set, use pretrained depth-based model")
    parser.add_argument("-cnn_name",type=str,help="depth-based cnn model name",default="alexnet")
    parser.add_argument("-num_views",type=int,help="depth-based number of views",default=3)
    parser.add_argument("-use_gpu",action="store_true",help="if set, use gpu to train")
    parser.add_argument("-use_eigen",action="store_true",help="if set, use eigen for hand FK")
    parser.add_argument("-use_good_gt",action="store_true",help="if set, use good gt to pre-train")
    parser.add_argument("-gradcheck",action="store_true",help="if set, gradcheck for output")
    parser.add_argument("-only_train",action="store_true",help="if set, only use training file for testing")
    parser.add_argument("-max_epoch",type=int,default=300000)
    parser.add_argument("-resume",type=int,help="0:train a new model, \
                                                 1:retrain the model with new config, \
                                                 2:continue to train the model with old config, \
                                                 3:evaluate.",default=0)
    return parser

if __name__=='__main__':
    mydata=Data('../ddg_data/grasp_dataset/',False)
    mydata.generate_z_positive_nn_data()

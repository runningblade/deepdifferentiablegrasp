#include <gmpxx.h>
#include "SDPDerivative/internal/SOSPolynomialScalarQ.h"
#include "SDPDerivative/SYSVInterface.h"
#include "SDPDerivative/DebugGradient.h"
#include "SDPDerivative/SparseUtils.h"
#include "SDPDerivative/MPFKernel.h"
#include "CommonFile/Timing.h"
#include <Eigen/Eigen>

USE_PRJ_NAMESPACE

template <typename T2>
void testSymmetricKronecker(int N=10)
{
  debugTriToIJ(100);
  typedef typename ScalarUtil<T2>::ScalarMat DMat;
  typedef typename ScalarUtil<T2>::ScalarCol DVec;
  DEFINE_NUMERIC_DELTA_T(T2)
  DMat X,A,B;
  X.setRandom(N,N);
  X=(X+X.transpose()).eval();
  A.setRandom(N,N);
  B.setRandom(N,N);
  DVec HV=symmetricKronecker<T2,DMat,DMat>(N,A,B)*toSVec<T2>(X);
  DVec HVRef=toSVec<T2>((A*X*B.transpose()+B*X*A.transpose())/2);
  DEBUG_GRADIENT("HV",HV.norm(),(HV-HVRef).norm())
  debugsymmetricKroneckerEigen(N);
}
int main()
{
  testSymmetricKronecker<scalarD>();
  SYSVInterface<double>::debug();
  SYSVInterface<float>::debug();
  SYSVInterface<SDPA_FLOAT>::debug();
}

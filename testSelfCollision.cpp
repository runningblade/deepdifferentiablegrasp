#include "DistanceExact/SelfCollision.h"
#include "DistanceExact/RotationUtil.h"
#include "CommonFile/MakeMesh.h"

USE_PRJ_NAMESPACE

#define N 4
#define RAD 0.1
int main(int argc,char** argv)
{
  ObjMesh obj;
  SelfCollision coll;
  for(sizeType i=0; i<N; i++) {
    sizeType type=RandEngine::randI(0,2);
    if(type==0)
      MakeMesh::makeBox3D(obj,Vec3::Random()+Vec3::Ones());
    else if(type==1)
      MakeMesh::makeTorus3D(obj,RandEngine::randR(1,2),RandEngine::randR(0.1,0.5),32,32);
    else
      MakeMesh::makeSphere3D(obj,RandEngine::randR(1,2),32);
    if(i<N-1)
      coll.addLink(obj,RAD);
  }
  coll.assemble();
  SelfCollision::TSS tss;
  for(sizeType i=0; i<N; i++)
    tss.push_back(concatCol<Mat3X,Mat3X>(expWGradV<scalar,Vec3>(Vec3::Random()*M_PI),Vec3::Random()));
  coll.writeVTK(&tss,"shapes");
  ObjectSample objS(obj,RAD);
  objS.writeVTK("shapes/object.vtk");
  coll.detectVTK(&objS,tss,"shapes");
  INFO("Deepest=False")
  coll.debugGradient(&objS,false);
  INFO("Deepest=True")
  coll.debugGradient(&objS,true);
  return 0;
}

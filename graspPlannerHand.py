import graspPlanner

if __name__=='__main__':
    parser=graspPlanner.get_parser()
    args=parser.parse_args()
    args.distance_fn='exact'
    args.optimizer='adam'
    args.mode='hand'
    args.hand='ShadowHand'
    args.joint_limit=True
    args.ex_name='hand_grid_1_meshes_distanceSum_0.1normalSum_dir4'
    args.collision=2
    args.alpha=3.0
    args.beta=8.0
    args.lr=0.001
    args.dir_res=4
    args.Q1Type='Q1Upper'
    args.mu=0.7
    args.resume=3
    args.input_dir='../ddg_data/gd_toy_poisson_022'
    args.debug=False
    args.use_quat=True
    args.use_eigen=False
    myPlanner=graspPlanner.GraspPlanner(args)
    myPlanner.plan()
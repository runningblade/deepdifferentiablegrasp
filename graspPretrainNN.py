import graspPretrain
from distance import filter_files
import torch
if __name__=='__main__':
    torch.cuda.set_device(0)
    parser=graspPretrain.get_parser()
    args=parser.parse_args()
    args.hand='ShadowHand'
    args.distance_fn='exact'
    args.optimizer='adam'
    args.joint_limit=True
    args.num_views=5
    args.cnn_name='resnet50'
    args.input_dir='../ddg_data/grasp_dataset/scaled_obj_meshes_aug'
    args.input_type='depth'
    args.bs=8
    args.lr=1e-4
    args.resume=0
    args.only_train=False
    args.use_gpu=True
    args.use_good_gt=True
    args.ex_name='pretrain_depth_all_meshes_aug_goodgt'
    myPlanner=graspPretrain.GraspPretrain(args)
    myPlanner.plan()

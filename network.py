import torch.nn as nn
import torch
import torchvision.models as models
import math

class DepthBasedCNN(nn.Module):
    def __init__(self,hand_name,cnn_name,num_views,pretraining,hand):
        super(DepthBasedCNN,self).__init__()
        self.hand_name=hand_name
        self.num_views=num_views
        self.cnn_name=cnn_name
        self.pretraining=pretraining
        self.use_resnet=cnn_name.startswith('resnet')
        output_num=0
        if hand.use_eigen:
            output_num=hand.extrinsic_size+hand.eg_num
        else:
            output_num=hand.extrinsic_size+hand.nr_dof()
        if self.use_resnet:
            if self.cnn_name == 'resnet18':
                net = models.resnet18(pretrained=self.pretraining)
                net.fc =nn.Linear(512,output_num)
            elif self.cnn_name == 'resnet34':
                net = models.resnet34(pretrained=self.pretraining)
                net.fc =nn.Linear(512,output_num)
            elif self.cnn_name == 'resnet50':
                net = models.resnet50(pretrained=self.pretraining)
                net.fc =nn.Linear(2048,output_num)
            self.net_1=nn.Sequential(*list(net.children())[:-1])
            self.net_2=net.fc
        else:
            if self.cnn_name == 'alexnet':
                self.net_1 = models.alexnet(pretrained=self.pretraining).features
                self.net_2 = models.alexnet(pretrained=self.pretraining).classifier
            elif self.cnn_name == 'vgg11':
                self.net_1 = models.vgg11(pretrained=self.pretraining).features
                self.net_2 = models.vgg11(pretrained=self.pretraining).classifier
            elif self.cnn_name == 'vgg16':
                self.net_1 = models.vgg16(pretrained=self.pretraining).features
                self.net_2 = models.vgg16(pretrained=self.pretraining).classifier
            self.net_2._modules['6'] = nn.Linear(4096,output_num)

        # parameters initialization
        if not self.pretraining:
            for m in self.modules():
                if isinstance(m,nn.Conv2d):
                    n=m.kernel_size[0]*m.kernel_size[1]*m.out_channels
                    m.weight.data.normal_(0,math.sqrt(2./n))
                elif isinstance(m,nn.Linear):
                    torch.nn.init.xavier_uniform_(m.weight)
                    m.bias.data.fill_(0.01)
                elif isinstance(m,nn.BatchNorm2d):
                    m.weight.data.fill_(1)
                    m.bias.data.zero_()

    def forward(self, x):
        y = self.net_1(x)
        y = y.view((int(x.shape[0]/self.num_views),self.num_views,y.shape[-3],y.shape[-2],y.shape[-1]))#(bs,num_views,512,7,7)
        return self.net_2(torch.max(y,1)[0].view(y.shape[0],-1))

class GridBasedCNN(nn.Module):
    def __init__(self,hand_name,hand):
        super(GridBasedCNN,self).__init__() #40*40*40
        self.hand_name=hand_name
        output_num=0
        if hand.use_eigen:
            output_num=hand.extrinsic_size+hand.eg_num
        else:
            output_num=hand.extrinsic_size+hand.nr_dof()
        self.grid_conv=nn.Sequential(
            nn.Conv3d(1,64,kernel_size=4),
            nn.ReLU(),
            nn.Conv3d(64,64,kernel_size=4),
            nn.ReLU(),
            nn.MaxPool3d(kernel_size=(2,2,2),stride=(2,2,2)),
            nn.Conv3d(64,64,kernel_size=4),
            nn.ReLU(),
            nn.MaxPool3d(kernel_size=(2,2,2),stride=(2,2,2)),
        )
        self.grid_fc=nn.Sequential(
            nn.Linear(64*7*7*7,4096),
            nn.ReLU(),
            nn.Linear(4096,1024),
            nn.ReLU(),
            nn.Linear(1024,output_num)
        )

        # parameters initialization
        for m in self.modules():
            if isinstance(m,nn.Conv3d):
                n=m.kernel_size[0]*m.kernel_size[1]*m.kernel_size[2]*m.out_channels
                m.weight.data.normal_(0,math.sqrt(2./n))
            elif isinstance(m,nn.Linear):
                torch.nn.init.xavier_uniform_(m.weight)
                m.bias.data.fill_(0.01)
            elif isinstance(m,nn.BatchNorm3d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m,nn.BatchNorm1d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()

    def forward(self, x1):
        x1=self.grid_conv(x1)
        x1=x1.view(x1.size(0),-1)
        x1=self.grid_fc(x1)
        return x1

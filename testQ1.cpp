#include "SDPDerivative/Q1LowerBound.h"
#include "SDPDerivative/Utils.h"

USE_PRJ_NAMESPACE

void testSolve(scalarD mu,sizeType halfOrder,sizeType nrPt,sizeType nrDir)
{
  Mat6d M=Mat6d::Identity();
  //M.diagonal()+=Vec6d::Random()*0.3f;
  Mat3Xd pss=Mat3Xd::Random(3,nrPt);
  Mat3Xd nss=Mat3Xd::Random(3,nrPt);
  for(sizeType i=0;i<pss.cols();i++)
    nss.col(i)=pss.col(i).normalized();

  {
    //compute random value
    SDPSolution sol;
    Q1LowerBound Q1(M,mu,halfOrder,nrPt,nrDir);
    Mat6Xd vss=Q1.buildVss(pss,nss);
    Q1LowerBound::SOSProb prob=Q1.buildProb(vss);
    //Q1.writeProbSDPA(prob,"Q1SDPA.dat");
    INFOV("Q1SDPA=%f",Q1.solveProbSDPA(prob,sol))
    //sol.debugFZ();
    INFOV("Q1Mosek=%f",Q1.solveProbMosek(prob,sol))
  }
}
void testSensitivity(scalarD mu,sizeType halfOrder,sizeType nrPt,sizeType nrDir)
{
  Mat6d M=Mat6d::Identity();
  M.diagonal()+=Vec6d::Random()*0.3f;
  Q1LowerBound Q1(M,mu,halfOrder,nrPt,nrDir);
  {
    //Q1.debugBuildProb();
    //Q1.debugJGSymmetric(1);
    //Q1.debugJGEigen();
    //Q1.debugJG();
    Q1.debugDXDEps(100);
  }
}
int main()
{
  //while(true)
  //testSolve(0.1,2,20,3);
  //while(true)
  //testSolve(0.75,2,10,4);
  while(true) {
  //testSolve(0.00,2,5,1);
    testSensitivity(0.10,2,10,3);
  }
  return 0;
}

#ifndef PYTHON_INTERFACE_H
#define PYTHON_INTERFACE_H

#include "DistanceExact/ObjMeshGeomCellExact.h"
#include "DistanceExact/SelfCollision.h"
#include "SDPDerivative/Q1LowerBound.h"
#include "SDPDerivative/Utils.h"

extern "C"
{
  bool debugMode();
  bool surrogateMode();
  //distance
  void distance(const char** paths,int nrShape,const double* pss,double* dss,double* nss,double* hss,int nrPt);
  //Q1 lower bound
  void initQ1LowerBound(const double* M,double mu,int halfOrder,int nrPt,int nrDir);
  double computeQ1LowerBound(const double* vss,const double* dvss,int nr,double* dQ1dv);
  //self collision
  void writeSelfColl(const char* path);
  void readSelfColl(const char* path);
  void createSelfColl();
  void printSelfColl();
  void assemble();
  void prune(int i,int j);
  int addLink(const double* vss,int nrPt,double r);
  void detect(const double* tss,const char** paths,int nr,double* dtss,double* loss,bool deepest);
  void writeVTK(const double* tss,const char** paths,int nr,const char* path);
}

#endif

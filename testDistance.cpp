#include "DistanceExact/TriangleExact.h"
#include "DistanceExact/ObjMeshGeomCellExact.h"

USE_PRJ_NAMESPACE

int main(int argc,char** argv)
{
  TriangleExact::debugPointDist("debugTriangleExact");
  if(argc>1) {
    ObjMeshGeomCellExact cell;
    cell.SerializableBase::read(argv[1]);
    cell.debugPointDist();
    cell.writePointDistVTK("pointDist.vtk");
  }
  return 0;
}

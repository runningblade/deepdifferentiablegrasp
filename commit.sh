rm -rdf __pycache__
rm -rdf debugWriteVTK
rm -rdf *.pickle
rm -rdf limits
rm *.log
rm *.pyc
rm *.user
rm *.vtp
rm *.jpg
rm *.vti
rm *.vtk
rm *.user
rm *.th
rm *.out
git add --all
git commit -m "$1"
git push -u origin master

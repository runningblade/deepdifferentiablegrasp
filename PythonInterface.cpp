#include "PythonInterface.h"
#include "CommonFile/Timing.h"
#include "SDPDerivative/DebugGradient.h"
//#define RANDOM_Q1_SURROGATE
//#define DEBUG_MODE

extern "C"
{
  //debug
  bool debugMode()
  {
#ifdef DEBUG_MODE
    return true;
#else
    return false;
#endif
  }
  bool surrogateMode()
  {
#ifdef RANDOM_Q1_SURROGATE
    return true;
#else
    return false;
#endif
  }
  //distance
  std::vector<std::string> objPaths;
  std::vector<COMMON::ObjMeshGeomCellExact> objs;
  void distance(const char** paths,int nrShape,const double* pss,double* dss,double* nss,double* hss,int nrPt)
  {
    using namespace COMMON;
    disableTiming();

    //get shape
    TBEG("ReadObjMeshGeomCell");
    objPaths.resize(nrShape,"");
    objs.resize(nrShape);
    OMP_PARALLEL_FOR_
    for(sizeType i=0; i<nrShape; i++)
      if(objPaths[i]!=paths[i]) {
        ASSERT_MSGV(COMMON::exists(paths[i]),"Cannot find: %s!",paths[i])
        objs[i].SerializableBase::read(paths[i]);
        objPaths[i]=paths[i];
      }
    TEND();

    //get distance
    TBEG("GetDistance");
    Eigen::Map<const Eigen::Matrix<double,-1,-1,Eigen::RowMajor>> pssMap(pss,3*nrShape,nrPt);
    Eigen::Map<Eigen::Matrix<double,-1,1>> dssMap(dss,nrPt*nrShape);
    Eigen::Map<Eigen::Matrix<double,3,-1>> nssMap(nss,3,nrPt*nrShape);
    Eigen::Map<Eigen::Matrix<double,3,-1>> hssMap(hss,3,nrPt*3*nrShape);
    Vec3 n,normal;
    Mat3 hessian;
    Vec2i feat;
#ifndef DEBUG_MODE
    OMP_PARALLEL_FOR_I(OMP_PRI(n,normal,hessian,feat))
#endif
    for(sizeType i=0; i<nrShape*nrPt; i++) {
      sizeType shapeId=i/nrPt;
      sizeType ptId=i%nrPt;
      Vec3 pt=pssMap.block<3,1>(shapeId*3,ptId).cast<scalar>();
      dssMap[i]=objs[shapeId].closest(pt,n,normal,hessian,feat);
#ifdef DEBUG_MODE
      std::cout << "Shape" << shapeId << " Point" << ptId << std::endl;
      std::cout << "Point:" << pt.transpose() << std::endl;
      std::cout << "Normal:" << normal.transpose() << std::endl;
      std::cout << "Hessian:\n" << hessian << std::endl;
#endif
      nssMap.col(i)=normal.cast<double>();
      hssMap.block<3,3>(0,i*3)=hessian.cast<double>();
    }
    TEND();
  }
  //Q1 lower bound
  COMMON::Q1LowerBound Q1LB;
  void initQ1LowerBound(const double* M,double mu,int halfOrder,int nrPt,int nrDir)
  {
    Eigen::Map<const COMMON::ScalarUtil<double>::ScalarMat6> MMap(M);
    Q1LB=COMMON::Q1LowerBound(MMap.inverse().cast<scalarD>(),mu,halfOrder,nrPt,nrDir);
  }
  double computeQ1LowerBound(const double* vss,const double* dvss,int nr,double* dQ1dv)
  {
#ifdef RANDOM_Q1_SURROGATE
    static COMMON::ScalarUtil<double>::ScalarCol coefVss;
    if(coefVss.size()!=6*Q1LB.nrDir()*Q1LB.nrPt())
      coefVss.setRandom(6*Q1LB.nrDir()*Q1LB.nrPt());
    double Q1=coefVss.dot(Eigen::Map<const COMMON::ScalarUtil<double>::ScalarCol>(vss,coefVss.size()));
    if(nr>0)
      for(sizeType j=0; j<nr; j++)
        dQ1dv[j]=coefVss.dot(Eigen::Map<const COMMON::ScalarUtil<double>::ScalarCol>(dvss+j*coefVss.size(),coefVss.size()));
    return Q1;
#else
    COMMON::SDPSolution sol;
    COMMON::Q1LowerBound::SOSProb prob;
    COMMON::Q1LowerBound::Vsss dVdEps(nr);
    Eigen::Map<const COMMON::ScalarUtil<double>::ScalarMat6X> vssMap(vss,6,Q1LB.nrDir()*Q1LB.nrPt());
#ifdef DEBUG_MODE
    for(sizeType i=0; i<vssMap.cols(); i++)
      printf("vss%3d: %+1.10f %+1.10f %+1.10f %+1.10f %+1.10f %+1.10f\n",i,vssMap(0,i),vssMap(1,i),vssMap(2,i),vssMap(3,i),vssMap(4,i),vssMap(5,i));
#endif
    for(sizeType i=0,off=0; i<nr; i++,off+=6*Q1LB.nrDir()*Q1LB.nrPt()) {
      Eigen::Map<const COMMON::ScalarUtil<double>::ScalarMat6X> dVdEpsMap(dvss+off,6,Q1LB.nrDir()*Q1LB.nrPt());
      dVdEps[i]=dVdEpsMap.cast<scalarD>();
    }
    prob=Q1LB.buildProb(vssMap.cast<scalarD>());
    double ret=Q1LB.solveProb(prob,sol,0.0f,false);
    if(nr>0) {
      COMMON::Q1LowerBound::Vec dQ1dvMap=Q1LB.computeDXDEpsSymmetric(vssMap.cast<scalarD>(),dVdEps,prob,sol);
      Eigen::Map<COMMON::ScalarUtil<double>::ScalarCol>(dQ1dv,nr)=dQ1dvMap.cast<double>();
      ASSERT(dQ1dvMap.size()==nr)
    }
    return ret;
#endif
  }
  //self collision
  COMMON::SelfCollision coll;
  std::vector<std::string> psetPaths;
  std::vector<COMMON::ObjectSample> psets;
  void writeSelfColl(const char* path)
  {
    coll.SerializableBase::write(path);
  }
  void readSelfColl(const char* path)
  {
    coll.SerializableBase::read(path);
  }
  void createSelfColl()
  {
    using namespace COMMON;
    coll=SelfCollision();
  }
  void printSelfColl()
  {
    coll.print();
  }
  void assemble()
  {
    coll.assemble();
  }
  void prune(int i,int j)
  {
    coll.prune(i,j);
  }
  int addLink(const double* vss,int nrPt,double r)
  {
    using namespace COMMON;
    Mat3X pss=Eigen::Map<const Eigen::Matrix<double,3,-1>>(vss,3,nrPt).cast<scalar>();
    return coll.addLink(pss,r);
  }
  void detect(const double* tss,const char** paths,int nr,double* dtss,double* loss,bool deepest)
  {
    using namespace COMMON;
    disableTiming();

    //get shape
    if(paths) {
      TBEG("ReadObjectSample");
      psetPaths.resize(nr,"");
      psets.resize(nr);
      OMP_PARALLEL_FOR_
      for(sizeType i=0; i<nr; i++)
        if(psetPaths[i]!=paths[i]) {
          ASSERT_MSGV(COMMON::exists(paths[i]),"Cannot find: %s!",paths[i])
          psets[i].SerializableBase::read(paths[i]);
          psetPaths[i]=paths[i];
        }
      TEND();
    }

    //detect
    sizeType off=coll.nrLink()*12;
    OMP_PARALLEL_FOR_
    for(sizeType i=0; i<nr; i++) {
      SelfCollision::Vec tssV=Eigen::Map<const Eigen::Matrix<double,-1,1>>(tss+i*off,off).cast<scalar>();
      SelfCollision::Vec dtssV=SelfCollision::Vec::Zero(off);
      loss[i]=coll.detect(paths?&psets[i]:NULL,tssV,dtssV,[&](const Vec3&) {},deepest);
      Eigen::Map<Eigen::Matrix<double,-1,1>>(dtss+i*off,off)=dtssV.cast<double>();
    }
  }
  void writeVTK(const double* tss,const char** paths,int nr,const char* path)
  {
    using namespace COMMON;
    disableTiming();

    //get shape
    if(paths) {
      TBEG("ReadObjectSample");
      psetPaths.resize(nr,"");
      psets.resize(nr);
      OMP_PARALLEL_FOR_
      for(sizeType i=0; i<nr; i++)
        if(psetPaths[i]!=paths[i]) {
          ASSERT_MSGV(COMMON::exists(paths[i]),"Cannot find: %s!",paths[i])
          psets[i].SerializableBase::read(paths[i]);
          psetPaths[i]=paths[i];
        }
      TEND();
    }

    sizeType off=coll.nrLink()*12;
    recreate(path);
    OMP_PARALLEL_FOR_
    for(sizeType i=0; i<nr; i++) {
      SelfCollision::Vec tssV=Eigen::Map<const Eigen::Matrix<double,-1,1>>(tss+i*off,off).cast<scalar>();
      coll.writeVTK(&tssV,std::string(path)+"/shape"+std::to_string(i));
      if(paths)
        psets[i].writeVTK(std::string(path)+"/shape"+std::to_string(i)+"/object.vtk");
      coll.detectVTK(paths?&psets[i]:NULL,tssV,std::string(path)+"/shape"+std::to_string(i));
    }
  }
}

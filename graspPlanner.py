import torch,os,progressbar,math,transforms3d
from distance import Distance,SDF,filter_files
from distanceCubic import DistanceCubic
from distanceExact import DistanceExact
from selfCollision import SelfCollision
from directions import Directions
import torch.optim as optim
from tensorboardX import SummaryWriter
from hand import Hand,trimesh_to_vtk,write_vtk
from network import DepthBasedCNN,GridBasedCNN
import vtk,trimesh,pickle,scipy,json,argparse,random,re
import computeQ1LowerBound,computeQ1UpperBound
import numpy as np
import xml.etree.ElementTree as ET
import transforms3d
import xml.dom.minidom as minidom

def config_from_xml(hand,xml_file):
    tree=ET.parse(xml_file)
    root=tree.getroot()
    robot_dof=root[1][1].text
    robot_dof_parameters=re.findall("[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?",robot_dof)
    pos_np=np.zeros(3,dtype=float)
    rot_np=np.zeros(4,dtype=float)
    joint_np=np.zeros(18, dtype=float)
    for m in range(len(robot_dof_parameters)):
        joint_np[m]=float(robot_dof_parameters[m])
        if abs(joint_np[m])<1e-10:
            joint_np[m]=0
    if hand.use_joint_limit:
        lb,ub=hand.lb_ub()
        joint_np=np.clip(joint_np,lb,ub)
        new_joint_np=np.zeros(hand.nr_dof(),dtype=float)
        for i in range(len(joint_np)):
            if joint_np[i]==ub[i]:
                new_joint_np[i]=-np.log((ub[i]-joint_np[i]+1e-8)/(joint_np[i]-lb[i]))
            elif joint_np[i]==lb[i]:
                new_joint_np[i]=-np.log((ub[i]-joint_np[i])/(joint_np[i]-lb[i]+1e-8))
            else:
                new_joint_np[i]=-np.log((ub[i]-joint_np[i])/(joint_np[i]-lb[i]))
        # def sigmoid(X):
        #     return 1/(1+np.exp(-X))
        # back_joint_np=sigmoid(new_joint_np)*(ub-lb)+lb
        joint_np=new_joint_np
        for i in range(len(joint_np)):
            if np.isinf(joint_np[i]) or np.isnan(joint_np[i]):
                print("DATA ERROR")
                exit()
    robot_transform=root[1][2][0].text
    robot_transform_parameters=re.findall("[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?",robot_transform)
    for m in range(len(robot_transform_parameters)-3):
        rot_np[m]=float(robot_transform_parameters[m])
    pos_np[0]=float(robot_transform_parameters[4])/100.0
    pos_np[1]=float(robot_transform_parameters[5])/100.0
    pos_np[2]=float(robot_transform_parameters[6])/100.0
    hand_config=np.concatenate((np.concatenate((pos_np,rot_np),axis=0),joint_np),axis=0)
    obj_transform=root[0][1][0].text
    obj_transform_parameters=re.findall("[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?",obj_transform)
    obj_config=np.asarray([None for i in range(7)])
    obj_config[0]=float(obj_transform_parameters[4])
    obj_config[1]=float(obj_transform_parameters[5])
    obj_config[2]=float(obj_transform_parameters[6])
    obj_config[3]=float(obj_transform_parameters[0])
    obj_config[4]=float(obj_transform_parameters[1])
    obj_config[5]=float(obj_transform_parameters[2])
    obj_config[6]=float(obj_transform_parameters[3])
    return hand_config,obj_config

def write_normal_vtk(location,normal,scale,path):
    appendFilter=vtk.vtkAppendPolyData()
    for i in range(len(location)):
        #normal
        normal_vtk=vtk.vtkArrowSource()
        normal_vtk.SetTipResolution(100)
        normal_vtk.SetShaftResolution(100)
        # Generate a random start and end point
        startPoint=[location[i][0],location[i][1],location[i][2]]
        endPoint=[0]*3
        rng=vtk.vtkMinimalStandardRandomSequence()
        rng.SetSeed(8775070)  # For testing.
        n=[normal[i][0],normal[i][1],normal[i][2]]
        direction=[None,None,None]
        direction[0]=n[0]
        direction[1]=n[1]
        direction[2]=n[2]
        for j in range(0,3):
            endPoint[j]=startPoint[j]+direction[j]*scale
        # Compute a basis
        normalizedX=[0 for i in range(3)]
        normalizedY=[0 for i in range(3)]
        normalizedZ=[0 for i in range(3)]
        # The X axis is a vector from start to end
        vtk.vtkMath.Subtract(endPoint,startPoint,normalizedX)
        length=vtk.vtkMath.Norm(normalizedX)
        vtk.vtkMath.Normalize(normalizedX)
        # The Z axis is an arbitrary vector cross X
        arbitrary=[0 for i in range(3)]
        for j in range(0,3):
            rng.Next()
            arbitrary[j]=rng.GetRangeValue(-10,10)
        vtk.vtkMath.Cross(normalizedX,arbitrary,normalizedZ)
        vtk.vtkMath.Normalize(normalizedZ)
        # The Y axis is Z cross X
        vtk.vtkMath.Cross(normalizedZ,normalizedX,normalizedY)
        matrix=vtk.vtkMatrix4x4()
        # Create the direction cosine matrix
        matrix.Identity()
        for j in range(0,3):
            matrix.SetElement(j,0,normalizedX[j])
            matrix.SetElement(j,1,normalizedY[j])
            matrix.SetElement(j,2,normalizedZ[j])
        # Apply the transforms
        transform=vtk.vtkTransform()
        transform.Translate(startPoint)
        transform.Concatenate(matrix)
        transform.Scale(length,length,length)
        # Create a transformFilter for the arrow
        transformFilter=vtk.vtkTransformPolyDataFilter()
        transformFilter.SetInputConnection(normal_vtk.GetOutputPort())
        transformFilter.SetTransform(transform)
        appendFilter.AddInputConnection(transformFilter.GetOutputPort())
        appendFilter.Update()
    writer=vtk.vtkPolyDataWriter()
    writer.SetInputConnection(appendFilter.GetOutputPort())
    writer.SetFileName(path)
    writer.Write()

def mkdir(path):
    folder=os.path.exists(path)
    if not folder:
        os.makedirs(path)

def adjust_learning_rate(optimizer,epoch,learning_rate,decay_rate,decay_step):
    lr=learning_rate*(decay_rate**(epoch//decay_step))
    for param_group in optimizer.param_groups:
        param_group['lr']=lr

def points_to_vtk(points):
    contacts=None
    for i in range(len(points[0])):
        point_mesh=trimesh.primitives.Sphere(radius=0.05,center=np.asarray([points[0][i][0], \
                                                points[0][i][1],points[0][i][2]]))
        if contacts==None:
            contacts=point_mesh
        else:
            contacts+=point_mesh
    return trimesh_to_vtk(contacts)

def write_txt(input_str,path):
    file=open(path,'w')
    file.write(input_str) 
    file.close()

class CombinedLoss(torch.nn.Module):
    def __init__(self,Q1Type):
        super(CombinedLoss, self).__init__()
        assert Q1Type=="Q1Lower" or Q1Type=="Q1Upper"
        self.Q1Type=Q1Type

    def forward(self,M,mu,alpha,sss,pss,distance_fn,collison_value,debug=False,collision_fn=None,beta=None,\
                hand_normal=None,tss=None,sdfs=None,mesh_paths=None,pset_paths=None,collision=0):
        if sdfs is not None:
            dc=distance_fn()
            d=dc(pss,sdfs)
        elif mesh_paths is not None:
            distance_fn.mesh_paths=mesh_paths
            d=distance_fn.apply(pss)
            if debug:
                print('Distance:')
                print(d[0])
        if self.Q1Type=="Q1Upper":
            cq=computeQ1UpperBound.ComputeQ1Layer()
            if hand_normal is not None:
                q=cq(M,mu,alpha,pss,d[0],d[1],sss,beta=beta,hand_normal=hand_normal)
            else: q=cq(M,mu,alpha,pss,d[0],d[1],sss)
            if debug:
                print('Q1:')
                print(q)
        else:
            cq=computeQ1LowerBound.ComputeQ1Layer
            cq.init(M,mu,pss.shape[2],alpha)
            q=cq.apply(pss,d[0],d[1])
        # add normal as loss
        finger_tip_index=torch.tensor([12,22,30,38,43]).type(d[0].type()).long()
        hand_normal=hand_normal.transpose(1,2)
        d1=torch.index_select(d[1],1,finger_tip_index)
        hn=torch.index_select(hand_normal,1,finger_tip_index)
        normal_dot=torch.bmm(d1.view(-1,3).unsqueeze(1),hn.contiguous().view(-1,3).unsqueeze(-1))+1
        z=torch.exp(-q)
        distances=torch.index_select(d[0],1,finger_tip_index)
        if collision_fn:
            if collision==1:
                collision_fn.mesh_paths=None
                c=collision_fn.apply(tss)
                if debug:
                    print('Collision:')
                    print(c)
                    print('Distance Loss:')
                    print(d[2])
                    collison_value[0]=c.sum()
                return z.sum()+c.sum()+(distances**2).sum()+d[2]+0.1*normal_dot.sum()
            else:
                collision_fn.mesh_paths=pset_paths
                c=collision_fn.apply(tss)
                if debug:
                    print('Collision:')
                    print(c)
                    collison_value[0]=c.sum()
                return z.sum()+c.sum()+(distances**2).sum()+d[2]+0.1*normal_dot.sum()
        else:
            if debug:
                print('Distance Loss:')
                print(d[2])
            return z.sum()+(distances**2).sum()+d[2]

class GraspPlanner():
    def __init__(self,args):
        #basic parameters
        FUNCTION_MAP={"linear":Distance,"cubic":DistanceCubic,"exact":DistanceExact}
        self.args=args
        if self.args.M==1:
            self.M=None
        else:
            self.M=torch.eye(6)
            self.M[3][3]=self.args.M
            self.M[4][4]=self.args.M
            self.M[5][5]=self.args.M
        dirs=Directions(res=self.args.dir_res)
        self.sss=torch.Tensor(dirs.dirs)
        self.loss_fn=CombinedLoss(Q1Type=self.args.Q1Type)
        self.distance_fn=FUNCTION_MAP[self.args.distance_fn]
        self.debug=self.args.debug
        #create hand
        self.hand=None
        self.opt_vars=None
        self.network=None
        self.collison_value=[9999] #recording collision_value
        if self.args.mode!='points':
            assert self.args.hand is not '',('Specify a hand!')
            self.hand=Hand("hand/"+self.args.hand+"/",0.01,use_joint_limit=self.args.joint_limit,\
                 use_quat=self.args.use_quat,use_eigen=self.args.use_eigen)
        #output directory
        self.ex_name=self.args.ex_name
        self.ex_dir=self.args.output_dir+self.ex_name
        mkdir(self.ex_dir)
        #read objects
        if self.distance_fn==DistanceExact:
            self.object_paths=filter_files(self.args.input_dir,'.BVH.dat',[],abs=True)
        else: self.object_paths=filter_files(self.args.input_dir,'.vtk.pickle',[],abs=True)
        #read SDFs
        self.sdfs=None
        if self.distance_fn is not DistanceExact:
            self.sdfs=[]
            for i in range(len(self.object_paths)):
                self.sdfs.append(pickle.load(open(self.object_paths[i],'rb')))
        #try reading PSet paths
        self.pset_paths=None
        if self.args.hand is not '':
            assert self.args.mode!='points',('Do not specify a hand when use points mode')
            self.pset_paths=filter_files(self.args.input_dir,'PSet.dat',[],abs=True)
            if len(self.pset_paths)!=len(self.object_paths):
                print('PSet length mismatch, discarding PSet!')
                self.pset_paths=None
            else:
                for o in self.object_paths:
                    if not (o[:-7]+'PSet.dat') in self.pset_paths:
                        print('PSet name (%s) mismatch, discarding PSet!'%o)
                        self.pset_paths=None
                        break
        #try constructing SelfCollision
        self.SelfCollision=None
        if self.args.collision>0:
            assert self.args.hand is not '', 'SelfCollision can only be used with hands!'
            SelfCollision.init(self.hand,0.01)
            self.SelfCollision=SelfCollision
        #build optimized variables
        if self.args.mode=='points':
            self.args.beta=None
            assert self.hand is None
            #-----------------------------hard coded initialization
            self.opt_vars=torch.DoubleTensor(1,3,3).uniform_(-1,1)
            self.opt_vars.requires_grad_()
            def get_pss_nss_tss(data):
                assert data is None
                if isinstance(self.opt_vars, list):
                    return self.opt_vars[0],None,None
                else:
                    return self.opt_vars,None,None
            self.pss_nss_tss=get_pss_nss_tss
            self.opt_vars=[self.opt_vars]
            assert len(self.object_paths)==1,("Only one object supported!")

        elif self.args.mode=='hand':
            assert self.hand is not None
            #-----------------------------hard coded initialization
            if self.args.use_eigen:
                self.opt_vars=torch.randn(1,self.hand.eg_num+self.hand.extrinsic_size).fill_(0)
            else:
                self.opt_vars=torch.randn(1,self.hand.nr_dof()+self.hand.extrinsic_size).fill_(0)
            self.opt_vars[:,0]=-0.5
            self.opt_vars[:,2]=3.5
            self.opt_vars[:,3]=0.0
            self.opt_vars[:,4]=1.0
            if self.args.joint_limit:
                lb,ub=self.hand.lb_ub()
                lb=torch.from_numpy(lb).view(1,-1)
                ub=torch.from_numpy(ub).view(1,-1)
                self.opt_vars[:,self.hand.extrinsic_size:]=-torch.log((ub-lb)/(0.0001-lb)-1)
            #-----------------------------hard coded initialization
            def get_pss_nss_tss(data):
                assert data is None
                if isinstance(self.opt_vars, list):
                    return self.hand.forward(self.opt_vars[0])
                else:
                    return self.hand.forward(self.opt_vars)
            self.pss_nss_tss=get_pss_nss_tss
            self.opt_vars.requires_grad_()
            self.opt_vars=[self.opt_vars]
        else:
            assert self.args.mode=='nn' and self.hand
            assert self.args.optimizer=='adam', ("Only adam can be used in nn model")
            self.input_type=self.args.input_type
            if self.input_type=='grid':
                self.network=GridBasedCNN(self.args.hand,self.hand)
                if self.args.use_pretrained:
                    if self.args.use_good_gt:
                        self.network=torch.load('../ddg_data/grid_good_gt.pt',map_location=lambda storage,loc:storage)
                    else:
                        self.network=torch.load('../ddg_data/grid_bad_gt.pt',map_location=lambda storage,loc:storage)
            else: 
                self.network=DepthBasedCNN(self.args.hand,self.args.cnn_name,self.args.num_views,True,self.hand)
                if self.args.use_pretrained:
                    if self.args.use_good_gt:
                        self.network=torch.load('../ddg_data/depth_good_gt.pt',map_location=lambda storage,loc:storage)
                    else:
                        self.network=torch.load('../ddg_data/depth_bad_gt.pt',map_location=lambda storage,loc:storage)
            self.train_file_list=[]
            self.test_file_list=[]
            for i in range(len(self.object_paths)):
                if 'bigbird' in self.object_paths[i] or 'ycb' in self.object_paths[i]:
                    self.test_file_list.append(self.object_paths[i][:-8])
                else: self.train_file_list.append(self.object_paths[i][:-8])
            def get_pss_nss_tss(data):
                assert data is not None
                return self.hand.forward(self.network.forward(data))
            self.pss_nss_tss=get_pss_nss_tss
        #try loading
        use_gpu=self.args.use_gpu
        only_train=self.args.only_train
        output_grasps=self.args.output_grasps
        if self.args.resume>=1:
            model_list=filter_files(self.ex_dir,".pt",[])
            assert os.path.exists(self.ex_dir) or len(model_list)>0,("No trained model or config file found!")
            if self.args.mode!='nn':
                self.opt_vars=torch.load(self.ex_dir+"/model.pt",map_location=lambda storage,loc:storage)
            else:
                self.network=torch.load(self.ex_dir+"/model.pt",map_location=lambda storage,loc:storage)
            if self.args.resume>=2:
                assert os.path.exists(self.ex_dir) or len(model_list)>0 or os.path.exists(self.ex_dir+"/config.json"), \
                ("No trained model or config file found!")
                with open(os.path.join(self.ex_dir,"config.json")) as json_file:
                    args=argparse.Namespace(**json.load(json_file))
                args.resume=self.args.resume
                self.args=args
            else:
                config_f=open(os.path.join(self.ex_dir,"config.json"),"w")
                json.dump(vars(self.args),config_f)
                config_f.close()
        else:
            config_f=open(os.path.join(self.ex_dir,"config.json"),"w")
            json.dump(vars(self.args),config_f)
            config_f.close()
            if self.args.mode=='nn':
                torch.save(self.network,self.ex_dir+'/init.pt')
            else:
                torch.save(self.opt_vars,self.ex_dir+'/init.pt')
        self.args.use_gpu=use_gpu
        self.args.only_train=only_train
        self.args.output_grasps=output_grasps

    def plan(self):
        if self.args.resume<3:
            if self.args.optimizer=='lbfgs_sci':
                opt_sol=self.train_with_scipy(None)
                opt_sol=torch.from_numpy(opt_sol[0]).view(1,3,3)
                torch.save(opt_sol,self.ex_dir+'/model.pt')
                exit()
            else:
                os.system('rm -rf '+self.ex_dir+'/train')
                os.system('rm -rf '+self.ex_dir+'/test')
                train_writer=SummaryWriter(self.ex_dir+'/train')
                test_writer=SummaryWriter(self.ex_dir+'/test')
                optimizer=None
                if self.args.optimizer=='lbfgs':
                    optimizer=optim.LBFGS(self.opt_vars,lr=self.args.lr,max_iter=20,max_eval=None,tolerance_grad=1e-5, \
                                          tolerance_change=1e-9,history_size=7,line_search_fn='strong_wolfe')
                if self.args.optimizer=='sgd':
                    optimizer=optim.SGD(self.opt_vars,lr=self.args.lr)
                if self.args.optimizer=='adam':
                    if self.args.mode=='nn':
                        optimizer=optim.Adam(self.network.parameters(),lr=self.args.lr)
                    else:
                        optimizer=optim.Adam(self.opt_vars,lr=self.args.lr)
                assert optimizer is not None
                #main loop
                best_error=99999
                for epoch in range(1,self.args.max_epoch):
                    adjust_learning_rate(optimizer,epoch,self.args.lr,0.9,50)
                    loss=self.train_with_torch(optimizer,None)
                    if self.args.only_train:
                        error=0
                    else: error=self.test_with_torch(optimizer,None)
                    train_writer.add_scalar('Train loss',loss,epoch)
                    test_writer.add_scalar('Test error',error,epoch)
                    print('Train average loss: ',loss)
                    print('Test average error: ',error)
                    print('Finish epoch: ',epoch)
                    if self.args.mode!='nn' or self.args.only_train:
                        error=loss
                    if error<best_error:
                        best_error=error
                        print('Best error at epoch: ',epoch,'=',best_error)
                        print('Save model..................')
                        print("##########################################")
                        if self.args.mode!='nn':
                            torch.save(self.opt_vars,self.ex_dir+'/model.pt')
                        else:
                            torch.save(self.network,self.ex_dir+'/model.pt')
                train_writer.close()
                test_writer.close()
        else:
            self.evaluate()
        
    def get_loss(self,data):
        pss,nss,tss=self.pss_nss_tss(data)
        loss=self.loss_fn(self.M,
                          self.args.mu,
                          self.args.alpha,
                          self.sss,
                          pss,
                          self.distance_fn,
                          self.collison_value,
                          self.debug,
                          collision_fn=self.SelfCollision,
                          tss=tss,
                          beta=self.args.beta,
                          hand_normal=nss,
                          sdfs=self.sdfs,
                          mesh_paths=self.object_paths,
                          pset_paths=self.pset_paths,
                          collision=self.args.collision)
        return loss

    def train_with_scipy(self,data):
        def loss_fn(opt_vars):
            if self.hand is not None:
                self.opt_vars=torch.from_numpy(opt_vars).view(1,-1)
            else:
                self.opt_vars=torch.from_numpy(opt_vars).view(1,3,-1)
            self.opt_vars.requires_grad_()
            loss=self.get_loss(None)
            loss.backward()
            grad=self.opt_vars.grad.view(9).detach().numpy()
            return loss.detach().numpy(),grad
        input_th=self.opt_vars[0].detach().numpy()                                                                                                         
        opt_solution=scipy.optimize.fmin_l_bfgs_b(loss_fn,input_th,fprime=None,args=(),approx_grad=False,m=10,\
                                                  factr=1e2,pgtol=1e-05,iprint=-1,maxfun=150000,maxiter=150000,\
                                                  disp=1,callback=None,maxls=20)
        return opt_solution
    
    def train_with_torch(self,optimizer,data):
        if self.args.optimizer=='lbfgs':
            def closure():
                optimizer.zero_grad()
                loss=self.get_loss(data)
                loss.backward()
                return loss
            final_loss=optimizer.step(closure)
            return final_loss.item()
        else:
            if self.args.mode!='nn':
                optimizer.zero_grad()
                loss=self.get_loss(data)
                if self.debug:
                    if self.collison_value[0]>0:
                        torch.save(self.opt_vars,self.ex_dir+'/model.pt')
                        exit() 
                loss.backward()
                optimizer.step()
                return loss.item()
            else:
                average_loss=0
                batch_size=self.args.bs
                random.shuffle(self.train_file_list)
                p=progressbar.ProgressBar()
                p.start(len(self.train_file_list))
                for i in range(0,len(self.train_file_list),batch_size):
                    data=self.load_data(i,batch_size,self.train_file_list)
                    if self.args.use_gpu:
                        data=data.cuda()
                        self.network=self.network.cuda()
                    self.network.train()
                    optimizer.zero_grad()
                    loss=self.get_loss(data)
                    if self.debug:
                        if self.collison_value[0]>0:
                            torch.save(self.network,self.ex_dir+'/model.pt')
                            exit() 
                    loss.backward()
                    optimizer.step()
                    average_loss+=loss
                    p.update(i)
                p.finish()
                print('---------Finish one training epoch---------')
                average_loss/=len(self.train_file_list)
                return average_loss.item()
    
    def test_with_torch(self,optimizer,data):
        if self.args.mode!='nn':
            return 0.0
        else:
            with torch.no_grad():
                average_error=0
                batch_size=self.args.bs
                random.shuffle(self.test_file_list)
                self.network=self.network.eval()
                p=progressbar.ProgressBar()
                p.start(len(self.test_file_list))
                for i in range(0,len(self.test_file_list),batch_size):
                    data=self.load_data(i,batch_size,self.test_file_list)
                    if self.args.use_gpu:
                        data=data.cuda()
                        self.network=self.network.cuda()
                    error=self.get_loss(data)
                    average_error+=error
                    p.update(i)
                p.finish()
                print('---------Finish one test epoch---------')
                average_error/=len(self.test_file_list)
                return average_error.item()
    
    def load_data(self,start_index,size,file_list):
        if len(file_list)-start_index<size:
            size=len(file_list)-start_index
        paths=file_list[start_index:start_index+size]
        self.object_paths=[]
        self.pset_paths=[]
        for i in range(size):
            self.object_paths.append(paths[i]+'.BVH.dat')
            self.pset_paths.append(paths[i]+'.PSet.dat')
        if self.args.input_type=='grid':
            batch_data=torch.zeros(size,1,40,40,40)
            for i in range(size):
                batch_data[i]=torch.load(paths[i]+'.smoothed.obj.grid')
        if self.args.input_type=='depth':
            batch_data=torch.zeros(size*self.args.num_views,3,224,224)
            for i in range(size):
                depth_tensor=torch.load(paths[i]+'.smoothed.obj.depth')
                assert self.args.num_views<=depth_tensor.shape[0],\
                    ('number of view must less than rendered image views')
                for k in range(self.args.num_views):
                    batch_data[i*self.args.num_views+k]=torch.Tensor(depth_tensor[k])
        return batch_data

    def generate_results(self,mesh_path,data=None,pset_paths=None,opt_vars=None,network=None,\
        hand=None,collision_fn=None):
        if network is not None:
            model_dir=self.ex_dir
            self.ex_dir=self.ex_dir+'/'+mesh_path.split('/')[-1]
            mkdir(self.ex_dir)
        if self.args.use_gpu:
            data=data.cuda()
        final=self.pss_nss_tss(data)
        if self.args.gradcheck:
            print('AutoGradCheck(EigenGrasp)=',torch.autograd.gradcheck(self.loss_fn,\
                (self.M,self.args.mu,self.args.alpha,self.sss,final[0],self.distance_fn,\
                self.collison_value,self.debug,self.SelfCollision,self.args.beta,final[1],\
                final[2],self.sdfs,self.object_paths,self.pset_paths),\
                eps=1e-6,atol=1e-6,rtol=1e-5,raise_exception=True))
        if opt_vars is not None:
            self.opt_vars=torch.load(self.ex_dir+'/init.pt',map_location=lambda storage,loc:storage)
        if network is not None:
            self.network=torch.load(model_dir+'/init.pt',map_location=lambda storage,loc:storage)
            if self.args.use_gpu:
                self.network=self.network.cuda()
            self.network=self.network.eval()
        init=self.pss_nss_tss(data)
        if self.distance_fn is DistanceExact:
            self.distance_fn.mesh_paths=self.object_paths
            d_ori=self.distance_fn.apply(init[0])
            d_final=self.distance_fn.apply(final[0])
        else:
            dc=self.distance_fn()
            d_ori=dc(init[0],self.sdfs)
            d_final=dc(final[0],self.sdfs)      
        if self.args.Q1Type=="Q1Upper":
            cq=computeQ1UpperBound.ComputeQ1Layer()
            q1_ori=cq(self.M,self.args.mu,self.args.alpha,init[0],d_ori[0],d_ori[1],self.sss,\
                beta=self.args.beta,hand_normal=init[1])
            q1_final=cq(self.M,self.args.mu,self.args.alpha,final[0],d_final[0],d_final[1],self.sss,\
                beta=self.args.beta,hand_normal=final[1])
        else:
            cq_ori=computeQ1LowerBound.ComputeQ1Layer
            cq_ori.init(self.M,self.args.mu,init[0].shape[2],self.args.alpha)
            q1_ori=cq.apply(init[0],d_ori[0],d_ori[1])
            cq_final=computeQ1LowerBound.ComputeQ1Layer
            cq_final.init(self.M,self.args.mu,final[0].shape[2],self.args.alpha)
            q1_final=cq.apply(final[0],d_final[0],d_final[1])
        original_contacts_vtk=points_to_vtk(init[0].transpose(1,2))
        write_vtk(original_contacts_vtk,self.ex_dir+'/original_contacts.vtk')
        final_contacts_vtk=points_to_vtk(final[0].transpose(1,2))
        write_vtk(final_contacts_vtk,self.ex_dir+'/final_contacts.vtk')
        output_single_contact=False
        if output_single_contact:
            mkdir(self.ex_dir+'/original_contacts')
            for i in range(len(init[0].transpose(1,2)[0])):
                point_mesh=trimesh.primitives.Sphere(radius=0.05,center=np.asarray([init[0].cpu().transpose(1,2)[0][i][0], \
                                            init[0].cpu().transpose(1,2)[0][i][1],init[0].cpu().transpose(1,2)[0][i][2]]))
                write_vtk(trimesh_to_vtk(point_mesh),self.ex_dir+'/original_contacts/'+str(i)+'.vtk')
            mkdir(self.ex_dir+'/final_contacts')
            for i in range(len(final[0].transpose(1,2)[0])):
                point_mesh=trimesh.primitives.Sphere(radius=0.05,center=np.asarray([final[0].cpu().transpose(1,2)[0][i][0], \
                                            final[0].cpu().transpose(1,2)[0][i][1],final[0].cpu().transpose(1,2)[0][i][2]]))
                write_vtk(trimesh_to_vtk(point_mesh),self.ex_dir+'/final_contacts/'+str(i)+'.vtk')
        write_normal_vtk(init[0].squeeze().permute(1,0),d_ori[1].squeeze(),1,\
            self.ex_dir+'/original_normals.vtk')
        write_normal_vtk(final[0].squeeze().permute(1,0),d_final[1].squeeze(),1,\
            self.ex_dir+'/final_normals.vtk')
        if hand is not None:
            write_normal_vtk(init[0].squeeze().permute(1,0),init[1].squeeze().permute(1,0),1,\
                self.ex_dir+'/original_hand_normals.vtk')
            write_normal_vtk(final[0].squeeze().permute(1,0),final[1].squeeze().permute(1,0),1,\
            self.ex_dir+'/final_hand_normals.vtk')
            if collision_fn:
                collision_fn.mesh_paths=pset_paths
                c_ori=collision_fn.apply(init[2])
                c_final=collision_fn.apply(final[2])
            if opt_vars is not None:
                self.hand.forward_kinematics(self.opt_vars[0].detach().numpy()[0][0:self.hand.extrinsic_size],\
                                            self.opt_vars[0].detach().numpy()[0][self.hand.extrinsic_size:])
            if network is not None:
                original_network_output=self.network.forward(data)
                self.hand.forward_kinematics(original_network_output[0].cpu().detach().numpy()[0:self.hand.extrinsic_size],\
                            original_network_output[0].cpu().detach().numpy()[self.hand.extrinsic_size:])            
            hand_vtk=trimesh_to_vtk(self.hand.draw(1,False))
            write_vtk(hand_vtk,self.ex_dir+'/original_hand.vtk')
            if opt_vars is not None:
                self.opt_vars=torch.load(self.ex_dir+'/model.pt',map_location=lambda storage,loc:storage)
                self.hand.forward_kinematics(self.opt_vars[0].detach().numpy()[0][0:self.hand.extrinsic_size],\
                                                self.opt_vars[0].detach().numpy()[0][self.hand.extrinsic_size:])
            if network is not None:
                self.network=torch.load(model_dir+'/model.pt',map_location=lambda storage,loc:storage)
                if self.args.use_gpu:
                    self.network=self.network.cuda()
                self.network=self.network.eval()
                final_network_output=self.network.forward(data)
                self.hand.forward_kinematics(final_network_output[0].cpu().detach().numpy()[0:self.hand.extrinsic_size],\
                                                final_network_output[0].cpu().detach().numpy()[self.hand.extrinsic_size:])
            hand_vtk=trimesh_to_vtk(self.hand.draw(1,False))
            write_vtk(hand_vtk,self.ex_dir+'/final_hand.vtk')
        obj_vtk=trimesh_to_vtk(trimesh.load(mesh_path+'.smoothed.obj'))
        write_vtk(obj_vtk,self.ex_dir+'/mesh.vtk')
        if collision_fn:
            write_txt(str(c_ori.item()),self.ex_dir+'/Collision_original.txt')
            write_txt(str(c_final.item()),self.ex_dir+'/Collision_final.txt')
        write_txt(str(q1_ori.item()),self.ex_dir+'/Q1_original.txt')
        write_txt(str(q1_final.item()),self.ex_dir+'/Q1_final.txt')
        write_txt(str(d_ori[2]),self.ex_dir+'/Distance_original.txt')
        write_txt(str(d_final[2]),self.ex_dir+'/Distance_final.txt')
        if self.args.output_grasps:
            self.generate_grasp(original_network_output[0].cpu(),self.ex_dir+'/original_hand.xml')
            self.generate_grasp(final_network_output[0].cpu(),self.ex_dir+'/final_hand.xml')
        if network is not None:
            self.ex_dir=model_dir
        return q1_ori.item(),q1_final.item(),c_ori.item(),c_final.item()

    def generate_grasp(self,network_output,path):
        position,quaternion,dofs=torch.split(network_output,[3,4,18],dim=0)
        position=position*100
        lb,ub=self.hand.lb_ub()
        lb=torch.from_numpy(lb).view(1,-1)
        ub=torch.from_numpy(ub).view(1,-1)
        dofs=torch.from_numpy(np.asarray(dofs)).view(1,-1)
        sigmoid=torch.nn.Sigmoid()
        dofs=torch.squeeze(sigmoid(dofs)*(ub-lb)+lb)
        obj_name=self.ex_dir.split('/')[-1]+'.smoothed'
        body_path='/home/lm/Documents/ddg_data/grasp_dataset/good_shapes/'+obj_name+'.xml'
        #create xml
        root=ET.Element('world')
        graspableBody=ET.SubElement(root,'graspableBody')
        filename=ET.SubElement(graspableBody,'filename')
        filename.text=body_path
        transform=ET.SubElement(graspableBody,'transform')
        fullTransform=ET.SubElement(transform,'fullTransform')
        fullTransform.text='(+1 +0 +0 +0)[+0 +0 +0]'
        robot=ET.SubElement(root,'robot')
        filename=ET.SubElement(robot,'filename')
        filename.text='/home/lm/Documents/grasp_generation/graspitmodified_lm/graspit/models/robots/ShadowHand/ShadowHandSimple.xml'
        dofValues=ET.SubElement(robot,'dofValues')
        dof=''
        for j in range(18):
            dof=dof+str(dofs[j].item())+' '
        trans='('
        trans=trans+str(quaternion[0].item())+' '
        trans=trans+str(quaternion[1].item())+' '
        trans=trans+str(quaternion[2].item())+' '
        trans=trans+str(quaternion[3].item())+')['
        trans=trans+str(position[0].item())+' '
        trans=trans+str(position[1].item())+' '
        trans=trans+str(position[2].item())+']'
        dofValues.text=dof
        transform=ET.SubElement(robot,'transform')
        fullTransform=ET.SubElement(transform,'fullTransform')
        fullTransform.text=trans
        camera=ET.SubElement(root,'camera')
        position_=ET.SubElement(camera,'position')
        position_.text='+0 +0 +400.0'
        orientation=ET.SubElement(camera,'orientation')
        orientation.text='+0 +0 +0 +1'
        focalDistance=ET.SubElement(camera,'focalDistance')
        focalDistance.text='+250.0'
        #write xml
        root_str=ET.tostring(root)
        reparsed=minidom.parseString(root_str.decode())
        root_str=reparsed.toprettyxml(indent="\t")
        file=open(path,"w")
        file.write(root_str)
        file.close()
        pose_refine=False
        if pose_refine:
            # pose refine
            binary='/home/lm/Documents/grasp_generation/graspitmodified_lm/build-graspit-Desktop-Release/graspit_cmdline'
            bodyRot=str(1)+','+str(0)+','+str(0)+','+str(0)
            bodyTrans='0,0,0'
            robotFile='/home/lm/Documents/grasp_generation/graspitmodified_lm/graspit/models/robots/ShadowHand/ShadowHandSimple.xml'
            robotRot=str(quaternion[0].item())+','+str(quaternion[1].item())+','+\
                    str(quaternion[2].item())+','+str(quaternion[3].item())
            robotTrans=str(position[0].item())+','+str(position[1].item())+','+str(position[2].item())
            robotDOF='0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'
            resultFile=self.ex_dir+'/'
            desiredDOF=str(dofs[0].item())
            for j in range(17):
                desiredDOF=desiredDOF+','+str(dofs[j].item())
            command=binary+' --bodyFile='+body_path+' --bodyRot='+bodyRot+' --bodyTrans='+bodyTrans+' --robotFile='+robotFile  \
                        +' --robotRot='+robotRot+' --robotTrans='+robotTrans+' --robotDOF='+robotDOF+' --resultFile='+resultFile \
                        +' --desiredDOF='+desiredDOF
            print(command)
            os.system(command)
            hand_refine_path=path.split('/')[-1][:-4]+'_refine.xml'
            hand_refine_quality_path=path.split('/')[-1][:-4]+'_refine_quality.txt'
            command_mv='mv '+resultFile+'grasp.xml'+' '+resultFile+hand_refine_path
            os.system(command_mv)
            command_mv='mv '+resultFile+'eps_qual.txt'+' '+resultFile+hand_refine_quality_path
            os.system(command_mv)

    def evaluate(self):
        if self.args.mode!='nn':
            mesh_path=self.object_paths[0][:-8]
            data=None
            self.generate_results(mesh_path,data,self.pset_paths,self.opt_vars,self.network,\
                                  self.hand,self.SelfCollision)
        else:
            with torch.no_grad():
                if self.args.use_gpu:
                    self.network=self.network.cuda()
                self.network=self.network.eval()
                if self.args.only_train:
                    eval_file_list=self.train_file_list
                else: eval_file_list=self.test_file_list
                avg_Q1_ori=0
                avg_Q1_final=0
                avg_Col_ori=0
                avg_Col_final=0
                for i in range(len(eval_file_list)):
                    data=self.load_data(i,1,eval_file_list)
                    mesh_path=eval_file_list[i]
                    Q1_ori,Q1_final,Col_ori,Col_final=self.generate_results(mesh_path,data,self.pset_paths,self.opt_vars,self.network,\
                        self.hand,self.SelfCollision)
                    print('Q1_ori: ',Q1_ori)
                    print('Q1_final: ',Q1_final)
                    print('Col_ori: ',Col_ori)
                    print('Col_final: ',Col_final)
                    avg_Q1_ori+=Q1_ori
                    avg_Q1_final+=Q1_final
                    avg_Col_ori+=Col_ori
                    avg_Col_final+=Col_final
                    print('Finished '+mesh_path)
                avg_Q1_ori/=len(eval_file_list)
                avg_Q1_final/=len(eval_file_list)
                avg_Col_ori/=len(eval_file_list)
                avg_Col_final/=len(eval_file_list)
                print('avg_Q1_ori: ',avg_Q1_ori)
                print('avg_Q1_final: ',avg_Q1_final)
                print('avg_Col_ori: ',avg_Col_ori)
                print('avg_Col_final: ',avg_Col_final)

def generate_results_from_graspit_xml(xml_path,mesh_path,output_dir):
    hand=Hand("hand/ShadowHand/",1,use_joint_limit=True,use_quat=True,use_eigen=False)
    hand_config,obj_config=config_from_xml(hand,xml_path)
    hand_config[0:3]=hand_config[0:3]*100
    hand.forward_kinematics(hand_config[0:hand.extrinsic_size],hand_config[hand.extrinsic_size:])
    hand_vtk=trimesh_to_vtk(hand.draw(1,False))
    write_vtk(hand_vtk,output_dir+'/hand.vtk')
    obj_rotation=transforms3d.quaternions.quat2mat(obj_config[3:7])
    obj_translation=np.zeros(3)
    obj_translation[0]=obj_config[0]
    obj_translation[1]=obj_config[1]
    obj_translation[2]=obj_config[2]
    obj_transform=transforms3d.affines.compose(obj_translation,obj_rotation,[1,1,1])
    mesh=trimesh.load(mesh_path)
    mesh.apply_transform(obj_transform)
    obj_vtk=trimesh_to_vtk(mesh)
    write_vtk(obj_vtk,output_dir+'/mesh.vtk')

def get_parser():
    parser=argparse.ArgumentParser()
    parser.add_argument("-ex_name",type=str,help="experiment name",default="ex")
    parser.add_argument("-mode",type=str,help="optimization mode: points, hand or network",default="hand")
    parser.add_argument("-optimizer",type=str,help="optimizer",default="adam")
    parser.add_argument("-distance_fn",type=str,help="function for distance calculation",default="cubic")  
    parser.add_argument("-lr",type=float,help="learning rate",default=1e-2)
    parser.add_argument("-alpha",type=float,help="alpha",default=0.8)
    parser.add_argument("-beta",type=float,help="alpha",default=None)
    parser.add_argument("-Q1Type",type=str,help="choose metric: Q1Upper, or Q1Lower",default="Q1Lower")
    parser.add_argument("-mu",type=float,help="coefficient of friction",default=0.7)
    parser.add_argument("-M",type=float,help="M, if 1(I or None) else if 0(no p×f)",default=1e-3)
    parser.add_argument("-dir_res",type=int,help="directions resolution",default=2)
    parser.add_argument("-hand",type=str,help="ShadowHand or BarrettHand",default="")
    parser.add_argument("-joint_limit",action="store_true",help="if set, apply joint limit to hand")
    parser.add_argument("-debug",action="store_true",help="if set, debug mode")
    parser.add_argument("-collision",type=int,help="0:for points mode,1:only self-collision,\
                                                    2:hand and objects collision",default=0)
    parser.add_argument("-input_dir",type=str,default="../ddg_data/tests")
    parser.add_argument("-output_dir",type=str,default="../ddg_results/")
    parser.add_argument("-input_type",type=str,help="input data type: grids or depths",default="grid")
    parser.add_argument("-bs",type=int,help="batch size",default=8)
    parser.add_argument("-use_pretrained",action="store_true",help="if set, use pretrained depth-based model")
    parser.add_argument("-cnn_name",type=str,help="depth-based cnn model name",default="alexnet")
    parser.add_argument("-num_views",type=int,help="depth-based number of views",default=3)
    parser.add_argument("-use_gpu",action="store_true",help="if set, use gpu to train")
    parser.add_argument("-use_quat",action="store_true",help="if set, use quaternion for hand FK")
    parser.add_argument("-use_eigen",action="store_true",help="if set, use eigen for hand FK")
    parser.add_argument("-gradcheck",action="store_true",help="if set, gradcheck for output")
    parser.add_argument("-only_train",action="store_true",help="if set, only use training file for testing")
    parser.add_argument("-use_good_gt",action="store_true",help="if set, use good gt pretrained model")
    parser.add_argument("-max_epoch",type=int,default=300000)
    parser.add_argument("-output_grasps",action="store_true",help="if set, output grasps")
    parser.add_argument("-resume",type=int,help="0:train a new model, \
                                                 1:retrain the model with new config, \
                                                 2:continue to train the model with old config, \
                                                 3:evaluate.",default=0)
    return parser

if __name__=='__main__':
    xml_path='/media/lm/documents/RSS2020_results/teaser/world.xml'
    mesh_path='/media/lm/documents/RSS2020_results/teaser/021_bleach_cleanser.off'
    output_path='/media/lm/documents/RSS2020_results/teaser'
    generate_results_from_graspit_xml(xml_path,mesh_path,output_path)



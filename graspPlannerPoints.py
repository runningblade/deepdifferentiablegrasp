import graspPlanner

if __name__=='__main__':
    parser=graspPlanner.get_parser()
    args=parser.parse_args()
    args.distance_fn='exact'
    args.optimizer='adam'
    args.mode='points'
    args.joint_limit=True
    args.lr=0.01
    args.alpha=3.0
    args.Q1Type='Q1Upper'
    args.dir_res=2
    args.mu=0.7
    args.resume=0
    args.input_dir='../ddg_data/meshes'
    myPlanner=graspPlanner.GraspPlanner(args)
    myPlanner.plan()